const mongoose = require('mongoose');
const CountryList = mongoose.model('CountriesList');
const passport = require('passport');
const _ = require('lodash');


module.exports.getAllCountryLists = (req, res, next) =>{
    CountryList.find(
        (err, countryList) => {
            if (!countryList)
                return res.status(404).json({ status: false, message: 'Country List record not found.' });
            else
                return res.status(200).json({ status: true, countryList : countryList });
        }
    );
}
