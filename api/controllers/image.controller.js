const mongoose = require('mongoose');
const passport = require('passport');
const _ = require('lodash');
var fs = require('fs'); 
var path = require('path'); 
var multer = require('multer'); 
  
module.exports.postImage = (req, res, file) => {
    try {
        return res.status(201).json({
            message: 'File uploded successfully'
        });
    } catch (error) {
        console.error(error);
    }
}
  
module.exports.postPrimaryImage = (req, res, file) => {
    try {
        return res.status(201).json({
            filename: req.file.filename
        });
    } catch (error) {
        console.error(error);
    }
}
module.exports.postSecondaryImage = (req, res, file) => {
    try {
        return res.status(201).json({
            filename: req.file.filename
        });
    } catch (error) {
        console.error(error);
    }
}
module.exports.postWbLogoImage = (req, res, file) => {
    try {
        return res.status(201).json({
            filename: req.file.filename
        });
    } catch (error) {
        console.error(error);
    }
}
module.exports.postAdbWbLogoImage = (req, res, file) => {
    try {
        return res.status(201).json({
            filename: req.file.filename
        });
    } catch (error) {
        console.error(error);
    }
}
