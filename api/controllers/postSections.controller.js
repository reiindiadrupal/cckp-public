const mongoose = require('mongoose');
const Acknowledgement = mongoose.model('Acknowledgement');
const Foreword = mongoose.model('Foreword');
const KeyMessage = mongoose.model('KeyMessage');
const CountryOverview = mongoose.model('CountryOverview');
const Climatology = mongoose.model('Climatology');
const ClimateRelatedNaturalHazard = mongoose.model('ClimateRelatedNaturalHazard');
const ClimateChangeImpact = mongoose.model('ClimateChangeImpact');
const PoliciesAndProgram = mongoose.model('PoliciesAndProgram');
const Adaptation = mongoose.model('Adaptation');
const passport = require('passport');
const _ = require('lodash');

module.exports.postSectionsData = (req,res,next) => {
    
    var acknowledgement = new Acknowledgement();
    acknowledgement.countryProfileID = mongoose.Types.ObjectId(req.body.countryProfileID);
    acknowledgement.acknowledgementMainSection = req.body.acknowledgementMainSection;
    acknowledgement.isDisplayAcknowledgementSection = req.body.isDisplayAcknowledgementSection;
    acknowledgement.save((err, doc) => {
        if(!err) {
           // res.send(doc);
        }
        else {
            //return next(err);
        }
    });

    var foreword = new Foreword();
    foreword.countryProfileID = mongoose.Types.ObjectId(req.body.countryProfileID);
    foreword.forewordMainSection = req.body.forewordMainSection;
    foreword.forewordFooterLinks = req.body.forewordFooterLinks;
    foreword.isDisplayforewordSection = req.body.isDisplayforewordSection;
    foreword.forewords = req.body.forewords;
    foreword.save((err, doc) => {
        if(!err) {
           // res.send(doc);
        }
        else {
            //return next(err);
        }
    });

    var keyMessage = new KeyMessage();
    keyMessage.countryProfileID = mongoose.Types.ObjectId(req.body.countryProfileID);
    keyMessage.keyMessageMainSection = req.body.keyMessageMainSection;
    keyMessage.keyMessageFooterLinks = req.body.keyMessageFooterLinks;
    keyMessage.isDisplaykeyMessageSection = req.body.isDisplaykeyMessageSection;
    keyMessage.keyMessages = req.body.keyMessages;
    keyMessage.save((err, doc) => {
        if(!err) {
        // res.send(doc);
        }
        else {
            //return next(err);
        }
    });

    var countryOverview = new CountryOverview();
    countryOverview.countryProfileID = mongoose.Types.ObjectId(req.body.countryProfileID);
    countryOverview.countryOverviewMainSection = req.body.countryOverviewMainSection;
    countryOverview.countryOverviewFooterLinks = req.body.countryOverviewFooterLinks;
    countryOverview.isDisplaycountryOverviewSection = req.body.isDisplaycountryOverviewSection;
    countryOverview.countryOverviews = req.body.countryOverviews;
    countryOverview.save((err, doc) => {
        if(!err) {
        // res.send(doc);
        }
        else {
            //return next(err);
        }
    });

    var climatology = new Climatology();
    climatology.countryProfileID = mongoose.Types.ObjectId(req.body.countryProfileID);
    climatology.climatologyMainSection = req.body.climatologyMainSection;
    climatology.climatologyFooterLinks = req.body.climatologyFooterLinks;
    climatology.isDisplayclimatologySection = req.body.isDisplayclimatologySection;
    climatology.climatologys = req.body.climatologys;
    climatology.save((err, doc) => {
        if(!err) {
        // res.send(doc);
        }
        else {
            //return next(err);
        }
    });

    var climateRelatedNaturalHazard = new ClimateRelatedNaturalHazard();
    climateRelatedNaturalHazard.countryProfileID = mongoose.Types.ObjectId(req.body.countryProfileID);
    climateRelatedNaturalHazard.climateRelatedNaturalHazardMainSection = req.body.climateRelatedNaturalHazardMainSection;
    climateRelatedNaturalHazard.climateRelatedNaturalHazardFooterLinks = req.body.climateRelatedNaturalHazardFooterLinks;
    climateRelatedNaturalHazard.isDisplayclimateRelatedNaturalHazardSection = req.body.isDisplayclimateRelatedNaturalHazardSection;
    climateRelatedNaturalHazard.climateRelatedNaturalHazards = req.body.climateRelatedNaturalHazards;
    climateRelatedNaturalHazard.save((err, doc) => {
        if(!err) {
           // res.send(doc);
        }
        else {
            //return next(err);
        }
    });
    
    var climateChangeImpact = new ClimateChangeImpact();
    climateChangeImpact.countryProfileID = mongoose.Types.ObjectId(req.body.countryProfileID);
    climateChangeImpact.climateChangeImpactMainSection = req.body.climateChangeImpactMainSection;
    climateChangeImpact.climateChangeImpactFooterLinks = req.body.climateChangeImpactFooterLinks;
    climateChangeImpact.isDisplayclimateChangeImpactSection = req.body.isDisplayclimateChangeImpactSection;
    climateChangeImpact.climateChangeImpacts = req.body.climateChangeImpacts;
    climateChangeImpact.save((err, doc) => {
        if(!err) {
        // res.send(doc);
        }
        else {
            //return next(err);
        }
    });

    var policiesAndProgram = new PoliciesAndProgram();
    policiesAndProgram.countryProfileID = mongoose.Types.ObjectId(req.body.countryProfileID);
    policiesAndProgram.policiesAndProgramMainSection = req.body.policiesAndProgramMainSection;
    policiesAndProgram.policiesAndProgramFooterLinks = req.body.policiesAndProgramFooterLinks;
    policiesAndProgram.isDisplaypoliciesAndProgramSection = req.body.isDisplaypoliciesAndProgramSection;
    policiesAndProgram.policiesAndPrograms = req.body.policiesAndPrograms;
    policiesAndProgram.save((err, doc) => {
        if(!err) {
        // res.send(doc);
        }
        else {
            //return next(err);
        }
    });

    var adaptation = new Adaptation();
    adaptation.countryProfileID = mongoose.Types.ObjectId(req.body.countryProfileID);
    adaptation.adaptationMainSection = req.body.adaptationMainSection;
    adaptation.adaptationFooterLinks = req.body.adaptationFooterLinks;
    adaptation.isDisplayadaptationSection = req.body.isDisplayadaptationSection;
    adaptation.adaptations = req.body.adaptations;
    adaptation.save((err, doc) => {
        if(!err) {
         res.send(doc);
        }
        else {
            //return next(err);
        }
    });
}



module.exports.updateSectionData = (req, res) => {
    let id = req.params.countryProfileID;

    Acknowledgement.findByIdAndUpdate(id, req.body, { upsert: true }, function (err) {
        if (err) {
            res.status(404).json({ id, status: false, message: 'failed at Acknowledgement' });
        }
    
    Foreword.findByIdAndUpdate(id, req.body, { upsert: true }, function (err) {
        if (err) {
            res.status(404).json({ id, status: false, message: 'failed at Foreword' });
        }
        KeyMessage.findByIdAndUpdate(id, req.body, { upsert: true }, function (err) {
            if (err) {
               res.status(404).json({ id, status: false, message: 'failed at keymessage' });
            }
            CountryOverview.findByIdAndUpdate(id, req.body, { upsert: true }, function (err) {
                if (err) {
                     res.status(404).json({ id, status: false, message: 'failed at country overview' });
                } 
                Climatology.findByIdAndUpdate(id, req.body, { upsert: true }, function (err) {
                    if (err) {
                        res.status(404).json({ id, status: false, message: 'failed at climatology' });
                    } 
                    ClimateRelatedNaturalHazard.findByIdAndUpdate(id, req.body, { upsert: true }, function (err) {
                        if (err) {
                            res.status(404).json({ id, status: false, message: 'failed at ClimateRelatedNaturalHazard' });
                        } 
                        ClimateChangeImpact.findByIdAndUpdate(id, req.body, { upsert: true }, function (err) {
                            if (err) {
                                res.status(404).json({ id, status: false, message: 'failed at ClimateChangeImpact' });
                            } 
                            PoliciesAndProgram.findByIdAndUpdate(id, req.body, { upsert: true }, function (err) {
                                if (err) {
                                    res.status(404).json({ id, status: false, message: 'failed at PoliciesAndProgram' });
                                } 
                                Adaptation.findByIdAndUpdate(id, req.body, { upsert: true }, function (err, post) {
                                    if (err) {
                                        res.status(404).json({ id, status: false, message: 'failed at Adaptation' });
                                    } else {
                                        res.status(200).json({ status: true, post, message: 'create section updated successfully.' });
                                    }
                                });
                            });
                        });
                    });
                });
            });
        });
    });
});

  

   



  

  


    //return res.status(200).json({ status: true, post, message: 'create section updated successfully.' });
}