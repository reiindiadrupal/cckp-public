const mongoose = require('mongoose');
const Foreword = mongoose.model('Foreword');
const passport = require('passport');
const _ = require('lodash');

module.exports.postForeword = (req,res,next) => {
    console.log(req.body.forewordMainSection);
    var foreword = new Foreword();
    foreword.forewordMainSection = req.body.forewordMainSection;
    // foreword.forewordFooterLinks = req.body.forewordFooterLinks;
    // foreword.isDisplayforewordSection = req.body.isDisplayforewordSection;
    // foreword.forewords = req.body.forewords;
    foreword.save((err, doc) => {
        if(!err) {
            res.send(doc);
        }
        else {
            console.log(err);
            return next(err);
        }
    });
}
