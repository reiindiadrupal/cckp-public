const mongoose = require('mongoose');
const CountryProfile = mongoose.model('CountryProfile');
const Acknowledgement = mongoose.model('Acknowledgement');
const Foreword = mongoose.model('Foreword');
const KeyMessage = mongoose.model('KeyMessage');
const CountryOverview = mongoose.model('CountryOverview');
const Climatology = mongoose.model('Climatology');
const ClimateRelatedNaturalHazard = mongoose.model('ClimateRelatedNaturalHazard');
const ClimateChangeImpact = mongoose.model('ClimateChangeImpact');
const PoliciesAndProgram = mongoose.model('PoliciesAndProgram');
const Adaptation = mongoose.model('Adaptation');
const passport = require('passport');
const _ = require('lodash');

module.exports.postCountryProfileData = (req,res,next) => {
    
    var countryProfile = new CountryProfile();
    countryProfile.title = req.body.title;
    countryProfile.regionName = req.body.regionName;
    countryProfile.countryName = req.body.countryName;
    countryProfile.primaryImageURL = req.body.primaryImageURL;
    countryProfile.secondaryImageURL = req.body.secondaryImageURL;
    countryProfile.wbLogoImageURL = req.body.wbLogoImageURL;
    countryProfile.adbWbLogoImageURL = req.body.adbWbLogoImageURL;
    countryProfile.isAdbProfile = req.body.isAdbProfile;
    countryProfile.isGenralProfile = req.body.isGenralProfile;
    countryProfile.pdfUrl = "";
    countryProfile.status = req.body.status;
    countryProfile.createdBy = req.body.createdBy;
    countryProfile.updatedBy = req.body.updatedBy;
    var date = new Date();
    var formattedDate = Number(date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
    countryProfile.createdDate = formattedDate;
    countryProfile.updatedDate = formattedDate;
    countryProfile.save((err, doc) => {
        if(!err) {
           res.send(doc);
        }
        else {
            return next(err);
        }
    });

}

module.exports.postUrlData = (req, res) => {
    let id = req.params.countryProfileID;
    CountryProfile.findByIdAndUpdate(id, req.body,
        (err, countryProfile) => {
            if (!countryProfile)
                return res.status(404).json({ _id: id, status: false, message: 'Country Profile record not found.' });
            else
                return res.status(200).json({ status: true, message: 'PDF url updated succcessfully.' });
        }
    );
}

module.exports.getAllCountryProfiles = (req, res, next) =>{
    CountryProfile.find(
        (err, countryProfile) => {
            if (!countryProfile)
                return res.status(404).json({ status: false, message: 'Country Profile record not found.' });
            else
                return res.status(200).json({ status: true, countryProfile : countryProfile });
        }
    );
}

module.exports.getCountryProfile = (req, res, next) => {
    let id = req.params.countryProfileID;
    countryProfileData = {};
    CountryProfile.findById(id,
        async (err, countryProfile) => {
            if (!countryProfile) {
                return res.status(404).json({ status: false, message: 'Country Profile record not found.' });
            } else {
                let acknowledgement = await Acknowledgement.findOne({ countryProfileID: id});
                let foreword = await Foreword.findOne({ countryProfileID: id});
                let keyMessage = await KeyMessage.findOne({ countryProfileID: id});
                let countryOverview = await CountryOverview.findOne({ countryProfileID: id});
                let climatology = await Climatology.findOne({ countryProfileID: id});
                let climateRelatedNaturalHazard = await ClimateRelatedNaturalHazard.findOne({ countryProfileID: id});
                let climateChangeImpact = await ClimateChangeImpact.findOne({ countryProfileID: id});
                let policiesAndProgram = await PoliciesAndProgram.findOne({ countryProfileID: id});
                let adaptation = await Adaptation.findOne({ countryProfileID: id});
                countryProfileData['countryProfile'] = countryProfile;
                countryProfileData['acknowledgement'] = acknowledgement;
                countryProfileData['foreword'] = foreword;
                countryProfileData['keyMessage'] = keyMessage;
                countryProfileData['countryOverview'] = countryOverview;
                countryProfileData['climatology'] = climatology;
                countryProfileData['climateRelatedNaturalHazard'] = climateRelatedNaturalHazard;
                countryProfileData['climateChangeImpact'] = climateChangeImpact;
                countryProfileData['policiesAndProgram'] = policiesAndProgram;
                countryProfileData['adaptation'] = adaptation;
                return res.status(200).json({ status: true, countryProfileData : countryProfileData });
            }
        }
    );
}
module.exports.deleteCountryProfile = (req, res, next) =>{
    let id = req.params.countryProfileID;
    
    CountryProfile.findByIdAndRemove({ _id: id },
        async (err, countryProfile) => {
            if (!countryProfile) {
                return res.status(404).json({ status: false, message: 'Country Profile record not found.' });
            }
            else {
                let acknowledgement = await Acknowledgement.findOneAndDelete({ countryProfileID: id});
                let foreword = await Foreword.findOneAndDelete({ countryProfileID: id});
                let keyMessage = await KeyMessage.findOneAndDelete({ countryProfileID: id});
                let countryOverview = await CountryOverview.findOneAndDelete({ countryProfileID: id});
                let climatology = await Climatology.findOneAndDelete({ countryProfileID: id});
                let climateRelatedNaturalHazard = await ClimateRelatedNaturalHazard.findOneAndDelete({ countryProfileID: id});
                let climateChangeImpact = await ClimateChangeImpact.findOneAndDelete({ countryProfileID: id});
                let policiesAndProgram = await PoliciesAndProgram.findOneAndDelete({ countryProfileID: id});
                let adaptation = await Adaptation.findOneAndDelete({ countryProfileID: id});
                return res.status(200).json({ status: true, message: 'Country Profile deleted.' });
            }
        }
    );
}

module.exports.updateCountryProfile = (req, res) => {
    let id = req.params.countryProfileID;
    CountryProfile.findByIdAndUpdate(id , req.body,
        (err, countryProfile) => {
            if (!countryProfile)
                return res.status(404).json({ _id: id,status: false, message: 'Country Profile record not found.' });
            else
                return res.status(200).json({ status: true, message: 'Country Profile updated succcessfully.' });
        }
    );
}

module.exports.getForewordsSection = (req, res, next) =>{
    let id = req.params.countryProfileID;
    countryProfileData = {};
    CountryProfile.findById(id,
        async (err, countryProfile) => {
            if (!countryProfile) {
                return res.status(404).json({ status: false, message: 'Country Profile record not found.' });
            } else {
                let foreword = await Foreword.findOne({ countryProfileID: id});
                countryProfileData['countryProfile'] = countryProfile;
                countryProfileData['foreword'] = foreword;
                return res.status(200).json({ status: true, countryProfileData : countryProfileData });
            }
        }
    );
}

module.exports.getAcknowledgementsSection = (req, res, next) =>{
    let id = req.params.countryProfileID;
    countryProfileData = {};
    CountryProfile.findById(id,
        async (err, countryProfile) => {
            if (!countryProfile) {
                return res.status(404).json({ status: false, message: 'Country Profile record not found.' });
            } else {
                let acknowledgement = await Foreword.findOne({ countryProfileID: id});
                countryProfileData['countryProfile'] = countryProfile;
                countryProfileData['acknowledgement'] = acknowledgement;
                return res.status(200).json({ status: true, countryProfileData : countryProfileData });
            }
        }
    );
}

module.exports.getKeyMessagesSection = (req, res, next) =>{
    let id = req.params.countryProfileID;
    countryProfileData = {};
    CountryProfile.findById(id,
        async (err, countryProfile) => {
            if (!countryProfile) {
                return res.status(404).json({ status: false, message: 'Country Profile record not found.' });
            } else {
                let keyMessage = await KeyMessage.findOne({ countryProfileID: id});
                countryProfileData['keyMessage'] = keyMessage;
                return res.status(200).json({ status: true, countryProfileData : countryProfileData });
            }
        }
    );
}

module.exports.getPoliciesAndProgramsSection = (req, res, next) =>{
    let id = req.params.countryProfileID;
    countryProfileData = {};
    CountryProfile.findById(id,
        async (err, countryProfile) => {
            if (!countryProfile) {
                return res.status(404).json({ status: false, message: 'Country Profile record not found.' });
            } else {
                let policiesAndProgram = await PoliciesAndProgram.findOne({ countryProfileID: id});
                countryProfileData['policiesAndProgram'] = policiesAndProgram;
                return res.status(200).json({ status: true, countryProfileData : countryProfileData });
            }
        }
    );
}

module.exports.getCountryOverviewsSection = (req, res, next) =>{
    let id = req.params.countryProfileID;
    countryProfileData = {};
    CountryProfile.findById(id,
        async (err, countryProfile) => {
            if (!countryProfile) {
                return res.status(404).json({ status: false, message: 'Country Profile record not found.' });
            } else {
                
                let countryOverview = await CountryOverview.findOne({ countryProfileID: id});
                countryProfileData['countryOverview'] = countryOverview;
                return res.status(200).json({ status: true, countryProfileData : countryProfileData });
            }
        }
    );
}

module.exports.getClimatologysSection = (req, res, next) =>{
    let id = req.params.countryProfileID;
    countryProfileData = {};
    CountryProfile.findById(id,
        async (err, countryProfile) => {
            if (!countryProfile) {
                return res.status(404).json({ status: false, message: 'Country Profile record not found.' });
            } else {
                let climatology = await Climatology.findOne({ countryProfileID: id});
                countryProfileData['climatology'] = climatology;
                return res.status(200).json({ status: true, countryProfileData : countryProfileData });
            }
        }
    );
}

module.exports.getClimateRelatedNaturalHazardsSection = (req, res, next) =>{
    let id = req.params.countryProfileID;
    countryProfileData = {};
    CountryProfile.findById(id,
        async (err, countryProfile) => {
            if (!countryProfile) {
                return res.status(404).json({ status: false, message: 'Country Profile record not found.' });
            } else {
                let climateRelatedNaturalHazard = await ClimateRelatedNaturalHazard.findOne({ countryProfileID: id});
                countryProfileData['climateRelatedNaturalHazard'] = climateRelatedNaturalHazard;
                return res.status(200).json({ status: true, countryProfileData : countryProfileData });
            }
        }
    );
}

module.exports.getClimateChangeImpactsSection = (req, res, next) =>{
    let id = req.params.countryProfileID;
    countryProfileData = {};
    CountryProfile.findById(id,
        async (err, countryProfile) => {
            if (!countryProfile) {
                return res.status(404).json({ status: false, message: 'Country Profile record not found.' });
            } else {
                let climateChangeImpact = await ClimateChangeImpact.findOne({ countryProfileID: id});
                countryProfileData['climateChangeImpact'] = climateChangeImpact;
                return res.status(200).json({ status: true, countryProfileData : countryProfileData });
            }
        }
    );
}

module.exports.getAdaptationsSection = (req, res, next) =>{
    let id = req.params.countryProfileID;
    countryProfileData = {};
    CountryProfile.findById(id,
        async (err, countryProfile) => {
            if (!countryProfile) {
                return res.status(404).json({ status: false, message: 'Country Profile record not found.' });
            } else {
                let adaptation = await Adaptation.findOne({ countryProfileID: id});
                countryProfileData['adaptation'] = adaptation;
                return res.status(200).json({ status: true, countryProfileData : countryProfileData });
            }
        }
    );
}