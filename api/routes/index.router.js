const express = require('express');
const jwtHelper = require('../config/jwtHelper');
const router  = express.Router();
const ctrlUser = require('../controllers/user.controller');
const countryProfile = require('../controllers/countryProfile.controller');
const countryList = require('../controllers/countryList.controller');
const postSections = require('../controllers/postSections.controller');
const image = require('../controllers/image.controller');
var multer  = require('multer');  
var path = require('path'); 
const basicAuth = require('../config/basicAuth');
const storage = multer.diskStorage({  
    destination: function (req, file, callback) {  
      console.log(file);
      callback(null,path.join(__dirname, '../uploads'));  
    },  
    filename: function (req, file, callback) {  
      //callback(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
     callback(null, file.originalname);  //old code
    }  
});  
const upload = multer({ storage : storage});  

router.post('/register', ctrlUser.register);
router.post('/authenticate', ctrlUser.authenticate);
router.get('/userProfile', jwtHelper.verifyJwtToken, ctrlUser.userProfile);
router.post('/countryProfile',basicAuth, countryProfile.postCountryProfileData);
router.post('/updatecountryProfile/:countryProfileID', basicAuth, countryProfile.updateCountryProfile);
router.post('/updateSection/:countryProfileID',basicAuth, postSections.updateSectionData);
router.post('/postSections',basicAuth, postSections.postSectionsData);
router.post('/postImage',basicAuth,upload.single('UploadFiles'), image.postImage);
router.post('/postPrimaryImage', basicAuth,upload.single('primaryImage'),image.postPrimaryImage);
router.post('/postSecondaryImage',basicAuth,upload.single('secondaryImage'), image.postSecondaryImage);
router.post('/postWbLogoImage',basicAuth, upload.single('wbLogo'), image.postWbLogoImage);
router.post('/postAdbWbLogoImage',basicAuth, upload.single('adbWbLogo'), image.postAdbWbLogoImage);
router.post('/postUrl/:countryProfileID',basicAuth, countryProfile.postUrlData);

router.get('/allCountryProfiles',basicAuth, countryProfile.getAllCountryProfiles);
router.get('/allCountryLists',basicAuth, countryList.getAllCountryLists);
router.get('/countryProfile/:countryProfileID', countryProfile.getCountryProfile);
router.get('/getAcknowledgementsSection/:countryProfileID', countryProfile.getAcknowledgementsSection);
router.get('/getForewordsSection/:countryProfileID', countryProfile.getForewordsSection);
router.get('/getKeyMessagesSection/:countryProfileID', countryProfile.getKeyMessagesSection);
router.get('/getPoliciesAndProgramsSection/:countryProfileID', countryProfile.getPoliciesAndProgramsSection);
router.get('/getCountryOverviewsSection/:countryProfileID', countryProfile.getCountryOverviewsSection);
router.get('/getClimatologysSection/:countryProfileID', countryProfile.getClimatologysSection);
router.get('/getClimateRelatedNaturalHazardsSection/:countryProfileID', countryProfile.getClimateRelatedNaturalHazardsSection);
router.get('/getClimateChangeImpactsSection/:countryProfileID', countryProfile.getClimateChangeImpactsSection);
router.get('/getAdaptationsSection/:countryProfileID', countryProfile.getAdaptationsSection);

router.get('/removeCountryProfile/:countryProfileID',basicAuth, countryProfile.deleteCountryProfile);


module.exports = router;