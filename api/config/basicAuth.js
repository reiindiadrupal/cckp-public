module.exports = basicAuth;
async function basicAuth(req, res, next) {
    //check for basic auth header
    if(process.env.isBasicAuth === "true"){
        if (!req.headers.authorization || req.headers.authorization.indexOf('Bearer ') === -1) {
            return res.status(401).json({ message: 'Unauthorized' });
        }
    }
    next();
}