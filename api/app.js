require('./config/config');
require('./models/db');
require('./config/passportConfig');

const https = require('https');
const fs = require('fs');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const rtsIndex = require('./routes/index.router');
const basicAuth = require('./config/basicAuth');
const passport = require('passport');
// const options = {
//     key: fs.readFileSync(process.env.privkey),
//     cert: fs.readFileSync(process.env.fullchain),
// };
var app = express();

// middleware

app.use(bodyParser.json());
app.use('/uploads', express.static(__dirname + '/uploads'));
app.use(cors());
//app.use(passport.initialize());
//app.use(basicAuth);
app.use('/api',rtsIndex);
  
// error handler
app.use((err, req, res, next) => {
    if (err.name === 'ValidationError') {
        var valErrors = [];
        Object.keys(err.errors).forEach(key => valErrors.push(err.errors[key].message));
        res.status(422).send(valErrors)
    }
});


// start server
// app.listen(process.env.PORT, () => console.log(`Server started at port : ${process.env.PORT}`));

https.createServer({}, app).listen(process.env.PORT, () => {
    console.log(`Server started at port : ${process.env.PORT}`)
});

