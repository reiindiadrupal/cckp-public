const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

var countryProfileSchema = new mongoose.Schema({
    title:{
        type:String,
    },
    regionName:{
        type:String,
    },
    countryName:{
        type:String,
    },
    primaryImageURL:{
        type:String,
    },
    secondaryImageURL:{
        type:String,
    },
    wbLogoImageURL:{
        type:String,
    },
    adbWbLogoImageURL:{
        type:String,
    },
    isAdbProfile:{
        type:Boolean,
    },
    isGenralProfile: {
        type: Boolean,
    },
    pdfUrl: {
        type: String,
    },
    createdBy:{
        type:String,
    },
    updatedBy:{
        type:String,
    },
    status:{
        type:Number,
    },
    createdDate: {
        type: String,
    },
    updatedDate: {
        type: String,
    }
    },
    {
    collection: 'countryProfile'
});



mongoose.model('CountryProfile',countryProfileSchema);