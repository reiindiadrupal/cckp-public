const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

var climatologySchema = new mongoose.Schema({
    countryProfileID:{
        type: mongoose.ObjectId ,
    },
    climatologyMainSection:{
        type:String,
    },
    climatologyFooterLinks:{
        type:String,
    },
    isDisplayclimatologySection:{
        type:Boolean,
    },
    isCheckedclimatologySection: {
        type: Boolean,
    },
    climatologys:{
        type:Array,
    }
    },
    {
    collection: 'climatology'
});



mongoose.model('Climatology',climatologySchema);