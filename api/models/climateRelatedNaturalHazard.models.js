const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

var climateRelatedNaturalHazardSchema = new mongoose.Schema({
    countryProfileID:{
        type: mongoose.ObjectId ,
    },
    climateRelatedNaturalHazardMainSection:{
        type:String,
    },
    climateRelatedNaturalHazardFooterLinks:{
        type:String,
    },
    isDisplayclimateRelatedNaturalHazardSection:{
        type:Boolean,
    },
    isCheckedclimateRelatedNaturalHazardSection: {
        type: Boolean,
    },
    climateRelatedNaturalHazards:{
        type:Array,
    }
    },
    {
    collection: 'climateRelatedNaturalHazard'
});



mongoose.model('ClimateRelatedNaturalHazard',climateRelatedNaturalHazardSchema);