const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

var countryOverviewSchema = new mongoose.Schema({
    countryProfileID:{
        type: mongoose.ObjectId ,
    },
    countryOverviewMainSection:{
        type:String,
    },
    countryOverviewFooterLinks:{
        type:String,
    },
    isDisplaycountryOverviewSection:{
        type:Boolean,
    },
    isCheckedcountryOverviewSection: {
        type: Boolean,
    },
    countryOverviews:{
        type:Array,
    }
    },
    {
    collection: 'countryOverview'
});



mongoose.model('CountryOverview',countryOverviewSchema);