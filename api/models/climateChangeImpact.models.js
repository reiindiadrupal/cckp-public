const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

var climateChangeImpactSchema = new mongoose.Schema({
    countryProfileID:{
        type: mongoose.ObjectId ,
    },
    climateChangeImpactMainSection:{
        type:String,
    },
    climateChangeImpactFooterLinks:{
        type:String,
    },
    isDisplayclimateChangeImpactSection:{
        type:Boolean,
    },
    isCheckedclimateChangeImpactSection: {
        type: Boolean,
    },
    climateChangeImpacts:{
        type:Array,
    }
    },
    {
    collection: 'climateChangeImpact'
});



mongoose.model('ClimateChangeImpact',climateChangeImpactSchema);