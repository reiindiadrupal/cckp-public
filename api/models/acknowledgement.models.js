const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

var acknowledgementSchema = new mongoose.Schema({
    countryProfileID:{
        type: mongoose.ObjectId ,
    },
    acknowledgementMainSection:{
        type:String,
    },
    isDisplayAcknowledgementSection:{
        type:Boolean,
    },
    isCheckedAcknowledgementSection: {
        type: Boolean,
    },
    },
    {
    collection: 'acknowledgement'
});

mongoose.model('Acknowledgement', acknowledgementSchema);