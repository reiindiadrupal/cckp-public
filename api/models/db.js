const mongoose = require('mongoose');
mongoose.connect(process.env.MONGODB_URI,{ useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
    if (!err) {
        console.log('MongoDB connection succeeded.');
    } 
    else {
        console.log('Error in MongoDB connection' + JSON.stringify(err, undefined, 2));
    } 
});

require('./user.models');
require('./countryProfile.models');
require('./countriesList.models');
require('./acknowledgement.models');
require('./foreword.models');
require('./keyMessage.models');
require('./countryOverview.models');
require('./climatology.models');
require('./climateRelatedNaturalHazard.models');
require('./climateChangeImpact.models');
require('./policiesAndProgram.models');
require('./adaptation.models');

