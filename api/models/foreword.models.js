const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

var forewordSchema = new mongoose.Schema({
    countryProfileID:{
        type: mongoose.ObjectId ,
    },
    forewordMainSection:{
        type:String,
    },
    forewordFooterLinks:{
        type:String,
    },
    isDisplayforewordSection:{
        type:Boolean,
    },
    isCheckedforewordSection: {
        type: Boolean,
    },
    forewords:{
        type:Array,
    }
    },
    {
    collection: 'foreword'
});



mongoose.model('Foreword',forewordSchema);