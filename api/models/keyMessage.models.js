const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

var keyMessageSchema = new mongoose.Schema({
    countryProfileID:{
        type: mongoose.ObjectId ,
    },
    keyMessageMainSection:{
        type:String,
    },
    keyMessageFooterLinks:{
        type:String,
    },
    isDisplaykeyMessageSection:{
        type:Boolean,
    },
    isCheckedkeyMessageSection: {
        type: Boolean,
    },
    keyMessages:{
        type:Array,
    }
    },
    {
    collection: 'keyMessage'
});



mongoose.model('KeyMessage',keyMessageSchema);