const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

var adaptationSchema = new mongoose.Schema({
    countryProfileID:{
        type: mongoose.ObjectId ,
    },
    adaptationMainSection:{
        type:String,
    },
    adaptationFooterLinks:{
        type:String,
    },
    isDisplayadaptationSection:{
        type:Boolean,
    },
    isCheckedadaptationSection: {
        type: Boolean,
    },
    adaptations:{
        type:Array,
    }
    },
    {
    collection: 'adaptation'
});



mongoose.model('Adaptation',adaptationSchema);