const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

var policiesAndProgramSchema = new mongoose.Schema({
    countryProfileID:{
        type: mongoose.ObjectId ,
    },
    policiesAndProgramMainSection:{
        type:String,
    },
    policiesAndProgramFooterLinks:{
        type:String,
    },
    isDisplaypoliciesAndProgramSection:{
        type:Boolean,
    },
    isCheckedpoliciesAndProgramSection: {
        type: Boolean,
    },
    policiesAndPrograms:{
        type:Array,
    }
    },
    {
    collection: 'policiesAndProgram'
});



mongoose.model('PoliciesAndProgram',policiesAndProgramSchema);