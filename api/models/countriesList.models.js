const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

var countriesListSchema = new mongoose.Schema(
  {
    region: {
      type: String,
    },
    country: {
      type: String,
    },
    code: {
      type: String,
    },
  },
  {
    collection: "countriesList",
  }
);

mongoose.model("CountriesList", countriesListSchema);
