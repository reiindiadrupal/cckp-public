(function(window) {
	window["env"] = window["env"] || {};
  
	// Environment variables
	window["env"]["clientId"] = "39e0d991-98a7-4aef-ba54-eefe59b4e5d6";
	window["env"]["tenantId"] = "31996441-7546-4120-826b-df0c3e239671";
	window["env"]["appUrl"] = "https://ec2-52-4-140-142.compute-1.amazonaws.com";
	window["env"]["apiBaseUrl"] = "https://ec2-52-4-140-142.compute-1.amazonaws.com";
	window["env"]["apiUploadFileUrl"] = "https://ec2-52-4-140-142.compute-1.amazonaws.com";

  })(this);