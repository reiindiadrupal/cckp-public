(function(window) {
	window.env = window.env || {};
  
	// Environment variables
	window["env"]["clientId"] = "${CLIENT_ID}";
	window["env"]["tenantId"] = "${TENANT_ID}";
	window["env"]["appUrl"] = "${API_URL}";
	window["env"]["apiBaseUrl"] = "${API_BASE_URL}";
	window["env"]["apiUploadFileUrl"] = "${API_UPLOAD_FILE_URL}";
})(this);