export const environment = {
  production: true,
  isBasicAuth: true,

 // docker image variable
  clientId: '39e0d991-98a7-4aef-ba54-eefe59b4e5d6',
  tenanatId: '31996441-7546-4120-826b-df0c3e239671',                    
  appName: 'CCKP',
  appUrl: 'https://qa1.reisystems.in',

  //winnovative server URL
  pdfcreatorServerUrl: 'https://qa2.reisystems.in/api/pdf',
  //productions API URL
  apiBaseUrl :'https://qa1.reisystems.in:3000/api',
  apiUploadFileUrl :'https://qa1.reisystems.in:3000/uploads/',
  conversionDelay :'20',
  navigationtimeout:'90',
  
  //local API URL
  //apiBaseUrl:'http://192.168.43.151:3000/api',
  //apiUploadFileUrl:'http://192.168.43.151:3000/uploads/',

  //Table 2 API Url
  T2tasURl:'https://climatedata.worldbank.org/climateweb/rest/v1/cru/country/tas/1901-2016/',
  T2prURl:'https://climatedata.worldbank.org/climateweb/rest/v1/cru/country/pr/1901-2016/',
  T2tasmaxURl:'https://climatedata.worldbank.org/climateweb/rest/v1/cru/country/tasmax/1901-2016/',
  T2tasminURl:'https://climatedata.worldbank.org/climateweb/rest/v1/cru/country/tasmin/1901-2016/',


  //Table 3 tas P10 urls
  T3tasP101stUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/tas/p10/rcp85/2020/2039/",
  T3tasP102ndUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/tas/p10/rcp85/2040/2059/",
  T3tasP103rdUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/tas/p10/rcp85/2060/2079/",
  T3tasP104thUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/tas/p10/rcp85/2080/2099/",

  //Table 3 tas median urls
  T3tasMedian1stUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/tas/median/rcp85/2020/2039/",
  T3tasMedian2ndUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/tas/median/rcp85/2040/2059/",
  T3tasMedian3rdUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/tas/median/rcp85/2060/2079/",
  T3tasMedian4thUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/tas/median/rcp85/2080/2099/",

  //Table 3 tas P90 Url
  T3tasP901stUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/tas/p90/rcp85/2020/2039/",
  T3tasP902ndUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/tas/p90/rcp85/2040/2059/",
  T3tasP903rdUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/tas/p90/rcp85/2060/2079/",
  T3tasP904thUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/tas/p90/rcp85/2080/2099/",



  //Table 3 pr P10 Url
  T3prP101stUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/pr/p10/rcp85/2020/2039/",
  T3prP102ndUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/pr/p10/rcp85/2040/2059/",
  T3prP103rdUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/pr/p10/rcp85/2060/2079/",
  T3prP104thUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/pr/p10/rcp85/2080/2099/",

  //Table 3 pr median urls
  T3prMedian1stUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/pr/median/rcp85/2020/2039/",
  T3prMedian2ndUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/pr/median/rcp85/2040/2059/",
  T3prMedian3rdUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/pr/median/rcp85/2060/2079/",
  T3prMedian4thUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/pr/median/rcp85/2080/2099/",

  //Table 3 pr P90 Url
  T3prP901stUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/pr/p90/rcp85/2020/2039/",
  T3prP902ndUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/pr/p90/rcp85/2040/2059/",
  T3prP903rdUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/pr/p90/rcp85/2060/2079/",
  T3prP904thUrl: "https://climatedata.worldbank.org/climateweb/rest/v1/AR5Data/country/ensemble/pr/p90/rcp85/2080/2099/"


};
