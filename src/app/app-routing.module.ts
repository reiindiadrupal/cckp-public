import { UpdateCountryProfileComponent } from './country-profile/update-country-profile/update-country-profile.component';
import { UpdateSectionComponent } from './country-profile/update-section/update-section.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CreateCountryProfileComponent } from './country-profile/create-country-profile/create-country-profile.component';
import { CreateSectionComponent } from './country-profile/create-section/create-section.component';
import { PdfReportComponent } from './country-profile/pdf-report/pdf-report.component';
import { GetFirstPageComponent } from './country-profile/get-first-page/get-first-page.component';
import { GetSecondPageComponent } from './country-profile/get-second-page/get-second-page.component';
import { PdfComponent } from './country-profile/pdf/pdf.component';
import { PreviewPdfComponent } from './country-profile/preview-pdf/preview-pdf.component';
import { MsalGuard } from '@azure/msal-angular';
import { environment } from '../environments/environment';
let routes: Routes;
if(environment.isBasicAuth){
  routes = [
    { 
      path: '', component: HomeComponent,canActivate: [ MsalGuard ],
    },
    { 
      path: 'home', component: HomeComponent,canActivate: [ MsalGuard ]
    },
    { 
      path: 'create-country-profile/create-section/:countryName/:countryProfileID', component: CreateSectionComponent,canActivate: [ MsalGuard ],
    },
    { 
      path: 'create-country-profile', component: CreateCountryProfileComponent,canActivate: [ MsalGuard ],
    },
    { 
      path: 'pdfReport/content/:countryProfileID', component: PdfReportComponent,
    },
    { 
      path: 'pdfReport/getFirstPage/:countryProfileID', component: GetFirstPageComponent,
    },
    { 
      path: 'pdfReport/getSecondPage/:countryProfileID', component: GetSecondPageComponent,
    },
    { 
      path: 'pdf/:countryProfileID/:countryName', component: PdfComponent,canActivate: [ MsalGuard ],
    },
    {
      path: 'updateCountryProfile/:countryProfileID', component: UpdateCountryProfileComponent,canActivate: [ MsalGuard ],
    },
    {
      path: 'update-country-profile/update-section/:countryName/:countryProfileID', component:UpdateSectionComponent,canActivate: [ MsalGuard ],
    },
    {
      path: 'preview-pdf/:countryProfileID', component: PreviewPdfComponent,canActivate: [ MsalGuard ],
    }
  ];
}else {
  routes = [
    { 
      path: '', component: HomeComponent,
    },
    { 
      path: 'home', component: HomeComponent,
    },
    { 
      path: 'create-country-profile/create-section/:countryName/:countryProfileID', component: CreateSectionComponent,
    },
    { 
      path: 'create-country-profile', component: CreateCountryProfileComponent,
    },
    { 
      path: 'pdfReport/content/:countryProfileID', component: PdfReportComponent,
    },
    { 
      path: 'pdfReport/getFirstPage/:countryProfileID', component: GetFirstPageComponent,
    },
    { 
      path: 'pdfReport/getSecondPage/:countryProfileID', component: GetSecondPageComponent,
    },
    { 
      path: 'pdf/:countryProfileID/:countryName', component: PdfComponent,
    },
    {
      path: 'updateCountryProfile/:countryProfileID', component: UpdateCountryProfileComponent,
    },
    {
      path: 'update-country-profile/update-section/:countryName/:countryProfileID', component:UpdateSectionComponent,
    },
    {
      path: 'preview-pdf/:countryProfileID', component: PreviewPdfComponent,
    }
  ];
}

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy:PreloadAllModules
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
