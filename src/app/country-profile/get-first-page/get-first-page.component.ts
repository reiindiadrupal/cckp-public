import { Component, ViewChild, OnInit, ElementRef, NgZone, ViewEncapsulation  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CreatecountryprofileService } from '../../shared/service/createcountryprofile.service';

@Component({
  selector: 'app-get-first-page',
  templateUrl: './get-first-page.component.html',
  styleUrls: ['./get-first-page.component.css'],
  encapsulation: ViewEncapsulation.None,
  
})
export class GetFirstPageComponent implements OnInit {
  countryProfileID: string;
  countryName: string;
  primaryImageURL: string;
  secondaryImageURL: string;
  wbLogoImageURL: string;
  adbWbLogoImageURL: string;
  primaryImageURLSrc: string;
  secondaryImageURLSrc: string;
  isAdbProfile: Boolean;
  isGenralProfile:boolean;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router, 
    private countryProfile: CreatecountryprofileService,
    private ngZone: NgZone,
  ) {
    this.activatedRoute.params.subscribe(params => {
      this.countryProfileID = params.countryProfileID;
  });

 }

 ngOnInit() {
  this.countryProfile.getCountryProfile(this.countryProfileID).subscribe(
    res => {
      this.countryName = res['countryProfileData'].countryProfile.countryName;
      console.log(res['countryProfileData']);
      this.primaryImageURL = res['countryProfileData'].countryProfile.primaryImageURL;
      if(this.primaryImageURL){
        this.primaryImageURLSrc = this.primaryImageURL;
      } else {
        this.primaryImageURLSrc = "./../../../assets/images/cover/4.jpg";
      }
      this.secondaryImageURL = res['countryProfileData'].countryProfile.secondaryImageURL;
      if(this.secondaryImageURL){
        this.secondaryImageURLSrc = this.secondaryImageURL;
      } else {
        this.secondaryImageURLSrc = "./../../../assets/images/cover/left.jpg";
      }
      this.wbLogoImageURL = res['countryProfileData'].countryProfile.wbLogoImageURL;
      this.adbWbLogoImageURL = res['countryProfileData'].countryProfile.adbWbLogoImageURL;
      this.isAdbProfile = res['countryProfileData'].countryProfile.isAdbProfile;
      this.isGenralProfile = res['countryProfileData'].countryProfile.isGenralProfile;
    },
    err => { 
      console.log(err);
    }
  );
  
   
}
 
}
