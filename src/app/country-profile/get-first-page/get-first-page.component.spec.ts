import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetFirstPageComponent } from './get-first-page.component';

describe('GetFirstPageComponent', () => {
  let component: GetFirstPageComponent;
  let fixture: ComponentFixture<GetFirstPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetFirstPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetFirstPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
