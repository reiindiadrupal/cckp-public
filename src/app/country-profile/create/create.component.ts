import { Component, OnInit, NgZone } from '@angular/core';
import { FormArray, FormsModule, FormGroup, FormBuilder, Validators, FormArrayName } from "@angular/forms";
import { Router } from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  
})
export class CreateComponent implements OnInit {
  
  createSection = {
    "foreword": {
        "content": 
         [
          { 
            "title" : "title1",
            "html" : "html1" 
          },
          { 
            "title" : "title2",
            "html" : "html2" 
          },
          { 
            "title" : "title3",
            "html" : "html3" 
          },
        ],
        "linkes":"linkes",
        "non":"non",
      }
    };


  public form: FormGroup;
  public contactList: FormArray;
  
  // returns all form groups under contacts
  get contactFormGroup() {
    return this.form.get('contacts') as FormArray;
  }

  constructor(private fb: FormBuilder) {}
  
  ngOnInit() {
     
      let array = this.createSection.foreword.content;

      if(array.length > 0 ){ 
        this.form = this.fb.group({
          non: '',
          linkes: '',
          contacts: this.fb.array([])
        });
        this.form.patchValue({non:this.createSection.foreword.non,linkes: this.createSection.foreword.linkes });
        this.createSection.foreword.content.forEach(t => {
        //var teacher: FormGroup = this.createContact();
        this.contacts().push(this.fb.group(t));
      });
      } else {
         //sub section added 
          this.form = this.fb.group({
            non: '',
            linkes: '',
            contacts: this.fb.array([])
          });
      }

      this.contactList = this.form.get('contacts') as FormArray;
  }
  contacts(): FormArray {
    return this.form.get('contacts') as FormArray;
  }
 

  // contact formgroup
  createContact(): FormGroup {
    return this.fb.group({
      html:'',
      title: '',
    });
  }

  // add a contact form group
  addContact() {
    console.log(this.createSection.foreword.content.length);
    //this.createSection.foreword.content.push({ "title" : "","html" : "" });
    this.contactList.push(this.createContact());
  }

  // remove contact from group
  removeContact(index) {
    this.contactList.removeAt(index);
  }

  getContactsFormGroup(index): FormGroup {
    
    const formGroup = this.contactList.controls[index] as FormGroup;
    return formGroup;
  }
  // method triggered when form is submitted
  submit() {
    console.log(this.form.value);
  }
}
