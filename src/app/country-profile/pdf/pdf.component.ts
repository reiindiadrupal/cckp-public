import { Component, ViewChild, OnInit, ElementRef, NgZone, ViewEncapsulation  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CreatecountryprofileService } from '../../shared/service/createcountryprofile.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.css'],
  encapsulation: ViewEncapsulation.None,  
})
export class PdfComponent implements OnInit {
  loading = true;
  countryName: string;
  countryProfileID: string;
  pdfSrc:string;
  constructor( 
    private activatedRoute: ActivatedRoute,
    private router: Router, 
    private countryProfiles: CreatecountryprofileService,
    private ngZone: NgZone,
  ) {
      this.activatedRoute.params.subscribe(params => {
        this.countryProfileID = params.countryProfileID;
        this.countryName = params.countryName;
    });

   }

  ngOnInit() {
    const pdfReportContent = {
      "profilename": this.countryName+".pdf",
      "conversionDelay" : 60,
      "navigationtimeout":40,
      "footerText": "<table style='width: 100%'><tr><td style = 'width: 90%'><p><span style = 'color: rgb(47, 84, 150);text-decoration: inherit;'><strong> CLIMATE </ strong ></span><span style = 'color: rgb(47, 84, 150); text-decoration: inherit;'><strong> RISK COUNTRY PROFILE — Philippines </strong></ span><span style = 'color: rgb(47, 84, 150); text-decoration: inherit;'><strong></strong></span></p></td><td> <span style = 'color: navy; font-weight: bold'> &p;</span></tr></table> ",
      "sectionlist": [
          {
            "sectiontitle": "firstcoverpage",
           //"sectionurl": environment.appUrl+"/pdfReport/getFirstPage/"+this.countryProfileID
           "sectionurl":"http://15.206.234.47/pdfReport/getFirstPage/5f22534efdec3e70a558ccff"
          },
          {
            "sectiontitle": "acknowledgement",
            //"sectionurl":environment.appUrl+"/pdfReport/getSecondPage/"+this.countryProfileID
            "sectionurl":"http://15.206.234.47/pdfReport/getKeyMessagesSection/5f22534efdec3e70a558ccff"
          },
          {
            "sectiontitle":"content",
            //"sectionurl":environment.appUrl+"/pdfReport/content/"+this.countryProfileID
            "sectionurl":"http://15.206.234.47/pdfReport/getForewordsSection/5f28f008fdec3e70a558cd09"
          },
          {
            "sectiontitle": "lastcoverpage",
           // "sectionurl":environment.appUrl+"/pdfReport/getFirstPage/"+this.countryProfileID
           "sectionurl":"http://15.206.234.47/pdfReport/getFirstPage/5f22534efdec3e70a558ccff"
          }
      ]
    }
    this.countryProfiles.getPDFReport(pdfReportContent).subscribe(
      res => {
        this.loading = false;
        console.log(res);
        console.log(res['URL']);
      
        window.open(res['URL']);
      },
      err => { 
        console.log(err);
      }
    );
  }
}
