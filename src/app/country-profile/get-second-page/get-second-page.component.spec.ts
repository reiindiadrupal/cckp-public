import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetSecondPageComponent } from './get-second-page.component';

describe('GetSecondPageComponent', () => {
  let component: GetSecondPageComponent;
  let fixture: ComponentFixture<GetSecondPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetSecondPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetSecondPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
