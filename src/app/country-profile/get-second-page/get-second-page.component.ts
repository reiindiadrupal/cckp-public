import { Component, ViewChild, OnInit, ElementRef, NgZone, ViewEncapsulation  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CreatecountryprofileService } from '../../shared/service/createcountryprofile.service';
import { CreatesectionsService } from '../../shared/service/createsections.service';
declare var $: any;

@Component({
  selector: 'app-get-second-page',
  templateUrl: './get-second-page.component.html',
  styleUrls: ['./get-second-page.component.css'],
  encapsulation: ViewEncapsulation.None,
})

export class GetSecondPageComponent implements OnInit {
  countryName: string;
  countryProfileID: string;
  acknowledgementMainSection :any;
  isDisplayAcknowledgementSection  : boolean;

    constructor( 
      private activatedRoute: ActivatedRoute,
      private router: Router, 
      private countryProfile: CreatecountryprofileService,
      private countrySections: CreatesectionsService,
      private ngZone: NgZone,
    ) {
        this.activatedRoute.params.subscribe(params => {
          this.countryProfileID = params.countryProfileID;
      });
     }
   

  ngOnInit() {
    this.countryProfile.getCountryProfile(this.countryProfileID).subscribe(
      res => {
        this.countryName = res['countryProfileData'].countryProfile.countryName;
        this.acknowledgementMainSection = res['countryProfileData'].acknowledgement.acknowledgementMainSection;
        this.isDisplayAcknowledgementSection = res['countryProfileData'].acknowledgement.isDisplayAcknowledgementSection;
      },
      err => { 
        console.log(err);
      }
    );
    
  }
}