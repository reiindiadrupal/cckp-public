import { constants } from './../../config/constants';
import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CreatecountryprofileService } from '../../shared/service/createcountryprofile.service';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { environment } from '../../../environments/environment';
declare var $: any;
@Component({
  selector: 'app-country-details',
  templateUrl: './update-country-profile.component.html',
  styleUrls: ['./update-country-profile.component.css']
})
export class UpdateCountryProfileComponent implements OnInit {
  countryProfileID: string;
  countryName: string = "";
  regionName: string = "";
  title: string = "";  
  Disabled: boolean = true;
  isSubmitted = false;
  createCountryProfileForm: FormGroup;
  serverErrorMessages: string;

  isRegionselected = true;
  primaryImage: string;
  secondaryImage: string;
  primaryImageUrl: string;
  secondaryImageUrl: string;
  wbLogoImage: string;
  adbWbLogoImage: string;
  wbLogoImageUrl: string;
  adbWbLogoImageUrl: string;
  selectedFile2: string;
  file:File;
  updateComponentData = constants.updateCountryProfileComponent;
  loading = false;
  contentEditable = true;
  showSucessMessage:boolean;
  primaryImageSetURL: String;
  secondaryImageSetURL: String;
  wbLogoImageSetURL: String;
  adbWbLogoImageSetURL: String;
  isAdbProfile = false;
  isGenralProfile = false;
  isDisplayPrimaryImageSetURL = false;
  isDisplaySecondaryImageSetURL = false;
  isDisplayWbLogoImageSetURL = false;
  isDisplayAdbWbLogoImageSetURL = false;


  constructor(
    private activatedRoute: ActivatedRoute,
    private createcountryprofileService: CreatecountryprofileService,
    private router: Router,
    private ngZone: NgZone,
    public fb: FormBuilder
) {
    this.activatedRoute.params.subscribe(params => {
      this.countryProfileID = params.countryProfileID;
    });
    this.mainForm();
   }

  ngOnInit() {
    window.scrollTo(0, 0) //scroll to top when page loads
    this.getCountryData();
  }

  ngOnDestroy() {
    //debugger;
    if ($('h1,h2,h3').text() === '') {
        $(this).remove();
        console.log("removed")
      }
  }

  mainForm() {
    this.createCountryProfileForm = this.fb.group({
      title: ['', [Validators.required]],
      regionName: ['', [Validators.required]],
      countryName: ['', [Validators.required]],
      primaryImageURL:  [''],
      secondaryImageURL: [''],
      wbLogoImageURL: [''],
      adbWbLogoImageURL:[''],
      isAdbProfile: [''],
      isGenralProfile: [''],
      updatedDate: new Date(),
      status: 1,
      pdfUrl: null
    })
  }
  // Getter to access form control
  get myCountryProfileForm() {
    return this.createCountryProfileForm.controls;
  }

  getCountryData = () => {
    this.createcountryprofileService.getCountryProfile(this.countryProfileID).subscribe(
      res => {
        this.createCountryProfileForm.patchValue({
          title: res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.title.trim(),
          regionName: res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.regionName,
          countryName: res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.countryName,
          primaryImageURL: res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.primaryImageURL,
          secondaryImageURL: res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.secondaryImageURL,
          wbLogoImageURL: res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.wbLogoImageURL,
          adbWbLogoImageURL: res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.adbWbLogoImageURL,
          isAdbProfile: res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.isAdbProfile,
          isGenralProfile: res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.isGenralProfile
        });

        this.title = res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.title.trim();
        this.regionName = res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.regionName;
        this.countryName = res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.countryName;
        this.primaryImageUrl = res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.primaryImageURL;
        this.secondaryImageUrl = res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.secondaryImageURL;
        this.wbLogoImageUrl = res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.wbLogoImageURL;
        this.adbWbLogoImageUrl = res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.adbWbLogoImageURL;
        this.isAdbProfile = res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.isAdbProfile;
        this.isGenralProfile = res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.isGenralProfile;
        console.log('test', this.primaryImageUrl);
        
      },
      err => {
        console.log(err);
      }
    );
  }

  isAdbProfileEvent(event) {
    if ( event.target.checked ) {
        this.contentEditable = true;
        this.isAdbProfile = true;
        this.isGenralProfile = false;
  } else {
    this.isAdbProfile = false;
    this.contentEditable = false;
    this.isGenralProfile = true;
  }
}

isGenralProfileEvent(event) {
  if ( event.target.checked ) {
      this.isGenralProfile = true;
      this.isAdbProfile = false;
    this.contentEditable = false;
         
} else {
  this.isAdbProfile = true;
  this.isGenralProfile = false;
  this.contentEditable = true;
  
}
}

  onSelectPrimaryImage = (event) =>{
    if (event.target.files && event.target.files[0]) {
      const imageData = new FormData();
      const file = event.target.files[0];
      imageData.append('primaryImage', file,file.name);
      this.createcountryprofileService.postPrimaryImage(imageData).subscribe(
          res => {
            console.log(res);
            this.primaryImageSetURL = environment.apiUploadFileUrl+res['filename'];
          }
      );
      const fileReader: FileReader = new FileReader();
      fileReader.readAsDataURL(event.target.files[0]); // read file as data url
      fileReader.onload = (event) => { // called once readAsDataURL is completed
        this.isDisplayPrimaryImageSetURL = true;
        this.primaryImageUrl = fileReader.result as string;
      }
    }
  }
 
  onSelectSecondaryImage = (event) => {
    if (event.target.files && event.target.files[0]) {
      const imageData = new FormData();
      const file = event.target.files[0];
      imageData.append('secondaryImage', file,file.name);
      this.createcountryprofileService.postSecondaryImage(imageData).subscribe(
          res => {
            console.log(res);
            this.secondaryImageSetURL = environment.apiUploadFileUrl+res['filename'];
          }
      );
      const fileReader: FileReader = new FileReader();
      fileReader.readAsDataURL(event.target.files[0]); // read file as data url
      fileReader.onload = (event) => { // called once readAsDataURL is completed
        this.isDisplaySecondaryImageSetURL = true;
        this.secondaryImageUrl = fileReader.result as string;
      }
    }
  }
  onSelectWbLogoImage = (event) => {
    if (event.target.files && event.target.files[0]) {
      const imageData = new FormData();
      const file = event.target.files[0];
      imageData.append('wbLogo', file,file.name);
      this.createcountryprofileService.postWbLogoImage(imageData).subscribe(
          res => {
            console.log(res);
            this.wbLogoImageSetURL = environment.apiUploadFileUrl+res['filename'];
          }
      );
      const fileReader: FileReader = new FileReader();
      fileReader.readAsDataURL(event.target.files[0]); // read file as data url
      fileReader.onload = (event) => { // called once readAsDataURL is completed
        this.isDisplayWbLogoImageSetURL = true;
        this.wbLogoImageUrl = fileReader.result as string;
      }
    }
  }
  onSelectAdbWbLogoImage = (event) => {
    if (event.target.files && event.target.files[0]) {
      const imageData = new FormData();
      const file = event.target.files[0];
      imageData.append('adbWbLogo', file,file.name);
      this.createcountryprofileService.postAdbWbLogoImage(imageData).subscribe(
          res => {
            console.log(res);
            this.adbWbLogoImageSetURL = environment.apiUploadFileUrl+res['filename'];
          }
      );
      const fileReader: FileReader = new FileReader();
      fileReader.readAsDataURL(event.target.files[0]); // read file as data url
      fileReader.onload = (event) => { // called once readAsDataURL is completed
        this.isDisplayAdbWbLogoImageSetURL = true;
        this.adbWbLogoImageUrl = fileReader.result as string;
      }
    }
  }

  onSubmit() {
    this.isSubmitted = true;
    if (!this.createCountryProfileForm.valid) {
      return false;
    } else {
      var updatedDate = new Date()
      var formattedDate = Number(updatedDate.getMonth() + 1) + '/' + updatedDate.getDate() + '/' + updatedDate.getFullYear();
      this.createCountryProfileForm.patchValue({
        primaryImageURL: this.primaryImageSetURL,
        secondaryImageURL: this.secondaryImageSetURL,
        wbLogoImageURL: this.wbLogoImageSetURL,
        adbWbLogoImageURL: this.adbWbLogoImageSetURL,
        isAdbProfile: this.isAdbProfile,
        isGenralProfile: this.isGenralProfile,
        updatedDate: formattedDate,
        status: 1,
        pdfUrl: null
    });
      this.createcountryprofileService.updateCountryProfile(this.createCountryProfileForm.value, this.countryProfileID).subscribe(
        res => {
          this.ngZone.run(() => this.router.navigateByUrl('update-country-profile/update-section/' + this.createCountryProfileForm.value.countryName + '/' + this.countryProfileID));
        },
        err => {
          if (err.status == 422) {
            this.serverErrorMessages = err.error.join('<br/');
          }
          else {
            this.serverErrorMessages = 'Something went wrong.Please contact admin.';
          }
        }
      );
    }
  }

}
