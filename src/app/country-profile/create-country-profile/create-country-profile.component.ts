import { Component, OnInit, NgZone } from '@angular/core';
import { FormsModule, FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { CreatecountryprofileService } from '../../shared/service/createcountryprofile.service';
import { CreatecountrylistService } from '../../shared/service/createcountrylist.service';
import { environment } from '../../../environments/environment';
import { UsernameValidator } from './username.validator';
import { MsalGuard, MsalService } from '@azure/msal-angular';
@Component({
  selector: 'app-create-country-profile',
  templateUrl: './create-country-profile.component.html',
  styleUrls: ['./create-country-profile.component.css']
})
export class CreateCountryProfileComponent implements OnInit {

  isSubmitted = false;
  countryProfileID: string;
  isRegionselected = false;
  createCountryProfileForm : FormGroup;
  primaryImage: string;
  secondaryImage: string;
  primaryImageUrl: string;
  secondaryImageUrl: string;
  wbLogoImage: string;
  adbWbLogoImage: string;
  wbLogoImageUrl: string;
  adbWbLogoImageUrl: string;
  selectedFile2: string;
  file:File;
  allData: any = []; 
  selectedCountry: String = "";
  selectedRegion: String = "";
  regions:Array<any>;
  countriesData:Array<any>;
  //regions:Array<any>;
  countries:Array<any>;
  loading = false;
  contentEditable = false;
  showSucessMessage:boolean;
  serverErrorMessages:string;
  primaryImageSetURL: String;
  secondaryImageSetURL: String;
  wbLogoImageSetURL: String;
  adbWbLogoImageSetURL: String;
  isAdbProfile = false;
  isGenralProfile = false;
  isDisplayPrimaryImageSetURL = false;
  isDisplaySecondaryImageSetURL = false;
  isDisplayWbLogoImageSetURL = false;
  isDisplayAdbWbLogoImageSetURL = false;
  username:string;
  name:string;
  constructor(
      public fb: FormBuilder,
      private router: Router,
      private ngZone: NgZone,
      private createcountryprofileService: CreatecountryprofileService,
      private countryListService: CreatecountrylistService,
      private _msalService: MsalService
    ) {  
      this.mainForm();
    }

  ngOnInit() {
    if(environment.isBasicAuth){
      const account = this._msalService.getAccount();
      this.name = account.name;
      this.username = account.userName;
    } else {
      this.name = 'User';
      this.username = 'User';
    }
    
    this.countryList();
    this.primaryImageSetURL = "";
    this.secondaryImageSetURL = "";
    this.wbLogoImageSetURL = "";
    this.adbWbLogoImageSetURL = "";
    this.isGenralProfile = true;
  }

  mainForm() {
    this.createCountryProfileForm = this.fb.group({
      title: ['', [Validators.required, UsernameValidator.cannotContainSpace]],
      regionName: ['', [Validators.required]],
      countryName: ['', [Validators.required]],
      primaryImageURL:  [''],
      secondaryImageURL: [''],
      wbLogoImageURL: [''],
      adbWbLogoImageURL:[''],
      isAdbProfile: [''],
      isGenralProfile: [''],
      status: 1,
      pdfUrl: null,
      createdBy:  [''],
      updatedBy:  [''],
    })
  }

  countryList() {
    this.countryListService.getAllCountryLists().subscribe(
      res => {
        this.countriesData = res['countryList'];
        this.regions = [...new Set(this.countriesData.map(item => item.region))];
      },
      err => { 
        console.log(err);
      }
    );
  }

  isAdbProfileEvent(event) {
      if ( event.target.checked ) {
          this.contentEditable = true;
          this.isAdbProfile = true;
          this.isGenralProfile = false;
    } else {
      this.isAdbProfile = false;
      this.contentEditable = false;
      this.isGenralProfile = true;
    }
  }

  isGenralProfileEvent(event) {
    if ( event.target.checked ) {
        this.isGenralProfile = true;
        this.isAdbProfile = false;
      this.contentEditable = false;
        
  } else {
    this.isAdbProfile = true;
    this.isGenralProfile = false;
    this.contentEditable = true;
    
  }
}

  // Choose regionName with select dropdown
  changeRegion(region){
    this.createCountryProfileForm.get('regionName').setValue(region, {
      onlySelf: true
    })
    if(region){
      this.isRegionselected = true;
      this.selectedRegion = region;
      //this.countries = this.countriesData.find(cntry => cntry.region === region).country;
      this.countries = this.countriesData.filter( obj => obj.region === region ).map( obj => obj.country );
    } else {
      this.isRegionselected = false;
    }
  }

  changeCountry(e){
    this.createCountryProfileForm.get('countryName').setValue(e, {
      onlySelf: true
    })
  }

  onSelectPrimaryImage = (event) =>{
    if (event.target.files && event.target.files[0]) {
      const imageData = new FormData();
      const file = event.target.files[0];
      imageData.append('primaryImage', file,file.name);
      this.createcountryprofileService.postPrimaryImage(imageData).subscribe(
          res => {
            console.log(res);
            this.primaryImageSetURL = environment.apiUploadFileUrl+res['filename'];
            console.log(this.primaryImageSetURL);
          }
      );
      const fileReader: FileReader = new FileReader();
      fileReader.readAsDataURL(event.target.files[0]); // read file as data url
      fileReader.onload = (event) => { // called once readAsDataURL is completed
        this.isDisplayPrimaryImageSetURL = true;
        this.primaryImageUrl = fileReader.result as string;
      }
    }
  }
 
  onSelectSecondaryImage = (event) => {
    if (event.target.files && event.target.files[0]) {
      const imageData = new FormData();
      const file = event.target.files[0];
      imageData.append('secondaryImage', file,file.name);
      this.createcountryprofileService.postSecondaryImage(imageData).subscribe(
          res => {
            console.log(res);
            this.secondaryImageSetURL = environment.apiUploadFileUrl+res['filename'];
          }
      );
      const fileReader: FileReader = new FileReader();
      fileReader.readAsDataURL(event.target.files[0]); // read file as data url
      fileReader.onload = (event) => { // called once readAsDataURL is completed
        this.isDisplaySecondaryImageSetURL = true;
        this.secondaryImageUrl = fileReader.result as string;
      }
    }
  }
  onSelectWbLogoImage = (event) => {
    if (event.target.files && event.target.files[0]) {
      const imageData = new FormData();
      const file = event.target.files[0];
      imageData.append('wbLogo', file,file.name);
      this.createcountryprofileService.postWbLogoImage(imageData).subscribe(
          res => {
            console.log(res);
            this.wbLogoImageSetURL = environment.apiUploadFileUrl+res['filename'];
          }
      );
      const fileReader: FileReader = new FileReader();
      fileReader.readAsDataURL(event.target.files[0]); // read file as data url
      fileReader.onload = (event) => { // called once readAsDataURL is completed
        this.isDisplayWbLogoImageSetURL = true;
        this.wbLogoImageUrl = fileReader.result as string;
      }
    }
  }
  onSelectAdbWbLogoImage = (event) => {
    if (event.target.files && event.target.files[0]) {
      const imageData = new FormData();
      const file = event.target.files[0];
      imageData.append('adbWbLogo', file,file.name);
      this.createcountryprofileService.postAdbWbLogoImage(imageData).subscribe(
          res => {
            console.log(res);
            this.adbWbLogoImageSetURL = environment.apiUploadFileUrl+res['filename'];
          }
      );
      const fileReader: FileReader = new FileReader();
      fileReader.readAsDataURL(event.target.files[0]); // read file as data url
      fileReader.onload = (event) => { // called once readAsDataURL is completed
        this.isDisplayAdbWbLogoImageSetURL = true;
        this.adbWbLogoImageUrl = fileReader.result as string;
      }
    }
  }

  // Getter to access form control
  get myCountryProfileForm(){
    return this.createCountryProfileForm.controls;
  }

  onSubmit() {
    this.isSubmitted = true;
    console.log('authname',this.name);
    var testTitle = this.createCountryProfileForm.controls.title.value.toString().trim();
    if (testTitle === '') {
      return false
    }
    if (!this.createCountryProfileForm.valid) {
      return false;
    } else {
      this.createCountryProfileForm.patchValue({
        primaryImageURL: this.primaryImageSetURL,
        secondaryImageURL: this.secondaryImageSetURL,
        wbLogoImageURL: this.wbLogoImageSetURL,
        adbWbLogoImageURL: this.adbWbLogoImageSetURL,
        isAdbProfile: this.isAdbProfile,
        isGenralProfile: this.isGenralProfile,
        status: 1,
        pdfUrl: null,
        createdBy:this.name,
        updatedBy:this.name
    });
    console.log(this.createCountryProfileForm.value);
      this.createcountryprofileService.postCreateCountryProfile(this.createCountryProfileForm.value).subscribe(
        res => {
            this.countryProfileID = res['_id']; 
            this.ngZone.run(() => this.router.navigateByUrl('create-country-profile/create-section/'+this.createCountryProfileForm.value.countryName+'/'+this.countryProfileID));
        },
        err => {
          if(err.status == 422){
            this.serverErrorMessages = err.error.join('<br/');
          }
          else{
            this.serverErrorMessages = 'Something went wrong.Please contact admin.';
          }
        }
    );
    }
  }
}

