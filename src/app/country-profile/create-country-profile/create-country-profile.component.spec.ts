import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCountryProfileComponent } from './create-country-profile.component';

describe('CreateCountryProfileComponent', () => {
  let component: CreateCountryProfileComponent;
  let fixture: ComponentFixture<CreateCountryProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCountryProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCountryProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
