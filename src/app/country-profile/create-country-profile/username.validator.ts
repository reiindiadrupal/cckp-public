import { AbstractControl, ValidationErrors } from '@angular/forms';

export class UsernameValidator {
    static cannotContainSpace(control: AbstractControl): ValidationErrors | null {
        if (((control.value as string) && control.value.trim() == "")) {
            return { cannotContainSpace: true }
        }

        return null;
    }
}