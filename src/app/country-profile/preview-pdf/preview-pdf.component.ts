import { Component, ViewChild, OnInit, ElementRef, NgZone, ViewEncapsulation  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CreatecountryprofileService } from '../../shared/service/createcountryprofile.service';
import { CreatesectionsService } from '../../shared/service/createsections.service';
declare var $: any;


@Component({
  selector: 'app-preview-pdf',
  templateUrl: './preview-pdf.component.html',
  styleUrls: ['./preview-pdf.component.css'],
  encapsulation: ViewEncapsulation.None,
  
})
export class PreviewPdfComponent implements OnInit {

  countryName: string;
  countryProfileID: string;
  forewordMainSection :any;
  forewordFooterLinks :any;
  isCheckedforewordSection : boolean;
  forewords :any;
  isDisplayforewordSection  : boolean;
  keyMessageMainSection :any;
  keyMessageFooterLinks :any;
  isCheckedkeyMessageSection : boolean;
  keyMessages :any;
  isDisplaykeyMessageSection :boolean;
  countryOverviewMainSection :any;
  countryOverviewFooterLinks :any;
  isCheckedcountryOverviewSection : boolean;
  countryOverviews :any;
  isDisplaycountryOverviewSection :boolean;
  climatologyMainSection :any;
  climatologyFooterLinks :any;
  isCheckedclimatologySection : boolean;
  climatologys :any;
  isDisplayclimatologySection :boolean;
  climateRelatedNaturalHazardMainSection :any;
  climateRelatedNaturalHazardFooterLinks :any;
  isCheckedclimateRelatedNaturalHazardSection : boolean;
  climateRelatedNaturalHazards :any;
  isDisplayclimateRelatedNaturalHazardSection :boolean;
  climateChangeImpactMainSection :any;
  climateChangeImpactFooterLinks :any;
  isCheckedclimateChangeImpactSection : boolean;
  climateChangeImpacts :any;
  isDisplayclimateChangeImpactSection :boolean;
  policiesAndProgramMainSection :any;
  policiesAndProgramFooterLinks :any;
  isCheckedpoliciesAndProgramSection : boolean;
  policiesAndPrograms :any;
  isDisplaypoliciesAndProgramSection :boolean;
  adaptationMainSection :any;
  adaptationFooterLinks :any;
  isCheckedadaptationSection : boolean;
  adaptations :any;
  isDisplayadaptationSection :boolean;
  primaryImageURL: string;
  secondaryImageURL: string;
  wbLogoImageURL: string;
  adbWbLogoImageURL: string;
  primaryImageURLSrc: string;
  secondaryImageURLSrc: string;
  isAdbProfile: Boolean;
  isGenralProfile:boolean;
  acknowledgementMainSection :any;
  isDisplayAcknowledgementSection  : boolean;

    constructor( 
      private activatedRoute: ActivatedRoute,
      private router: Router, 
      private countryProfile: CreatecountryprofileService,
      private countrySections: CreatesectionsService,
      private ngZone: NgZone,
    ) {
        this.activatedRoute.params.subscribe(params => {
          this.countryProfileID = params.countryProfileID;
      });
     }
   
     ngAfterViewChecked() { 
      $(".e-rte-image").each(function () {
        if ($(this).width() > 110) {
          //$(this).closest('p, div').addClass('imgWrap');
        }
      });
  
      $(".mainSection sup").each(function () {
        var format = /^[A-Za-z0-9\!\@\#\$\%\^\&\*\)\(+\=\._-]+$/g;
        if( $(this).text().match(format) ){
          $(this).html('<a href="#' + $(this).text() + '">' + $(this).text() + '</a>');
        }
      });
      
      $(".subSectionContent sup").each(function () {
        var format = /^[A-Za-z0-9\!\@\#\$\%\^\&\*\)\(+\=\._-]+$/g;
        if( $(this).text().match(format) ){
          $(this).html('<a href="#' + $(this).text() + '">' + $(this).text() + '</a>');
        }
      });
      $(".footerSection sup").each(function () {
  
          var text = $(this).text();
          var op = text.replace(/&nbsp;/g, '');
          $(this).attr("id",op);
  
      });
    }

  ngOnInit() {
    //this.print();
    this.countryProfile.getCountryProfile(this.countryProfileID).subscribe(
      res => {
        this.countryName = res['countryProfileData'].countryProfile.countryName;
        this.acknowledgementMainSection = res['countryProfileData'].acknowledgement.acknowledgementMainSection;
        this.isDisplayAcknowledgementSection = res['countryProfileData'].acknowledgement.isDisplayAcknowledgementSection;
      },
      err => { 
        console.log(err);
      }
    );
    this.countryProfile.getCountryProfile(this.countryProfileID).subscribe(
      res => {
        this.countryName = res['countryProfileData'].countryProfile.countryName;
        console.log(res['countryProfileData']);
        this.primaryImageURL = res['countryProfileData'].countryProfile.primaryImageURL;
        if(this.primaryImageURL){
          this.primaryImageURLSrc = this.primaryImageURL;
        } else {
          this.primaryImageURLSrc = "./../../../assets/images/cover/4.jpg";
        }
        this.secondaryImageURL = res['countryProfileData'].countryProfile.secondaryImageURL;
        if(this.secondaryImageURL){
          this.secondaryImageURLSrc = this.secondaryImageURL;
        } else {
          this.secondaryImageURLSrc = "./../../../assets/images/cover/left.jpg";
        }
        this.wbLogoImageURL = res['countryProfileData'].countryProfile.wbLogoImageURL;
        this.adbWbLogoImageURL = res['countryProfileData'].countryProfile.adbWbLogoImageURL;
        this.isAdbProfile = res['countryProfileData'].countryProfile.isAdbProfile;
        this.isGenralProfile = res['countryProfileData'].countryProfile.isGenralProfile;
      },
      err => { 
        console.log(err);
      }
    );
    this.countryProfile.getCountryProfile(this.countryProfileID).subscribe(
      res => {
        this.countryName = res['countryProfileData'].countryProfile.countryName;
        this.forewordMainSection = res['countryProfileData'].foreword.forewordMainSection;
        this.forewordFooterLinks = res['countryProfileData'].foreword.forewordFooterLinks;
        this.isCheckedforewordSection = res['countryProfileData'].foreword.isCheckedforewordSection;
        this.forewords = res['countryProfileData'].foreword.forewords;
        this.isDisplayforewordSection = res['countryProfileData'].foreword.isDisplayforewordSection;
        this.keyMessageMainSection = res['countryProfileData'].keyMessage.keyMessageMainSection;
        this.keyMessageFooterLinks = res['countryProfileData'].keyMessage.keyMessageFooterLinks;
        this.isCheckedkeyMessageSection = res['countryProfileData'].keyMessage.isCheckedkeyMessageSection;
        this.keyMessages = res['countryProfileData'].keyMessage.keyMessages;
        this.isDisplaykeyMessageSection = res['countryProfileData'].keyMessage.isDisplaykeyMessageSection;
        this.countryOverviewMainSection = res['countryProfileData'].countryOverview.countryOverviewMainSection;
        this.countryOverviewFooterLinks = res['countryProfileData'].countryOverview.countryOverviewFooterLinks;
        this.isCheckedcountryOverviewSection = res['countryProfileData'].countryOverview.isCheckedcountryOverviewSection;
        this.countryOverviews = res['countryProfileData'].countryOverview.countryOverviews;
        this.isDisplaycountryOverviewSection = res['countryProfileData'].countryOverview.isDisplaycountryOverviewSection;
        this.climatologyMainSection = res['countryProfileData'].climatology.climatologyMainSection;
        this.climatologyFooterLinks = res['countryProfileData'].climatology.climatologyFooterLinks;
        this.isCheckedclimatologySection = res['countryProfileData'].climatology.isCheckedclimatologySection;
        this.climatologys = res['countryProfileData'].climatology.climatologys;
        this.isDisplayclimatologySection = res['countryProfileData'].climatology.isDisplayclimatologySection;
        this.climateRelatedNaturalHazardMainSection = res['countryProfileData'].climateRelatedNaturalHazard.climateRelatedNaturalHazardMainSection;
        this.climateRelatedNaturalHazardFooterLinks = res['countryProfileData'].climateRelatedNaturalHazard.climateRelatedNaturalHazardFooterLinks;
        this.isCheckedclimateRelatedNaturalHazardSection = res['countryProfileData'].climateRelatedNaturalHazard.isCheckedclimateRelatedNaturalHazardSection;
        this.climateRelatedNaturalHazards = res['countryProfileData'].climateRelatedNaturalHazard.climateRelatedNaturalHazards;
        this.isDisplayclimateRelatedNaturalHazardSection = res['countryProfileData'].climateRelatedNaturalHazard.isDisplayclimateRelatedNaturalHazardSection;
        this.climateChangeImpactMainSection = res['countryProfileData'].climateChangeImpact.climateChangeImpactMainSection;
        this.climateChangeImpactFooterLinks = res['countryProfileData'].climateChangeImpact.climateChangeImpactFooterLinks;
        this.isCheckedclimateChangeImpactSection = res['countryProfileData'].climateChangeImpact.isCheckedclimateChangeImpactSection;
        this.climateChangeImpacts = res['countryProfileData'].climateChangeImpact.climateChangeImpacts;
        this.isDisplayclimateChangeImpactSection = res['countryProfileData'].climateChangeImpact.isDisplayclimateChangeImpactSection;
        this.policiesAndProgramMainSection = res['countryProfileData'].policiesAndProgram.policiesAndProgramMainSection;
        this.policiesAndProgramFooterLinks = res['countryProfileData'].policiesAndProgram.policiesAndProgramFooterLinks;
        this.isCheckedpoliciesAndProgramSection = res['countryProfileData'].policiesAndProgram.isCheckedpoliciesAndProgramSection;
        this.policiesAndPrograms = res['countryProfileData'].policiesAndProgram.policiesAndPrograms;
        this.isDisplaypoliciesAndProgramSection = res['countryProfileData'].policiesAndProgram.isDisplaypoliciesAndProgramSection;
        this.adaptationMainSection = res['countryProfileData'].adaptation.adaptationMainSection;
        this.adaptationFooterLinks = res['countryProfileData'].adaptation.adaptationFooterLinks;
        this.isCheckedadaptationSection = res['countryProfileData'].adaptation.isCheckedadaptationSection;
        this.adaptations = res['countryProfileData'].adaptation.adaptations;
        this.isDisplayadaptationSection = res['countryProfileData'].adaptation.isDisplayadaptationSection;
        
        
        console.log("test",res['countryProfileData']);
      
      },
      err => { 
        console.log(err);
      }
    );
    
    // $('h1,h2,h3').each(function() {
    //   if ($(this).text() === '') {
    //     $(this).remove();
    //   }
    // });

      // if ($('h2').text() === '') {
      //   $(this).remove();
      // }


    setTimeout(function() {
      var toc = "";
      var level = 0;
      var index = 0;

      document.getElementById("contents").innerHTML =
      document.getElementById("contents").innerHTML.replace(
        /<h([1-3])\b[^>]*>(.*?)<\/h([1-3])>/gi,
        function (str, openLevel, titleText, closeLevel) {
          index++;
          if (openLevel != closeLevel) {
            return str + ' - ' + openLevel;
          }
          if (openLevel > level) {
            toc += (new Array(openLevel - level + 1)).join("<ul>");
          } else if (openLevel < level) {
            toc += (new Array(level - openLevel + 1)).join("</ul>");
          }
          level = parseInt(openLevel);
          const originalString = titleText;
          const strippedString = originalString.replace(/(<([^>]+)>)/gi, "");
          var anchor = strippedString.replace(/ /g, "_");
          toc += '<li><a id="TOCEntry_'+ index +'_ID" class="level'+level+'" href="#'+ anchor +'"\><span class="levelText">' + strippedString +'</span></a></li>';
          var element = $(str);
          var attributesArray = [];
          $(element[0].attributes).each(function() {
            let item = this.nodeName+'='+'"'+this.nodeValue+'"';
            attributesArray.push(item);
          });
          var attrSplit = attributesArray.join().replace(',', ' ');
          return '<h' + openLevel +' '+attrSplit+'><a id="TOCEntry_'+ index +'_Target_ID" class="bookmark" name="'+ anchor +'"\>'
                + titleText + "</a></h" + closeLevel + ">";
        }
      );


      if (level) {
        toc += (new Array(level + 1)).join("</ul>");
      }
      toc += '<span data-mapping-enabled="true" data-mapping-id="TOC_ENTRIES_COUNT"  data-content="'+index+'" style="color: transparent;" id="TOC_ENTRIES_COUNT">&nbsp;&nbsp;</span>'
      document.getElementById("toc").innerHTML += toc;
    }, 5000);
    this.print();
  }
  
  print = async() => {
    let printContents;
    printContents = await window.document.getElementById('print-section').innerHTML;
    setTimeout(() => {
      window.print();
    }, 6000);
  }

  //self preview
  previewPdf = (event) => {
    window.print();
  }

}
