import { AlertService } from './../../shared/service/alert.service';
import { Component, OnInit, NgZone } from '@angular/core';
import { FormControl, FormArray, FormsModule, FormGroup, FormBuilder, Validators, FormArrayName } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';
import { ToolbarService, ImageService, HtmlEditorService, RichTextEditor } from '@syncfusion/ej2-angular-richtexteditor';
import { CreatesectionsService } from '../../shared/service/createsections.service';
import { CreatecountryprofileService } from '../../shared/service/createcountryprofile.service';
declare var $: any;
import { environment } from '../../../environments/environment';
import { ToastrManager } from 'ng6-toastr-notifications';
import { CreatecountrylistService } from './../../shared/service/createcountrylist.service';
import { TableService } from './../../shared/service/table.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-update-section',
  templateUrl: './update-section.component.html',
  styleUrls: ['./update-section.component.css'],
  providers: [ToolbarService, ImageService, HtmlEditorService]
})

export class UpdateSectionComponent implements OnInit {
  adbProfile: boolean;
  generalProfile: boolean;
  countryProfileTitle: string;
  AndesBold: string;
  pdfUrlMsg = '';
  isSubmitted = false;
  countryName: string;
  countryProfileID: string;
  profileTitle: String;

  gettasData: any = null;
    tasData: number = null;
    gett2PrData: any = null;
    t2PrData: number = null;
    getT2tasmaxData: any = null;
    T2tasmaxData: number = null;
    getT2tasminData: any = null;
    T2tasminData: number = null;

    T3tasP101stData: any = null;
    getT3tasP101stData: any = null;
    T3tasP102ndData: any = null;
    getT3tasP102ndData: any = null;
    T3tasP103rdData: any = null;
    getT3tasP103rdData: any = null;
    T3tasP104thData: any = null;
    getT3tasP104thData: any = null;
    T3tasMedian1stData: any = null;
    getT3tasMedian1stData: any = null;
    T3tasMedian2ndData: any = null;
    getT3tasMedian2ndData: any = null;
    T3tasMedian3rdData: any = null;
    getT3tasMedian3rdData: any = null;
    T3tasMedian4thData: any = null;
    getT3tasMedian4thData: any = null;
    T3tasP901stData: any = null;
    getT3tasP901stData: any = null;
    T3tasP902ndData: any = null;
    getT3tasP902ndData: any = null;
    T3tasP903rdData: any = null;
    getT3tasP903rdData: any = null;
    T3tasP904thData: any = null;
    getT3tasP904thData: any = null;
    T3prP101stData: any = null;
    getT3prP101stData: any = null;
    T3prP102ndData: any = null;
    getT3prP102ndData: any = null;
    T3prP103rdData: any = null;
    getT3prP103rdData: any = null;
    T3prP104thData: any = null;
    getT3prP104thData: any = null;
    T3prMedian1stData: any = null;
    getT3prMedian1stData: any = null;
    T3prMedian2ndData: any = null;
    getT3prMedian2ndData: any = null;
    T3prMedian3rdData: any = null;
    getT3prMedian3rdData: any = null;
    T3prMedian4thData: any = null;
    getT3prMedian4thData: any = null;
    T3prP901stData: any = null;
    getT3prP901stData: any = null;
    T3prP902ndData: any = null;
    getT3prP902ndData: any = null;
    T3prP903rdData: any = null;
    getT3prP903rdData: any = null;
    T3prP904thData: any = null;
    getT3prP904thData: any = null;
  
  forwardSubSectionErrorMsg: string = '';
  keyMessageSubSectionErrorMsg: string = '';
  countryOverviewSubSectionsErrorMsg: string = '';
  climatologySubSectionErrorMsg: string = '';
  climateRelatedNaturalHazardSubSectionErrorMsg: string = ''
  climateChangeImpactSubSectionErrorMsg: string = ''
  policiesAndProgramSubSectionErrorMsg: string = ''
  adaptationSubSectionErrorMsg: string = ''

  countryCode = '';
  LifeExpectancyAtBirth: any = null;
  LifeExpectancyAtBirthData: any = null;
  LifeExpectancyAtBirthNan: any = null;
  adultLiteracyRate: any = null;
  adultLiteracyRateData: any = null;
  adultLiteracyRateNan: any = null;
  PopulationDensity: any;
  PopulationDensityData: any = null;
  PopulationDensityNan: any = null;
  PercentOfPopulationwithAccessToElectricity: any;
  PercentOfPopulationwithAccessToElectricityData: any = null;
  PercentOfPopulationwithAccessToElectricityNan: any = null;
  GdpPerCapita: any = null;
  GdpPerCapitaData: any = null;
  GdpPerCapitaNan: any = null;

  fig1GENTitle = '';
  fig2GENTitle  = '';
  fig3GENTitle  = '';
  fig4aGENTitle = ''; 
  fig4bGENTitle = ''; 
  fig5GENTitle  = '';
  fig6aGENTitle = ''; 
  fig6bGENTitle = ''; 
  fig6cGENTitle = ''; 
  fig6dGENTitle = ''; 
  fig7GENTitle  = '';
  fig8GENTitle  = '';
  fig9GENTitle  = '';

 
  fig1GENUrl = '';
  fig2GENUrl  = '';
  fig3GENUrl  = '';
  fig4aGENUrl  = '';
  fig4bGENUrl  = '';
  fig5GENUrl  = '';
  fig6aGENUrl  = '';
  fig6bGENUrl  = '';
  fig6cGENUrl  = '';
  fig6dGENUrl  = '';
  fig7GENUrl  = '';
  fig8GENUrl  = '';
  fig9GENUrl  = '';

  fig1GENfigure = '';
  fig2GENfigure  = '';
  fig3GENfigure  = '';
  fig4aGENfigure  = '';
  fig4bGENfigure  = '';
  fig5GENfigure  = '';
  fig6aGENfigure  = '';
  fig6bGENfigure  = '';
  fig6cGENfigure  = '';
  fig6dGENfigure  = '';
  fig7GENfigure  = '';
  fig8GENfigure  = '';
  fig9GENfigure  = '';

  fig1AdbUrl = '';
  fig2AdbUrl = '';
  fig3aAdbUrl = '';
  fig3bAdbUrl = '';
  fig4AdbUrl = '';
  fig5aAdbUrl = '';
  fig5bAdbUrl = '';
  fig5cAdbUrl = '';
  fig5dAdbUrl = '';
  fig6AdbUrl = '';
  fig7AdbUrl = '';

  fig1Adbfigure = '';
  fig2Adbfigure = '';
  fig3aAdbfigure = '';
  fig3bAdbfigure = '';
  fig4Adbfigure = '';
  fig5aAdbfigure = '';
  fig5bAdbfigure = '';
  fig5cAdbfigure = '';
  fig5dAdbfigure = '';
  fig6Adbfigure = '';
  fig7Adbfigure = '';

  fig1Adbtitle = '';
  fig2Adbtitle = '';
  fig3aAdbtitle = '';
  fig3bAdbtitle = '';
  fig4Adbtitle = '';
  fig5aAdbtitle = '';
  fig5bAdbtitle = '';
  fig5cAdbtitle = '';
  fig5dAdbtitle = '';
  fig6Adbtitle = '';
  fig7Adbtitle = '';
  
  fetchABDImageDataArr:any = [];
  fetchGENImageDataArr:any = [];
  i : number;
  z : number;

  fig1GENHtml = '';
    fig2GENHtml  = '';
    fig3GENHtml  = '';
    fig4aGENHtml  = '';
    fig4bGENHtml  = '';
    fig5GENHtml  = '';
    fig6aGENHtml  = '';
    fig6bGENHtml  = '';
    fig6cGENHtml  = '';
    fig6dGENHtml  = '';
    fig7GENHtml  = '';
    fig8GENHtml  = '';
    fig9GENHtml  = '';

    fig1AdbHtml = '';
    fig2AdbHtml = '';
    fig3aAdbHtml = '';
    fig3bAdbHtml = '';
    fig4AdbHtml = '';
    fig5aAdbHtml = '';
    fig5bAdbHtml = '';
    fig5cAdbHtml = '';
    fig5dAdbHtml = '';
    fig6AdbHtml = '';
    fig7AdbHtml = '';

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private createsectionsService: CreatesectionsService,
    private ngZone: NgZone,
    private countryProfile: CreatecountryprofileService,
    private toastr: ToastrManager,
    private countryListService: CreatecountrylistService,
    private tableService: TableService,
    private toaster: AlertService,
    private httpClient: HttpClient
  ) {
    this.activatedRoute.params.subscribe(params => {
      this.countryName = params.countryName;
      this.countryProfileID = params.countryProfileID;
    });
  }

  public fontFamily: Object = {
    default: "BertholdLight", // to define default font-family
    items: [
      { text: "Andes Book", value: "AndesBook", command: "Font", subCommand: "FontName" },
      { text: "Andes Bold", value: "AndesBold", command: "Font", subCommand: "FontName" },
      { text: "Andes Extra Light", value: "AndesExtraLight", command: "Font", subCommand: "FontName" },
      { text: "BertholdBE Regular", value: "BertholdBERegular", command: "Font", subCommand: "FontName" },
      { text: "BertholdLight", value: "BertholdLight", command: "Font", subCommand: "FontName" },
    ]
  };

  public backgroundColor = {
    columns: 2,
    // colorCode: { 'Custom': ['#ffffff', '#00aae7', '#002f53', '#e7f4fc', '#00375e', '#000000', '']},
    colorCode: { 'Custom': ['#ffffff', '#00aae7', '#000000', ''] },
    modeSwitcher: true
  }

  public fontColor = {
    columns: 2,
    // colorCode: { 'Custom': ['#ffffff', '#00aae5', '#231f20','#a7a9ac','#77c9ef','#002f54','#36ace7','#000000', '']}, 
    colorCode: { 'Custom': ['#ffffff', '#00aae5', '#231f20', '#000000', ''] },
    modeSwitcher: true
  }

  public tools: object = {
    type: 'MultiRow',
    //items: ['Bold', 'Italic', 'Underline', 'StrikeThrough', 'FontName', 'FontSize', 'FontColor', 'BackgroundColor', 'LowerCase', 'UpperCase', 'SuperScript', 'SubScript','|', 'Formats', 'Alignments', 'OrderedList', 'UnorderedList', 'Outdent', 'Indent', '|', 'CreateLink', 'Image', '|', 'ClearFormat', 'Print','SourceCode', 'FullScreen', '|', 'Undo', 'Redo' ]};
    items: ['Bold', 'Italic', 'Underline', 'StrikeThrough', 'FontName', 'FontSize', 'FontColor', 'BackgroundColor', 'LowerCase', 'UpperCase', 'SuperScript', 'SubScript', '|', 'Formats', 'Alignments', 'OrderedList', 'UnorderedList', 'Outdent', 'Indent', '|', 'CreateLink', 'Image', 'CreateTable', '|', 'Undo', 'Redo', 'SourceCode']
  };
  public insertImageSettings = {
    //path: "http://ec2-52-22-8-193.compute-1.amazonaws.com/ngpdfcreator/blobData/",
    //saveUrl : 'http://ec2-52-22-8-193.compute-1.amazonaws.com/ngpdfcreator/api/uploadbox',
    path: environment.apiUploadFileUrl,
    saveUrl: environment.apiBaseUrl + '/postImage',
    allowedTypes: ['.jpeg', '.jpg', '.png','.svg','.bmp']
    
  };
  public pasteCleanupSettings: object = {
    // prompt: true,
   //  plainText: false,
   keepFormat: false,
 };
  public placeholderContent: String = 'Enter your content here...';
  public placeholderFooter: String = 'Enter your footer links here...';
  public form: FormGroup;
  public forewordList: FormArray;
  public keyMessageList: FormArray;
  public countryOverviewList: FormArray;
  public climatologyList: FormArray;
  public climateRelatedNaturalHazardList: FormArray;
  public climateChangeImpactList: FormArray;
  public policiesAndProgramList: FormArray;
  public adaptationList: FormArray;

  isCheckedAcknowledgementSection = false;
  isCheckedforewordSection = false;
  isCheckedkeyMessageSection = false;
  isCheckedcountryOverviewSection = false;
  isCheckedclimatologySection = false;
  isCheckedclimateRelatedNaturalHazardSection = false;
  isCheckedclimateChangeImpactSection = false;
  isCheckedpoliciesAndProgramSection = false;
  isCheckedadaptationSection = false;

  isDisplayAcknowledgementSection  = true;
  isDisplayforewordSection = true;
  isDisplaykeyMessageSection = true;
  isDisplaycountryOverviewSection = true;
  isDisplayclimatologySection = true;
  isDisplayclimateRelatedNaturalHazardSection = true;
  isDisplayclimateChangeImpactSection = true;
  isDisplaypoliciesAndProgramSection = true;
  isDisplayadaptationSection = true;
  loading = false;
  showSucessMessage: boolean;
  serverErrorMessages: string;

  climatologyOptions: string[] = ['Climate Baseline', 'Climate Future', 'Custom'];
  staticclimatologyOptions: string[] = ['Climate Baseline', 'Climate Future', 'Custom'];
  climateRelatedNaturalHazardOptions: string[] = ['Heat Waves', 'Drought', 'Flood', 'Tropical Cyclones', 'Storm Surge', 'Overview', 'Climate Change Impacts', 'Implications for DRM', 'Custom'];
  staticclimateRelatedNaturalHazardOptions: string[] = ['Heat Waves', 'Drought', 'Flood', 'Tropical Cyclones', 'Storm Surge', 'Overview', 'Climate Change Impacts', 'Implications for DRM', 'Custom'];
  climateChangeImpactOptions: string[] = ['Natural Resources', 'Economic Sectors', 'Communities', 'Agriculture', 'Water', 'Energy', 'Health', 'Transportation', 'Coastal Zone', 'Fisheries', 'Forestry', 'Biodiversity', 'Education', 'Industry', 'Custom'];
  staticclimateChangeImpactOptions: string[] = ['Natural Resources', 'Economic Sectors', 'Communities', 'Agriculture', 'Water', 'Energy', 'Health', 'Transportation', 'Coastal Zone', 'Fisheries', 'Forestry', 'Biodiversity', 'Education', 'Industry', 'Custom'];
  policiesAndProgramOptions: string[] = ['National Adaptation Policies and Strategies', 'Climate Change Priorities of the ADB and WBG', 'Custom'];
  staticpoliciesAndProgramOptions: string[] = ['National Adaptation Policies and Strategies', 'Climate Change Priorities of the ADB and WBG', 'Custom'];
  adaptationOptions: string[] = ['Institutional Framework for Adaptation', 'Policy Framework for Adaptation', 'Recommendations', 'Custom'];
  staticadaptationOptions: string[] = ['Institutional Framework for Adaptation', 'Policy Framework for Adaptation', 'Recommendations', 'Custom'];

  createSection = {
    "foreword": {
      "content":
        [
          // { 
          //   "forewordSubSectionsTitle" : "title1",
          //   "forewordSubSectionsContent" : "<p>The Rich Text Editor triggers events based on its actions. </p><p> The events can be used as an extension point to perform custom operations.</p><ul><li>created - Triggers when the component is rendered.</li><li>change - Triggers only when RTE is blurred and changes are done to the content.</li> <li>focus - Triggers when RTE is focused in.</li> <li>blur - Triggers when RTE is focused out.</li><li>actionBegin - Triggers before command execution using toolbar items or executeCommand method.</li><li>actionComplete - Triggers after command execution using toolbar items or executeCommand method.</li><li>destroyed – Triggers when the component is destroyed.</li></ul>" 
          // },  
        ],
      "forewordFooterLinks": "linkes",
      "forewordMainSection": "non",
    },
  };

  fetchImageData(countryCode,countryName) {
    const fetchImages = {
        "countryOverView": [
            {
                "title": `Elevation of ${countryName}`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig1.png`
            },
            {
                "title": `ND-GAIN Index for ${countryName}`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig2.svg`
            }
        ],
        "countryOverViewAdb": [
            {
                "title": `The ND-GAIN Index summarizes a country's vulnerability to climate change and other global challenges in combination with its readiness to improve resilience. It aims to help businesses and the public sector better prioritize investments for a more efficient response to the immediate global challenges ahead.`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig1.svg`
            }
        ],
        "Climatology": [
            {
                "title": `Average Monthly Temperature and Rainfall of ${countryName} for 1901-2019`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig3.svg`
            },
            {
                "title": ` Map of average annual temperature (left); annual precipitation (right) for ${countryName}, 1901-2019`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig4a.png`
            },
            {
                "title": ` Map of average annual temperature (left); annual precipitation (right) for ${countryName}, 1901-2019`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig4b.png`
            },
            {
                "title": ` Mean Annual Temperature for ${countryName}, 1901-2019 `,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig5.svg`
            },
            {
                "title": "CMIP5 multi-model ensemble projected change (32 GCMs) in annual temperature (top) and precipitation (bottom) by 2040-2059 (left) and by 2080-2099 (right), relative to 1986-2005 baseline under RCP 8.5",
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig6a.png`
            },
            {
                "title": "CMIP5 multi-model ensemble projected change (32 GCMs) in annual temperature (top) and precipitation (bottom) by 2040-2059 (left) and by 2080-2099 (right), relative to 1986-2005 baseline under RCP 8.5",
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig6b.png`
            },
            {
                "title": "CMIP5 multi-model ensemble projected change (32 GCMs) in annual temperature (top) and precipitation (bottom) by 2040-2059 (left) and by 2080-2099 (right), relative to 1986-2005 baseline under RCP 8.5",
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig6c.png`
            },
            {
                "title": "CMIP5 multi-model ensemble projected change (32 GCMs) in annual temperature (top) and precipitation (bottom) by 2040-2059 (left) and by 2080-2099 (right), relative to 1986-2005 baseline under RCP 8.5",
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig6d.png`
            }
            ,
            {
                "title": `Historical and Projected Average Temperature for ${countryName} from 1986 to 2099`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig7.svg`
            },
            ,
            {
                "title": "Projected Change in Summer Days (Tmax> 25°C)",
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig8.svg`
            },
            ,
            {
                "title": `Annual Average Precipitation in ${countryName} for 1986 to 2099`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig9.svg`
            },
        ],
        "ClimatologyAdb": [
            {
                "title": `Average monthly temperature and rainfall in ${countryName} (1901-2019)`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig2.svg`
            },
            {
                "title": `(Left) Annual Mean Temperature, and (Right) Annual Mean Rainfall (mm) in ${countryName} over the period 1901-2019`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig3a.png`
            },
            {
                "title": `(Left) Annual Mean Temperature, and (Right) Annual Mean Rainfall (mm) in ${countryName} over the period 1901-2019`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig3b.png`
            },
            {
                "title": `Projected average temperature anomaly’ and ‘projected annual rainfall anomaly’ in ${countryName}. Outputs of 16 models within the ensemble simulating RCP 8.5 over the period 2080-2099. Models shown represent the subset of models within the ensemble which provide projections across all RCPs and therefore are most robust for comparison.17 Three models are labelled.`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig4.png`
            },
            {
                "title": "CMIP5 ensemble projected change (32 GCMs) in annual temperature (top) and precipitation (bottom) by 2040–2059 (left) and by 2080–2090 (right) relative to 1986–2005 baseline under RCP 8.5.",
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig5a.png`
            },
            {
                "title": "CMIP5 ensemble projected change (32 GCMs) in annual temperature (top) and precipitation (bottom) by 2040–2059 (left) and by 2080–2090 (right) relative to 1986–2005 baseline under RCP 8.5.",
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig5b.png`
            },
            {
                "title": "CMIP5 ensemble projected change (32 GCMs) in annual temperature (top) and precipitation (bottom) by 2040–2059 (left) and by 2080–2090 (right) relative to 1986–2005 baseline under RCP 8.5.",
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig5c.png`
            },
            {
                "title": "CMIP5 ensemble projected change (32 GCMs) in annual temperature (top) and precipitation (bottom) by 2040–2059 (left) and by 2080–2090 (right) relative to 1986–2005 baseline under RCP 8.5.",
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig5d.png`
            }
            ,
            {
                "title": `Historic and projected average annual temperature in ${countryName} under RCP 2.6 (blue) and RCP 8.5 (red) estimated by the model ensemble. Shading represents the standard deviation of the model ensemble.`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig6.svg`
            },
            ,
            {
                "title": `Projected change (anomaly) in monthly temperature, shown by month, for ${countryName} for the period 2080-2099 under RCP 8.5. The value shown represents the median of the model ensemble with the shaded areas showing the 10th – 90th percentiles.`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig7.svg`
            }
        ]
    }
    return fetchImages;
}
  fetchABDImageData(countryCode,countryName) {
    let fetchImages = 
        [
            {   
                "indexNo":1,
                "figure":`Figure `,
                "title": `The ND-GAIN Index summarizes a country's vulnerability to climate change and other global challenges in combination with its readiness to improve resilience. It aims to help businesses and the public sector better prioritize investments for a more efficient response to the immediate global challenges ahead.`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig1.svg`
            },
            {   
                "indexNo":2,
                "figure":`Figure `,
                "title": `Average monthly temperature and rainfall in ${countryName} (1901-2019)`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig2.svg`
            },
            {
                "indexNo":3,
                "figure":`Figure `,
                "title": `(Left) Annual Mean Temperature, and (Right) Annual Mean Rainfall (mm) in ${countryName} over the period 1901-2019`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig3a.png`
            },
            {   
                "indexNo":4,
                "figure":`Figure `,
                "title": `(Left) Annual Mean Temperature, and (Right) Annual Mean Rainfall (mm) in ${countryName} over the period 1901-2019`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig3b.png`
            },
            {
                "indexNo":5,
                "figure":`Figure `,
                "title": `Projected average temperature anomaly’ and ‘projected annual rainfall anomaly’ in ${countryName}. Outputs of 16 models within the ensemble simulating RCP 8.5 over the period 2080-2099. Models shown represent the subset of models within the ensemble which provide projections across all RCPs and therefore are most robust for comparison.17 Three models are labelled.`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig4.png`
            },
            {
                "indexNo":6,
                "figure":`Figure `,
                "title": "CMIP5 ensemble projected change (32 GCMs) in annual temperature (top) and precipitation (bottom) by 2040–2059 (left) and by 2080–2090 (right) relative to 1986–2005 baseline under RCP 8.5.",
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig5a.png`
            },
            {   
                "indexNo":7,
                "figure":`Figure `,
                "title": "CMIP5 ensemble projected change (32 GCMs) in annual temperature (top) and precipitation (bottom) by 2040–2059 (left) and by 2080–2090 (right) relative to 1986–2005 baseline under RCP 8.5.",
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig5b.png`
            },
            {   
                "indexNo":8,
                "figure":`Figure `,
                "title": "CMIP5 ensemble projected change (32 GCMs) in annual temperature (top) and precipitation (bottom) by 2040–2059 (left) and by 2080–2090 (right) relative to 1986–2005 baseline under RCP 8.5.",
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig5c.png`
            },
            {   
                "indexNo":9,
                "figure":`Figure `,
                "title": "CMIP5 ensemble projected change (32 GCMs) in annual temperature (top) and precipitation (bottom) by 2040–2059 (left) and by 2080–2090 (right) relative to 1986–2005 baseline under RCP 8.5.",
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig5d.png`
            },
            {   
                "indexNo":10,
                "figure":`Figure `,
                "title": `Historic and projected average annual temperature in ${countryName} under RCP 2.6 (blue) and RCP 8.5 (red) estimated by the model ensemble. Shading represents the standard deviation of the model ensemble.`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig6.svg`
            },
            {   
                "indexNo":11,
                "figure":`Figure `,
                "title": `Projected change (anomaly) in monthly temperature, shown by month, for ${countryName} for the period 2080-2099 under RCP 8.5. The value shown represents the median of the model ensemble with the shaded areas showing the 10th – 90th percentiles.`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig7.svg`
            }
        ];
        
        let fetchABDImageDataArr = [];
        this.fetchABDImageDataArr = [];
        for (let ADBi = 0; ADBi < fetchImages.length; ADBi++) {
            const img = new Image();
            img.onload = () => {
              // console.log('RESULT: url=' + fetchImages[i].url + 'exists');
              this.fetchABDImageDataArr.push({"indexNo":fetchImages[ADBi].indexNo,"figure":`Figure`,"title":fetchImages[ADBi].title,"url":fetchImages[ADBi].url}); 
              
            }
            img.onerror = () => {
              // console.log('Error: url=' + fetchImages[i].url + 'Not exists');
            }
            img.src = fetchImages[ADBi].url;
          
        }
    
    return fetchABDImageDataArr;
  }
  fetchGENImageData(countryCode,countryName) {
    const fetchImages = 
        [
            {   
                "indexNo":1,
                "figure":`Figure `,
                "title": `Elevation of ${countryName}<sup>1</sup>`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig1.png`
            },
            {
                "indexNo":2,
                "figure":`Figure `,
                "title": `ND-GAIN Index for ${countryName}`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig2.svg`
            },
            {
                "indexNo":3,
                "figure":`Figure `,
                "title": `Average Monthly Temperature and Rainfall of ${countryName} for 1901-2019`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig3.svg`
            },
            {
                "indexNo":4,
                "figure":`Figure `,
                "title": ` Map of average annual temperature (left); annual precipitation (right) for ${countryName}, 1901-2019`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig4a.png`
            },
            {
                "indexNo":5,
                "figure":`Figure `,
                "title": ` Map of average annual temperature (left); annual precipitation (right) for ${countryName}, 1901-2019`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig4b.png`
            },
            {
                "indexNo":6,
                "figure":`Figure `,
                "title": ` Mean Annual Temperature for ${countryName}, 1901-2019 `,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig5.svg`
            },
            {   
                "indexNo":7,
                "figure":`Figure `,
                "title": "CMIP5 multi-model ensemble projected change (32 GCMs) in annual temperature (top) and precipitation (bottom) by 2040-2059 (left) and by 2080-2099 (right), relative to 1986-2005 baseline under RCP 8.5",
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig6a.png`
            },
            {   
                "indexNo":8,
                "figure":`Figure `,
                "title": "CMIP5 multi-model ensemble projected change (32 GCMs) in annual temperature (top) and precipitation (bottom) by 2040-2059 (left) and by 2080-2099 (right), relative to 1986-2005 baseline under RCP 8.5",
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig6b.png`
            },
            {   
                "indexNo":9,
                "figure":`Figure `,
                "title": "CMIP5 multi-model ensemble projected change (32 GCMs) in annual temperature (top) and precipitation (bottom) by 2040-2059 (left) and by 2080-2099 (right), relative to 1986-2005 baseline under RCP 8.5",
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig6c.png`
            },
            {   
                "indexNo":10,
                "figure":`Figure `,
                "title": "CMIP5 multi-model ensemble projected change (32 GCMs) in annual temperature (top) and precipitation (bottom) by 2040-2059 (left) and by 2080-2099 (right), relative to 1986-2005 baseline under RCP 8.5",
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig6d.png`
            },
            {   
                "indexNo":11,
                "figure":`Figure `,
                "title": `Historical and Projected Average Temperature for ${countryName} from 1986 to 2099`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig7.svg`
            },
            {   
                "indexNo":12,
                "figure":`Figure `,
                "title": "Projected Change in Summer Days (Tmax> 25°C)",
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig8.svg`
            },
            {   
                "indexNo":13,
                "figure":`Figure `,
                "title": `Annual Average Precipitation in ${countryName} for 1986 to 2099`,
                "url": `https://climatedata.worldbank.org/climate/img/country-profile/${countryCode}/Fig9.svg`
            },
        ]
        let fetchGENImageDataArr = [];
        this.fetchGENImageDataArr = [];
        for (let GENi = 0; GENi < fetchImages.length; GENi++) {
            
            const img = new Image();
            img.onload = () => {
              
              this.fetchGENImageDataArr.push({"indexNo":fetchImages[GENi].indexNo,"figure":`Figure`,"title":fetchImages[GENi].title,"url":fetchImages[GENi].url}); 

            }
            img.onerror = () => {
              // console.log('Error: url=' + fetchImages[i].url + 'Not exists');
            }
            img.src = fetchImages[GENi].url;
          
        }
    return fetchGENImageDataArr;
  }
  previewPdf = async (event) => {
    event.preventDefault();
    //saveInformation
    var saveButtonClick = document.getElementById("saveInformation");
    await saveButtonClick.click(); // this will trigger the click event
    window.open('preview-pdf/' + this.countryProfileID);
  }

  async getCountryCode() {
    await this.countryListService.getAllCountryLists().subscribe(
      res => {
        let bigCities = res['countryList'] && res['countryList'].filter(item => item.country == this.countryName);
        this.countryCode = bigCities[0].code
      });
  }

  async ngOnInit() {
    this.loading = true;
        setTimeout(() => {
            this.loading = false;
            }, 6000);
    this.getCountryCode(); // to get the country code of selected country.
    let createSectionData = this.createSection.foreword.content;

    if (createSectionData.length > 0) {

      this.form = this.fb.group({
        forewordMainSection: '',
        forewordFooterLinks: '',
        forewords: this.fb.array([]),
        keyMessageMainSection: '',
        keyMessageFooterLinks: '',
        keyMessages: this.fb.array([])
      });

      this.form.patchValue({
        forewordMainSection: this.createSection.foreword.forewordMainSection,
        forewordFooterLinks: this.createSection.foreword.forewordFooterLinks,
        keyMessageMainSection: this.createSection.foreword.forewordMainSection,
        keyMessageFooterLinks: this.createSection.foreword.forewordFooterLinks

      });

      this.createSection.foreword.content.forEach(t => {
        this.forewordsArray().push(this.fb.group(t));
      });

    }
    else {

      this.form = this.fb.group({
        isCheckedAcknowledgementSection: false,
        isCheckedforewordSection: false,
        isCheckedkeyMessageSection: false,
        isCheckedcountryOverviewSection: false,
        isCheckedclimatologySection: false,
        isCheckedclimateRelatedNaturalHazardSection: false,
        isCheckedclimateChangeImpactSection: false,
        isCheckedpoliciesAndProgramSection: false,
        isCheckedadaptationSection: false,

        countryProfileID: this.countryProfileID,
        acknowledgementMainSection:'',
        isDisplayAcknowledgementSection: true,
        forewordMainSection: '',
        forewordFooterLinks: '',
        forewords: this.fb.array([]),
        isDisplayforewordSection: true,
        keyMessageMainSection: '',
        keyMessageFooterLinks: '',
        keyMessages: this.fb.array([]),
        isDisplaykeyMessageSection: true,
        countryOverviewMainSection: '',
        countryOverviewFooterLinks: '',
        countryOverviews: this.fb.array([]),
        isDisplaycountryOverviewSection: true,
        climatologyMainSection: '',
        climatologyFooterLinks: '',
        climatologySubSectionsType: [this.climatologyOptions[2]],
        climatologys: this.fb.array([]),
        isDisplayclimatologySection: true,
        climateRelatedNaturalHazardMainSection: '',
        climateRelatedNaturalHazardFooterLinks: '',
        climateRelatedNaturalHazardSubSectionsType: new FormControl(null),
        climateRelatedNaturalHazards: this.fb.array([]),
        isDisplayclimateRelatedNaturalHazardSection: true,
        climateChangeImpactMainSection: '',
        climateChangeImpactFooterLinks: '',
        climateChangeImpactSubSectionsType: new FormControl(null),
        climateChangeImpacts: this.fb.array([]),
        isDisplayclimateChangeImpactSection: true,
        policiesAndProgramMainSection: '',
        policiesAndProgramFooterLinks: '',
        policiesAndProgramSubSectionsType: new FormControl(null),
        policiesAndPrograms: this.fb.array([]),
        isDisplaypoliciesAndProgramSection: true,
        adaptationMainSection: '',
        adaptationFooterLinks: '',
        adaptationSubSectionsType: new FormControl(null),
        adaptations: this.fb.array([]),
        isDisplayadaptationSection: true,
      });

      // this.form.controls['climatologySubSectionsType'].setValue('Custom', {onlySelf: true});
      this.form.controls['climateRelatedNaturalHazardSubSectionsType'].setValue('Custom', { onlySelf: true });
      this.form.controls['climateChangeImpactSubSectionsType'].setValue('Custom', { onlySelf: true });
      this.form.controls['policiesAndProgramSubSectionsType'].setValue('Custom', { onlySelf: true });
      this.form.controls['adaptationSubSectionsType'].setValue('Custom', { onlySelf: true });

    }

    this.forewordList = this.form.get('forewords') as FormArray;
    this.keyMessageList = this.form.get('keyMessages') as FormArray;
    this.countryOverviewList = this.form.get('countryOverviews') as FormArray;
    this.climatologyList = this.form.get('climatologys') as FormArray;
    this.climateRelatedNaturalHazardList = this.form.get('climateRelatedNaturalHazards') as FormArray;
    this.climateChangeImpactList = this.form.get('climateChangeImpacts') as FormArray;
    this.policiesAndProgramList = this.form.get('policiesAndPrograms') as FormArray;
    this.adaptationList = this.form.get('adaptations') as FormArray;
    await this.getCountryData();
  }

  ngOnDestroy() {
    if ($('h1,h2,h3,h4,h5').text() === '') {
        $(this).remove();
        console.log("removed");
    }
}

  getCountryData = () => {

    this.countryProfile.getCountryProfile(this.countryProfileID).subscribe(
      res => {
        //get ADB vs general profiles
        this.generalProfile = res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.isGenralProfile
        this.adbProfile = res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.isAdbProfile
       
        if (this.generalProfile === true && this.adbProfile === false) {
          this.climatologyOptions = ['Climate Baseline', 'Climate Future', 'Custom'];
          this.climateRelatedNaturalHazardOptions = ['Overview', 'Climate Change Impacts', 'Implications for DRM', 'Custom'];
          this.climateChangeImpactOptions = ['Agriculture', 'Water', 'Energy', 'Health', 'Transportation', 'Coastal Zone', 'Fisheries', 'Forestry', 'Biodiversity', 'Education', 'Industry', 'Custom'];
          this.policiesAndProgramOptions = ['Custom'];
          this.adaptationOptions = ['Institutional Framework for Adaptation', 'Policy Framework for Adaptation', 'Recommendations', 'Custom'];
        }
        
        if (this.adbProfile === true && this.generalProfile === false) {
          this.climatologyOptions = ['Climate Baseline', 'Climate Future', 'Custom'];
          this.climateRelatedNaturalHazardOptions = ['Heat Waves', 'Drought', 'Flood', 'Tropical Cyclones', 'Storm Surge', 'Custom'];
          this.climateChangeImpactOptions = ['Natural Resources', 'Economic Sectors', 'Communities', 'Custom'];
          this.policiesAndProgramOptions = ['National Adaptation Policies and Strategies', 'Climate Change Priorities of the ADB and WBG', 'Custom'];
          this.adaptationOptions = ['Custom'];
        }

        // get checkbox values
        res['countryProfileData'].acknowledgement && res['countryProfileData'].acknowledgement.isCheckedAcknowledgementSection ?
          this.isCheckedAcknowledgementSection = res['countryProfileData'].acknowledgement.isCheckedAcknowledgementSection : "";
        
        res['countryProfileData'].foreword && res['countryProfileData'].foreword.isCheckedforewordSection ?
          this.isCheckedforewordSection = res['countryProfileData'].foreword.isCheckedforewordSection : "";

        res['countryProfileData'].keyMessage && res['countryProfileData'].keyMessage.isCheckedkeyMessageSection ?
          this.isCheckedkeyMessageSection = res['countryProfileData'].keyMessage.isCheckedkeyMessageSection : "";

        res['countryProfileData'].countryOverview && res['countryProfileData'].countryOverview.isCheckedcountryOverviewSection ?
          this.isCheckedcountryOverviewSection = res['countryProfileData'].countryOverview.isCheckedcountryOverviewSection : "";

        res['countryProfileData'].climatology && res['countryProfileData'].climatology.isCheckedclimatologySection ?
          this.isCheckedclimatologySection = res['countryProfileData'].climatology.isCheckedclimatologySection : "";

        res['countryProfileData'].climateRelatedNaturalHazard && res['countryProfileData'].climateRelatedNaturalHazard.isCheckedclimateRelatedNaturalHazardSection ?
          this.isCheckedclimateRelatedNaturalHazardSection = res['countryProfileData'].climateRelatedNaturalHazard.isCheckedclimateRelatedNaturalHazardSection : "";

        res['countryProfileData'].climateChangeImpact && res['countryProfileData'].climateChangeImpact.isCheckedclimateChangeImpactSection ?
          this.isCheckedclimateChangeImpactSection = res['countryProfileData'].climateChangeImpact.isCheckedclimateChangeImpactSection : "";
        
        res['countryProfileData'].policiesAndProgram && res['countryProfileData'].policiesAndProgram.isCheckedpoliciesAndProgramSection ?
          this.isCheckedpoliciesAndProgramSection = res['countryProfileData'].policiesAndProgram.isCheckedpoliciesAndProgramSection : "";
        
        res['countryProfileData'].adaptation && res['countryProfileData'].adaptation.isCheckedadaptationSection ?
          this.isCheckedadaptationSection = res['countryProfileData'].adaptation.isCheckedadaptationSection : "";
        
        //get country profile title
        this.countryProfileTitle = res['countryProfileData'].countryProfile && res['countryProfileData'].countryProfile.title

        // get field values
        res['countryProfileData'].acknowledgement && this.form.get('acknowledgementMainSection').setValue(res['countryProfileData'].acknowledgement.acknowledgementMainSection, {
          onlySelf: true
        })
        res['countryProfileData'].foreword && this.form.get('forewordMainSection').setValue(res['countryProfileData'].foreword.forewordMainSection, {
          onlySelf: true
        })
        res['countryProfileData'].foreword && this.form.get('forewordFooterLinks').setValue(res['countryProfileData'].foreword.forewordFooterLinks, {
          onlySelf: true
        })
        res['countryProfileData'].keyMessage && this.form.get('keyMessageMainSection').setValue(res['countryProfileData'].keyMessage.keyMessageMainSection, {
          onlySelf: true
        })
        res['countryProfileData'].keyMessage && this.form.get('keyMessageFooterLinks').setValue(res['countryProfileData'].keyMessage.keyMessageFooterLinks, {
          onlySelf: true
        })
        res['countryProfileData'].countryOverview && this.form.get('countryOverviewMainSection').setValue(res['countryProfileData'].countryOverview.countryOverviewMainSection, {
          onlySelf: true
        })
        res['countryProfileData'].countryOverview && this.form.get('countryOverviewFooterLinks').setValue(res['countryProfileData'].countryOverview.countryOverviewFooterLinks, {
          onlySelf: true
        })
        res['countryProfileData'].climatology && this.form.get('climatologyMainSection').setValue(res['countryProfileData'].climatology.climatologyMainSection, {
          onlySelf: true
        })
        res['countryProfileData'].climatology && this.form.get('climatologyFooterLinks').setValue(res['countryProfileData'].climatology.climatologyFooterLinks, {
          onlySelf: true
        })
        res['countryProfileData'].climateRelatedNaturalHazard && this.form.get('climateRelatedNaturalHazardMainSection').setValue(res['countryProfileData'].climateRelatedNaturalHazard.climateRelatedNaturalHazardMainSection, {
          onlySelf: true
        })
        res['countryProfileData'].climateRelatedNaturalHazard && this.form.get('climateRelatedNaturalHazardFooterLinks').setValue(res['countryProfileData'].climateRelatedNaturalHazard.climateRelatedNaturalHazardFooterLinks, {
          onlySelf: true
        })
        res['countryProfileData'].climateChangeImpact && this.form.get('climateChangeImpactMainSection').setValue(res['countryProfileData'].climateChangeImpact.climateChangeImpactMainSection, {
          onlySelf: true
        })
        res['countryProfileData'].climateChangeImpact && this.form.get('climateChangeImpactFooterLinks').setValue(res['countryProfileData'].climateChangeImpact.climateChangeImpactFooterLinks, {
          onlySelf: true
        })
        res['countryProfileData'].policiesAndProgram && this.form.get('policiesAndProgramMainSection').setValue(res['countryProfileData'].policiesAndProgram.policiesAndProgramMainSection, {
          onlySelf: true
        })
        res['countryProfileData'].policiesAndProgram && this.form.get('policiesAndProgramFooterLinks').setValue(res['countryProfileData'].policiesAndProgram.policiesAndProgramFooterLinks, {
          onlySelf: true
        })
        res['countryProfileData'].adaptation && this.form.get('adaptationMainSection').setValue(res['countryProfileData'].adaptation.adaptationMainSection, {
          onlySelf: true
        })
        res['countryProfileData'].adaptation && this.form.get('adaptationFooterLinks').setValue(res['countryProfileData'].adaptation.adaptationFooterLinks, {
          onlySelf: true
        })
        res['countryProfileData'].foreword && this.form.get('adaptationFooterLinks').setValue(res['countryProfileData'].adaptation.adaptationFooterLinks, {
          onlySelf: true
        })
        
        // section hide-show
        if (res['countryProfileData'].acknowledgement && res['countryProfileData'].acknowledgement.isDisplayAcknowledgementSection !== null) {
          this.isDisplayAcknowledgementSection = res['countryProfileData'].acknowledgement && res['countryProfileData'].acknowledgement.isDisplayAcknowledgementSection
        } else {
          this.isDisplayAcknowledgementSection = true;
        }
        if (res['countryProfileData'].foreword && res['countryProfileData'].foreword.isDisplayforewordSection !== null) {
          this.isDisplayforewordSection = res['countryProfileData'].foreword && res['countryProfileData'].foreword.isDisplayforewordSection
        } else {
          this.isDisplayforewordSection = true;
        }
        if (res['countryProfileData'].keyMessage && res['countryProfileData'].keyMessage.isDisplaykeyMessageSection !== null) {
          this.isDisplaykeyMessageSection = res['countryProfileData'].keyMessage && res['countryProfileData'].keyMessage.isDisplaykeyMessageSection
        } else {
          this.isDisplaykeyMessageSection = true;
        }
        if (res['countryProfileData'].countryOverview && res['countryProfileData'].countryOverview.isDisplaycountryOverviewSection !== null) {
          this.isDisplaycountryOverviewSection = res['countryProfileData'].countryOverview && res['countryProfileData'].countryOverview.isDisplaycountryOverviewSection;
        } else {
          this.isDisplaycountryOverviewSection = true;
        }
        if (res['countryProfileData'].climatology && res['countryProfileData'].climatology.isDisplayclimatologySection !== null) {
          this.isDisplayclimatologySection = res['countryProfileData'].climatology && res['countryProfileData'].climatology.isDisplayclimatologySection;
        } else {
          this.isDisplayclimatologySection = true;
        }
        if (res['countryProfileData'].climateRelatedNaturalHazard && res['countryProfileData'].climateRelatedNaturalHazard.isDisplayclimateRelatedNaturalHazardSection !== null) {
          this.isDisplayclimateRelatedNaturalHazardSection = res['countryProfileData'].climateRelatedNaturalHazard && res['countryProfileData'].climateRelatedNaturalHazard.isDisplayclimateRelatedNaturalHazardSection;
        } else {
          this.isDisplayclimateRelatedNaturalHazardSection = true;
        }
        if (res['countryProfileData'].climateChangeImpact && res['countryProfileData'].climateChangeImpact.isDisplayclimateChangeImpactSection !== null) {
          this.isDisplayclimateChangeImpactSection = res['countryProfileData'].climateChangeImpact && res['countryProfileData'].climateChangeImpact.isDisplayclimateChangeImpactSection;
        } else {
          this.isDisplayclimateChangeImpactSection = true;
        }
        if (res['countryProfileData'].policiesAndProgram && res['countryProfileData'].policiesAndProgram.isDisplaypoliciesAndProgramSection !== null) {
          this.isDisplaypoliciesAndProgramSection = res['countryProfileData'].policiesAndProgram && res['countryProfileData'].policiesAndProgram.isDisplaypoliciesAndProgramSection;
        } else {
          this.isDisplaypoliciesAndProgramSection = true;
        }
        if (res['countryProfileData'].adaptation && res['countryProfileData'].adaptation.isDisplayadaptationSection !== null) {
          this.isDisplayadaptationSection = res['countryProfileData'].adaptation && res['countryProfileData'].adaptation.isDisplayadaptationSection;
        } else {
          this.isDisplayadaptationSection = true;
        }


        // sub-sections data fetch and setup
        if (res['countryProfileData'].foreword && res['countryProfileData'].foreword.forewords.length !== null) {
          res['countryProfileData'].foreword.forewords.forEach(item => {
            this.forewordsArray()
              .push(this.fb.group({
               // "isCheckedforewordSubSections": item.isCheckedforewordSubSections,
                "forewordSubSectionsTitle": item.forewordSubSectionsTitle,
                "forewordSubSectionsContent": item.forewordSubSectionsContent
              }));
          });
        }

        if (res['countryProfileData'].keyMessage && res['countryProfileData'].keyMessage.keyMessages.length !== null) {
          res['countryProfileData'].keyMessage.keyMessages.forEach(item => {
            this.keyMessagesArray()
              .push(this.fb.group({
               // "isCheckedkeyMessageSubSections": item.isCheckedkeyMessageSubSections,
                "keyMessageSubSectionsTitle": item.keyMessageSubSectionsTitle,
                "keyMessageSubSectionsContent": item.keyMessageSubSectionsContent
              }));
          });
        }

        if (res['countryProfileData'].countryOverview && res['countryProfileData'].countryOverview.countryOverviews.length !== null) {
          res['countryProfileData'].countryOverview.countryOverviews.forEach(item => {
            this.countryOverviewsArray()
              .push(this.fb.group({
               // "isCheckedcountryOverviewSubSections": item.isCheckedcountryOverviewSubSections,
                "countryOverviewSubSectionsTitle": item.countryOverviewSubSectionsTitle,
                "countryOverviewSubSectionsContent": item.countryOverviewSubSectionsContent
              }));
          });
        }

        if (res['countryProfileData'].climatology && res['countryProfileData'].climatology.climatologys.length !== null) {
          res['countryProfileData'].climatology.climatologys.forEach(item => {
            this.climatologysArray()
              .push(this.fb.group({
               // "isCheckedclimatologySubSections": item.isCheckedclimatologySubSections,
                "climatologySubSectionsTitle": item.climatologySubSectionsTitle,
                "climatologySubSectionsContent": item.climatologySubSectionsContent
              }));
          });
        }

        if (res['countryProfileData'].climateRelatedNaturalHazard && res['countryProfileData'].climateRelatedNaturalHazard.climateRelatedNaturalHazards.length !== null) {
          res['countryProfileData'].climateRelatedNaturalHazard.climateRelatedNaturalHazards.forEach(item => {
            this.climateRelatedNaturalHazardsArray()
              .push(this.fb.group({
               // "isCheckedclimateRelatedNaturalHazardSubSections": item.isCheckedclimateRelatedNaturalHazardSubSections,
                "climateRelatedNaturalHazardSubSectionsTitle": item.climateRelatedNaturalHazardSubSectionsTitle,
                "climateRelatedNaturalHazardSubSectionsContent": item.climateRelatedNaturalHazardSubSectionsContent
              }));
          });
        }

        if (res['countryProfileData'].climateChangeImpact && res['countryProfileData'].climateChangeImpact.climateChangeImpacts.length !== null) {
          res['countryProfileData'].climateChangeImpact.climateChangeImpacts.forEach(item => {
            this.climateChangeImpactsArray()
              .push(this.fb.group({
               // "isCheckedclimateChangeImpactSubSections":item.isCheckedclimateChangeImpactSubSections,
                "climateChangeImpactSubSectionsTitle": item.climateChangeImpactSubSectionsTitle,
                "climateChangeImpactSubSectionsContent": item.climateChangeImpactSubSectionsContent
              }));
          });
        }

        if (res['countryProfileData'].policiesAndProgram && res['countryProfileData'].policiesAndProgram.policiesAndPrograms.length !== null) {
          res['countryProfileData'].policiesAndProgram.policiesAndPrograms.forEach(item => {
            this.policiesAndProgramsArray()
              .push(this.fb.group({
               // "isCheckedpoliciesAndProgramSubSections": item.isCheckedpoliciesAndProgramSubSections,
                "policiesAndProgramSubSectionsTitle": item.policiesAndProgramSubSectionsTitle,
                "policiesAndProgramSubSectionsContent": item.policiesAndProgramSubSectionsContent
              }));
          });
        }

        if (res['countryProfileData'].adaptation && res['countryProfileData'].adaptation.adaptations.length !== null) {
          res['countryProfileData'].adaptation.adaptations.forEach(item => {                
            this.adaptationsArray()
              .push(this.fb.group({
                //"isCheckedadaptationSubSections":item.isCheckedadaptationSubSections,
                "adaptationSubSectionsTitle":  item.adaptationSubSectionsTitle ,
                "adaptationSubSectionsContent": item.adaptationSubSectionsContent
              }));
          });
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  moveUp(index: number, arrayName: any) {
    if (index >= 1)
      this.swap(index, index - 1, arrayName)
  }

  moveDown(index: number, arrayName: any) {
    if (index < arrayName.length - 1)
      this.swap(index, index + 1, arrayName)
  }

  private swap(x: any, y: any, arrayName: any) {
    var b = arrayName[x];
    arrayName[x] = arrayName[y];
    arrayName[y] = b;
  }

  createForeword(): FormGroup {
    return this.fb.group({
     // isCheckedforewordSubSections: true,
      forewordSubSectionsTitle: ['', [Validators.required]],
      forewordSubSectionsContent: '',

    });
  }
  createkeyMessage(): FormGroup {
    return this.fb.group({
     //isCheckedkeyMessageSubSections: true,
      keyMessageSubSectionsTitle: ['', [Validators.required]],
      keyMessageSubSectionsContent: '',
    });
  }
  createcountryOverview(): FormGroup {
    return this.fb.group({
     //isCheckedcountryOverviewSubSections: true,
      countryOverviewSubSectionsTitle: ['', [Validators.required]],
      countryOverviewSubSectionsContent: '',
    });
  }
  createclimatology(): FormGroup {
    return this.fb.group({
      //isCheckedclimatologySubSections: true,
      climatologySubSectionsTitle: ['', [Validators.required]],
      climatologySubSectionsContent: '',
    });
  }
  createclimateRelatedNaturalHazard(): FormGroup {
    return this.fb.group({
     //isCheckedclimateRelatedNaturalHazardSubSections: true,
      climateRelatedNaturalHazardSubSectionsTitle: ['', [Validators.required]],
      climateRelatedNaturalHazardSubSectionsContent: '',
    });
  }
  createclimateChangeImpact(): FormGroup {
    return this.fb.group({
      //isCheckedclimateChangeImpactSubSections: true,
      climateChangeImpactSubSectionsTitle: ['', [Validators.required]],
      climateChangeImpactSubSectionsContent: '',
    });
  }
  createpoliciesAndProgram(): FormGroup {
    return this.fb.group({
     // isCheckedpoliciesAndProgramSubSections: true,
      policiesAndProgramSubSectionsTitle: ['', [Validators.required]],
      policiesAndProgramSubSectionsContent: '',
    });
  }
  createadaptation(): FormGroup {
    return this.fb.group({
      //isCheckedadaptationSubSections: true,
      adaptationSubSectionsTitle: ['', [Validators.required]],
      adaptationSubSectionsContent: '',
    });
  }

  
  get forewordFormGroup() {
    return this.form.get('forewords') as FormArray;
  }
  get keyMessageFormGroup() {
    return this.form.get('keyMessages') as FormArray;
  }
  get countryOverviewFormGroup() {
    return this.form.get('countryOverviews') as FormArray;
  }
  get climatologyFormGroup() {
    return this.form.get('climatologys') as FormArray;
  }
  get climateRelatedNaturalHazardFormGroup() {
    return this.form.get('climateRelatedNaturalHazards') as FormArray;
  }
  get climateChangeImpactFormGroup() {
    return this.form.get('climateChangeImpacts') as FormArray;
  }
  get policiesAndProgramFormGroup() {
    return this.form.get('policiesAndPrograms') as FormArray;
  }
  get adaptationFormGroup() {
    return this.form.get('adaptations') as FormArray;
  }


  forewordsArray(): FormArray {
    return this.form.get('forewords') as FormArray;
  }
  keyMessagesArray(): FormArray {
    return this.form.get('keyMessages') as FormArray;
  }
  countryOverviewsArray(): FormArray {
    return this.form.get('countryOverviews') as FormArray;
  }
  climatologysArray(): FormArray {
    return this.form.get('climatologys') as FormArray;
  }
  climateRelatedNaturalHazardsArray(): FormArray {
    return this.form.get('climateRelatedNaturalHazards') as FormArray;
  }
  climateChangeImpactsArray(): FormArray {
    return this.form.get('climateChangeImpacts') as FormArray;
  }
  policiesAndProgramsArray(): FormArray {
    return this.form.get('policiesAndPrograms') as FormArray;
  }
  adaptationsArray(): FormArray {
    return this.form.get('adaptations') as FormArray;
  }

  addforewordSubSection() {
    this.forewordList.push(this.createForeword());
  }
  addkeyMessageSubSection() {
    this.keyMessageList.push(this.createkeyMessage());
  }
  addcountryOverviewSubSection() {
    this.countryOverviewList.push(this.createcountryOverview());
  }
  addclimatologySubSection() {
    if (this.form.value.climatologySubSectionsType == "Custom") {
      this.climatologyList.push(this.createclimatology());
    }
    else {
      this.climatologysArray().push(this.fb.group({ "climatologySubSectionsTitle": new FormControl({ value: this.form.value.climatologySubSectionsType, disabled: true }), "climatologySubSectionsContent": "" }));

      const indexclimatology = this.climatologyOptions.indexOf(this.form.value.climatologySubSectionsType, 0);
      //console.log(indexclimatology);
      if (indexclimatology > -1) {
        this.climatologyOptions.splice(indexclimatology, 1);
      }

      if (this.climatologyOptions.length) {
        this.form.controls['climatologySubSectionsType'].setValue(this.climatologyOptions[0]);
      }
    }
  }
  addclimateRelatedNaturalHazardSubSection() {
    if (this.form.value.climateRelatedNaturalHazardSubSectionsType == "Custom") {
      this.climateRelatedNaturalHazardList.push(this.createclimateRelatedNaturalHazard());
    }
    else {
      this.climateRelatedNaturalHazardsArray().push(this.fb.group({ "climateRelatedNaturalHazardSubSectionsTitle": new FormControl({ value: this.form.value.climateRelatedNaturalHazardSubSectionsType, disabled: true }), "climateRelatedNaturalHazardSubSectionsContent": "" }));

      const indexclimateRelatedNaturalHazard = this.climateRelatedNaturalHazardOptions.indexOf(this.form.value.climateRelatedNaturalHazardSubSectionsType, 0);
      //console.log(indexclimateRelatedNaturalHazard);
      if (indexclimateRelatedNaturalHazard > -1) {
        this.climateRelatedNaturalHazardOptions.splice(indexclimateRelatedNaturalHazard, 1);
      }

      if (this.climateRelatedNaturalHazardOptions.length) {
        this.form.controls['climateRelatedNaturalHazardSubSectionsType'].setValue(this.climateRelatedNaturalHazardOptions[0]);
      }
    }
  }
  addclimateChangeImpactSubSection() {

    if (this.form.value.climateChangeImpactSubSectionsType == "Custom") {
      this.climateChangeImpactList.push(this.createclimateChangeImpact());
    }
    else {
      this.climateChangeImpactsArray().push(this.fb.group({ "climateChangeImpactSubSectionsTitle": new FormControl({ value: this.form.value.climateChangeImpactSubSectionsType, disabled: true }), "climateChangeImpactSubSectionsContent": "" }));

      const indexclimateChangeImpact = this.climateChangeImpactOptions.indexOf(this.form.value.climateChangeImpactSubSectionsType, 0);
      //console.log(indexclimateChangeImpact);
      if (indexclimateChangeImpact > -1) {
        this.climateChangeImpactOptions.splice(indexclimateChangeImpact, 1);
      }
      if (this.climateChangeImpactOptions.length) {
        this.form.controls['climateChangeImpactSubSectionsType'].setValue(this.climateChangeImpactOptions[0]);
      }
    }
  }
  addpoliciesAndProgramSubSection() {

    if (this.form.value.policiesAndProgramSubSectionsType == "Custom") {
      this.policiesAndProgramList.push(this.createpoliciesAndProgram());
    }
    else {
      this.policiesAndProgramsArray().push(this.fb.group({ "policiesAndProgramSubSectionsTitle": new FormControl({ value: this.form.value.policiesAndProgramSubSectionsType, disabled: true }), "policiesAndProgramSubSectionsContent": "" }));

      const indexpoliciesAndProgram = this.policiesAndProgramOptions.indexOf(this.form.value.policiesAndProgramSubSectionsType, 0);
      //console.log(indexpoliciesAndProgram);
      if (indexpoliciesAndProgram > -1) {
        this.policiesAndProgramOptions.splice(indexpoliciesAndProgram, 1);
      }

      if (this.policiesAndProgramOptions.length) {
        this.form.controls['policiesAndProgramSubSectionsType'].setValue(this.policiesAndProgramOptions[0]);
      }
    }
  }
  addadaptationSubSection() {

    if (this.form.value.adaptationSubSectionsType == "Custom") {
      this.adaptationList.push(this.createadaptation());
    }
    else {
      this.adaptationsArray().push(this.fb.group({ "adaptationSubSectionsTitle": new FormControl({ value: this.form.value.adaptationSubSectionsType, disabled: true }), "adaptationSubSectionsContent": "" }));

      const indexadaptation = this.adaptationOptions.indexOf(this.form.value.adaptationSubSectionsType, 0);
      // console.log(indexadaptation);
      if (indexadaptation > -1) {
        this.adaptationOptions.splice(indexadaptation, 1);
      }
      if (this.adaptationOptions.length) {
        this.form.controls['adaptationSubSectionsType'].setValue(this.adaptationOptions[0]);
      }
    }
  }

  removeforewordSubSection(index) {

    this.forewordList.removeAt(index);
  }
  removekeyMessageSubSection(index) {
    this.keyMessageList.removeAt(index);
  }
  removecountryOverviewSubSection(index) {
    this.countryOverviewList.removeAt(index);
  }
  removeclimatologySubSection(index) {
    const deletedclimatology = this.staticclimatologyOptions.indexOf(this.climatologyList.getRawValue()[index].climatologySubSectionsTitle, 0);
    if (deletedclimatology > -1) {
      this.climatologyOptions.push(this.climatologyList.getRawValue()[index].climatologySubSectionsTitle);
    }
    this.climatologyList.removeAt(index);
  }
  removeclimateRelatedNaturalHazardSubSection(index) {
    const deletedclimateRelatedNaturalHazard = this.staticclimateRelatedNaturalHazardOptions.indexOf(this.climateRelatedNaturalHazardList.getRawValue()[index].climateRelatedNaturalHazardSubSectionsTitle, 0);
    if (deletedclimateRelatedNaturalHazard > -1) {
      this.climateRelatedNaturalHazardOptions.push(this.climateRelatedNaturalHazardList.getRawValue()[index].climateRelatedNaturalHazardSubSectionsTitle);
    }
    this.climateRelatedNaturalHazardList.removeAt(index);
  }
  removeclimateChangeImpactSubSection(index) {
    const deletedclimateChangeImpact = this.staticclimateChangeImpactOptions.indexOf(this.climateChangeImpactList.getRawValue()[index].climateChangeImpactSubSectionsTitle, 0);
    if (deletedclimateChangeImpact > -1) {
      this.climateChangeImpactOptions.push(this.climateChangeImpactList.getRawValue()[index].climateChangeImpactSubSectionsTitle);
    }
    this.climateChangeImpactList.removeAt(index);
  }
  removepoliciesAndProgramSubSection(index) {
    const deletedpoliciesAndProgram = this.staticpoliciesAndProgramOptions.indexOf(this.policiesAndProgramList.getRawValue()[index].policiesAndProgramSubSectionsTitle, 0);
    if (deletedpoliciesAndProgram > -1) {
      this.policiesAndProgramOptions.push(this.policiesAndProgramList.getRawValue()[index].policiesAndProgramSubSectionsTitle);
    }
    this.policiesAndProgramList.removeAt(index);
  }
  removeadaptationSubSection(index) {
    const deletedadaptation = this.staticadaptationOptions.indexOf(this.adaptationList.getRawValue()[index].adaptationSubSectionsTitle, 0);
    if (deletedadaptation > -1) {
      this.adaptationOptions.push(this.adaptationList.getRawValue()[index].adaptationSubSectionsTitle);
    }
    this.adaptationList.removeAt(index);
  }

  getForewordFormGroup(index): FormGroup {
    const formGroup = this.forewordList.controls[index] as FormGroup;
    return formGroup;
  }
  getkeyMessageFormGroup(index): FormGroup {
    const formGroup = this.keyMessageList.controls[index] as FormGroup;
    return formGroup;
  }
  getcountryOverviewFormGroup(index): FormGroup {
    const formGroup = this.countryOverviewList.controls[index] as FormGroup;
    return formGroup;
  }
  getclimatologyFormGroup(index): FormGroup {
    const formGroup = this.climatologyList.controls[index] as FormGroup;
    return formGroup;
  }
  getclimateRelatedNaturalHazardFormGroup(index): FormGroup {
    const formGroup = this.climateRelatedNaturalHazardList.controls[index] as FormGroup;
    return formGroup;
  }
  getclimateChangeImpactFormGroup(index): FormGroup {
    const formGroup = this.climateChangeImpactList.controls[index] as FormGroup;
    return formGroup;
  }
  getpoliciesAndProgramFormGroup(index): FormGroup {
    const formGroup = this.policiesAndProgramList.controls[index] as FormGroup;
    return formGroup;
  }
  getadaptationFormGroup(index): FormGroup {
    const formGroup = this.adaptationList.controls[index] as FormGroup;
    return formGroup;
  }

  isActiveAcknowledgementSection() {
    this.isCheckedAcknowledgementSection = this.isCheckedAcknowledgementSection,
    this.isDisplayAcknowledgementSection = true;
  }
  isInActiveAcknowledgementSection() {
    this.isCheckedAcknowledgementSection = this.isCheckedAcknowledgementSection,
    this.isDisplayAcknowledgementSection = false;
  }
  isActiveForewordSection() {
    this.isDisplayforewordSection = true;
  }
  isInActiveForewordSection() {
    this.isDisplayforewordSection = false;
  }
  isActivekeyMessageSection() {
    this.isDisplaykeyMessageSection = true;
  }
  isInActivekeyMessageSection() {
    this.isDisplaykeyMessageSection = false;
  }
  isActivecountryOverviewSection() {
    this.isDisplaycountryOverviewSection = true;
  }
  isInActivecountryOverviewSection() {
    this.isDisplaycountryOverviewSection = false;
  }
  isActiveclimatologySection() {
    this.isDisplayclimatologySection = true;
  }
  isInActiveclimatologySection() {
    this.isDisplayclimatologySection = false;
  }
  isActiveclimateRelatedNaturalHazardSection() {
    this.isDisplayclimateRelatedNaturalHazardSection = true;
  }
  isInActiveclimateRelatedNaturalHazardSection() {
    this.isDisplayclimateRelatedNaturalHazardSection = false;
  }
  isActiveclimateChangeImpactSection() {
    this.isDisplayclimateChangeImpactSection = true;
  }
  isInActiveclimateChangeImpactSection() {
    this.isDisplayclimateChangeImpactSection = false;
  }
  isActivepoliciesAndProgramSection() {
    this.isDisplaypoliciesAndProgramSection = true;
  }
  isInActivepoliciesAndProgramSection() {
    this.isDisplaypoliciesAndProgramSection = false;
  }
  isActiveadaptationSection() {
    this.isDisplayadaptationSection = true;
  }
  isInActiveadaptationSection() {
    this.isDisplayadaptationSection = false;
  }
 
  //toggle sub sections
  toggleforewordSubSection(event) {
    console.log(event.target.checked)
  }

  togglekeyMessageSubSections(event) {
    console.log(event.target.checked)
  }

  togglecountryOverviewSubSection(event) {
    console.log(event.target.checked)
  }

  toggleclimatologySubSections(event) {
    console.log(event.target.checked)
  }

  toggleclimateRelatedNaturalHazardSubSections(event) {
    console.log(event.target.checked)
  }

  toggleclimateChangeImpactSubSections(event) {
    console.log(event.target.checked)
  }

  togglepoliciesAndProgramSubSections(event) {
    console.log(event.target.checked)
  }

  toggleadaptationSubSections(event) {
    console.log(event.target.checked)
  }

  // //toggle sections
  toggleAcknowledgementSection(event) {
    event.target.checked ? this.isCheckedAcknowledgementSection = true : this.isCheckedAcknowledgementSection = false;
  }

  toggleForewordSection(event) {
    event.target.checked ? this.isCheckedforewordSection = true : this.isCheckedforewordSection = false;
  }

  toggleKeyMessageSection(event) {
    event.target.checked ? this.isCheckedkeyMessageSection = true : this.isCheckedkeyMessageSection = false;
  }

  togglecountryOverviewSection(event) {
    event.target.checked ? this.isCheckedcountryOverviewSection = true : this.isCheckedcountryOverviewSection = false;
  }

  toggleClimatologySection(event) {
    event.target.checked ? this.isCheckedclimatologySection = true : this.isCheckedclimatologySection = false;
  }

  toggleClimateRelatedNaturalHazardSection(event) {
    event.target.checked ? this.isCheckedclimateRelatedNaturalHazardSection = true : this.isCheckedclimateRelatedNaturalHazardSection = false;
  }

  toggleClimateChangeImpactSection(event) {
    event.target.checked ? this.isCheckedclimateChangeImpactSection = true : this.isCheckedclimateChangeImpactSection = false;
  }

  togglePoliciesAndProgramSection(event) {
    event.target.checked ? this.isCheckedpoliciesAndProgramSection = true : this.isCheckedpoliciesAndProgramSection = false;
  }

  toggleAdaptationSection(event) {
    event.target.checked ? this.isCheckedadaptationSection = true : this.isCheckedadaptationSection = false;
  }

  fatchAPIacknowledgementSection() {
    if (this.generalProfile === true && this.adbProfile === false) {
        this.form.patchValue({
            acknowledgementMainSection: `<h3><span style="color: rgb(0, 170, 229); text-decoration: inherit;"><br></span></h3><h3><span style="color: rgb(0, 170, 229); text-decoration: inherit;"><br></span></h3><h3><span style="color: rgb(0, 170, 229); text-decoration: inherit;"><br></span></h3><h3><span style="color: rgb(0, 170, 229); text-decoration: inherit;"><br></span></h3><h3><span style="color: rgb(0, 170, 229); text-decoration: inherit;"><br></span></h3><h3><span style="color: rgb(0, 170, 229); text-decoration: inherit;"><br></span></h3><h3><span style="color: rgb(0, 170, 229); text-decoration: inherit;"><br></span></h3><h3><span style="color: rgb(0, 170, 229); text-decoration: inherit;"><br></span></h3><h3><span style="color: rgb(0, 170, 229); text-decoration: inherit;"><br></span></h3><h3><span style="color: rgb(0, 170, 229); text-decoration: inherit;"><br></span></h3><h3><span style="text-decoration-line: inherit; letter-spacing: 0.9px; background-color: unset; text-align: inherit;">COPYRIGHT</span><br></h3><p><span lang="EN-US">© 2020 by the World Bank Group</span></p><p><span lang="EN-US">1818 H Street NW, Washington, DC 20433</span></p><p><span lang="EN-US">Telephone: 202-473-1000; Internet: </span><span lang="EN-US"><span><a href="http://www.worldbank.org" title="" target="_blank">www.worldbank.org</a></span></span><span lang="EN-US"> </span></p><p><span lang="EN-US"><br></span></p><p><span lang="EN-US">This work is a product of the staff of the World Bank Group (WBG) and with external contributions. The opinions, findings, interpretations, and conclusions expressed in this work are those of the authors and do not necessarily reflect the views or the official policy or position of the WBG, its Board of Executive Directors, or the governments it represents.</span></p><p><span lang="EN-US"><br></span></p><p><span lang="EN-US">The WBG does not guarantee the accuracy of the data included in this work and do not make any warranty, express or implied, nor assume any liability or responsibility for any consequence of their use. This publication follows the WBG's practice in references to member designations, borders, and maps. The boundaries, colors, denominations, and other information shown on any map in this work, or the use of the term "country" do not imply any judgment on the part of the WBG, its Boards, or the governments it represents, concerning the legal status of any territory or geographic area or the endorsement or acceptance of such boundaries. </span></p><p><span lang="EN-US"><br></span></p><p><span lang="EN-US">The mention of any specific companies or products of manufacturers does not imply that they</span><span lang="EN-US"> are endorsed or recommended by the WBG in preference to others of a similar nature that are not mentioned. </span></p><p><br></p><h3>RIGHTS AND PERMISSIONS</h3><p><span lang="EN-US">The material in this work is subject to copyright. Because the WBG encourages dissemination of its knowledge, this work may be reproduced, in whole or in part, for noncommercial purposes as long as full attribution to this work is given.</span></p><p><span lang="EN-US"><br></span></p><p><span lang="EN-US">Please cite the work as follows:</span><span lang="EN-US"> </span><span lang="EN-US">Climate Risk Profile: <span> ${this.countryName} </span> (2020): The World Bank Group.</span></p><p><span lang="EN-US"><br></span></p><p><span lang="EN-US">Any queries on rights and licenses, including subsidiary rights, should be addressed to World Bank Publications, The</span><span lang="EN-US"> World Bank Group, 1818 H Street NW, Washington, DC 20433, USA; fax: 202-522-2625; e-mail: </span><span lang="EN-US"><a style="word-break: break-word" href="http://mailto:pubrights@worldbank.org" title="" target="_blank">pubrights@worldbank.org</a></span><span lang="EN-US">.</span></p><p><span lang="EN-US"><br></span></p><p><i><span lang="EN-US">Cover Photos:</span></i><span lang="EN-US"> © Oscar Mendoza / Food and Agricultural Organization, “El Salvador familias productoras” May 24, 2017” via </span><span lang="EN-US"><a href="https://www.flickr.com/photos/136748408@N02/35572671212/" title="" target="_blank">Flickr, Creative Commons CC BY-NC-ND 2.0</a></span><span lang="EN-US">. © David Stanley “Church in Izalco” May 13, 2012 via </span><span lang="EN-US"><a href="https://www.flickr.com/photos/136748408@N02/35572671212/" title="" target="_blank">Flickr, Creative Commons CC BY-NC-ND 2.0</a></span><span lang="EN-US">.</span></p><p><span lang="EN-US"><br></span></p><p><span lang="EN-US"><i><span lang="EN-US">Graphic Design:</span></i><span lang="EN-US"> Circle Graphics, Washington, DC.</span><br></span></p><p><br></p><h3><span>ACKNOWLEDGEMENTS</span></h3><div><span style="color: rgb(35, 31, 32); letter-spacing: 0.3px; background-color: unset; text-align: inherit;">This profile is part of a series of Climate Risk Country Profiles developed by the World Bank Group (WBG). The country profile synthesizes most relevant data and information on climate change, disaster risk reduction, and adaptation actions and policies at the country level. The country profile series are designed as a quick reference source for development practitioners to better integrate climate resilience in development planning and policy making. This effort is managed and led by Ana E. Bucher (Senior Climate Change Specialist, WBG).</span><br></div><p><br></p><p>This profile was written by MacKenzie Dove (Senior Climate Change Consultant, WBG). Additional support was provided by Yunziyi Lang (Climate Change Analyst, WBG).</p><p><br></p><p>Climate and climate-related information is largely drawn from the <a style="word-break: break-word;" href="https://climateknowledgeportal.worldbank.org/" title="" target="_blank">Climate Change Knowledge Portal (CCKP)</a>, a WBG online platform with available global climate data and analysis based on the latest <a style="word-break: break-word;" href="https://www.ipcc.ch/reports/" title="" target="_blank">Intergovernmental Panel on Climate Change (IPCC)</a> reports and datasets. The team is grateful for all comments and suggestions received from the sector, regional, and country development specialists, as well as climate research scientists and institutions for their advice and guidance on use of climate related datasets.</p>`,
        });
    }
    if (this.generalProfile === false && this.adbProfile === true) {
        this.form.patchValue({
            acknowledgementMainSection: `<h3><span style="color: rgb(0, 170, 229); text-decoration: inherit;"><br></span></h3><h3><span style="color: rgb(0, 170, 229); text-decoration: inherit;"><br></span></h3><h3><span style="background-color: unset; letter-spacing: 0.9px; text-align: inherit; text-decoration-line: inherit;">COPYRIGHT</span><br></h3><p><span>© 2020 by the World Bank Group</span></p><p><span>1818 H Street NW, Washington, DC 20433</span></p><p><span>Telephone: 202-473-1000; Internet: </span><span><span><a href="http://www.worldbank.org" title="" target="_blank">www.worldbank.org</a></span></span><span> </span></p><p><span><br></span></p><p><span>Asian Development Bank</span></p><p><span>6 ADB Avenue, Mandaluyong City, 1550 Metro Manila, Philippines</span></p><p><span>Tel +63 2 8632 4444; Fax +63 2 8636 2444</span></p><p><span><a href="http://www.worldbank.org/" title="" target="_blank">www.adb.org</a><br></span></p><p><span><br></span></p><p><span>This work is a product of the staff of the World Bank Group (WBG) and the Asian Development Bank (ADB) and with external contributions. The opinions, findings, interpretations, and conclusions expressed in this work are those of the authors’ and do not necessarily reflect the views or the official policy or position of the WBG, its Board of Executive Directors, or the governments it represents or of ADB, its Board of Governors, or the governments they represent.</span></p><p><span><br></span></p><p><span>The WBG and ADB do not guarantee the accuracy of the data included in this work and do not make any warranty, express or implied, nor assume any liability or responsibility for any consequence of their use. This publication follows the WBG's practice in references to member designations, borders, and maps. ADB, however, recognizes “China” as the People’s Republic of China. The boundaries, colors, denominations, and other information shown on any map in this work, or the use of the term "country" do not imply any judgment on the part of the WBG or ADB, their respective Boards, or the governments they represent, concerning the legal status of any territory or geographic area or the endorsement or acceptance of such boundaries.</span></p><p><span><br></span></p><p><span>The mention of any specific companies or products of manufacturers does not imply that they are endorsed or recommended by either the WBG or ADB in preference to others of a similar nature that are not mentioned.</span></p><p><br></p><h3>RIGHTS AND PERMISSIONS</h3><p><span>The material in this work is subject to copyright. Because the WB and ADB encourage dissemination of their knowledge, this work may be reproduced, in whole or in part, for non commercial purposes as long as full attribution to this work is given. This work is licensed under the Creative Commons Attribution-NonCommercial 3.0 IGO License. To view a copy of this license, visit </span><span><a href="http://creativecommons.org/licenses/by-nc/3.0/igo/"><span>http://creativecommons.org/licenses/by-nc/3.0/igo/</span></a></span><span> or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.</span></p><p><br></p><p><span>Please cite the work as follows: Climate Risk Profile:&nbsp;<span>${this.countryName}</span> (2020): The World Bank Group and Asian Development Bank.</span></p><p><br></p><p><span>Any queries on rights and licenses, including subsidiary rights, should be addressed to World Bank Publications, The World Bank Group, 1818 H Street NW, Washington, DC 20433, USA; fax: 202-522-2625; e-mail: pubrights@worldbank.org.</span></p><p><br></p><p><i><span>Cover Photos:</span></i><span> <span>©Armine Grigoryan /World Bank, “</span></span><a href="https://www.flickr.com/photos/worldbank/7468289824/in/album-72157623022871729/"><span>The road to Tatev Monastery</span></a><span>” May 31, 2012 via Flickr, Creative Commons CCBY-NC-ND 2.0. © Vigen Sargsyan / World Bank, “</span><a href="https://www.flickr.com/photos/worldbank/7468960560/in/album-72157623022871729/"><span>Rehabilitation of Marmarik dam</span></a><span></span><span>” May 3, 2011 via Flickr, Creative Commons CC BY-NCND 2.0.</span></p><p><span><br></span></p><p><span><i><span>Graphic Design:</span></i><span>&nbsp;</span><br></span></p><p><br></p><h3><span>ACKNOWLEDGEMENTS</span></h3><p><span>This profile is part of a series of Climate Risk Country Profiles that are jointly developed by the World Bank Group (WBG) and the Asian Development Bank (ADB). These profiles synthesize the most relevant data and information on climate change, disaster risk reduction, and adaptation actions and policies at the country level. The profile is designed as a quick reference source for development practitioners to better integrate climate resilience in development planning and policy making. This effort is co-led by Ana E. Bucher (Senior Climate Change Specialist, WBG) and Arghya Sinha Roy (Senior Climate Change Specialist, ADB).<span>&nbsp;&nbsp;&nbsp;</span></span></p><p><br></p><p><span>This profile was written by Alex Chapman (Consultant, ADB), William Davies (Consultant, ADB) and Ciaran Downey (Consultant). Technical review of the profiles was undertaken by Robert L. Wilby (Loughborough University). Additional support was provided by Yunziyi Lang (Climate Change Analyst, WBG), MacKenzie Dove (Senior Climate Change Consultant, WBG), Adele Casorla-Castillo (Consultant, ADB), and Charles Rodgers (Consultant, ADB). This profile also benefitted from inputs of WBG and ADB regional staffs.</span></p><p><br></p><p>Climate and climate-related information is largely drawn from the <a style="word-break: break-word;" href="https://climateknowledgeportal.worldbank.org/">Climate Change Knowledge Portal (CCKP)</a>, a WBG online platform with available global climate data and analysis based on the latest <a style="word-break: break-word;" href="https://www.ipcc.ch/reports/">Intergovernmental Panel on Climate Change (IPCC)</a> reports and datasets. The team is grateful for all comments and suggestions received from the sector, regional, and country development specialists, as well as climate research scientists and institutions for their advice and guidance on use of climate related datasets.</p>`,
        });
    }

}

fatchAPIforewordSection() {
  if (this.generalProfile === true && this.adbProfile === false) {
      this.form.patchValue({
          forewordMainSection: `<p><span>Climate change is a major risk to good development outcomes, and the World Bank Group is committed to playing an important role in helping countries integrate climate action into their core development agendas. The World Bank Group is committed to supporting client countries to invest in and build a low-carbon, climate-resilient future, helping them to be better prepared to adapt to current and future climate impacts.</span></p><p><span><br></span></p><p><span>The World Bank Group is investing in incorporating and systematically managing climate risks in development operations through its individual corporate commitments.</span></p><p><span><br></span></p><p><span>A key aspect of the World Bank Group’s Action Plan on Adaptation and Resilience (2019) is to help countries shift from addressing adaptation as an incremental cost and isolated investment to systematically incorporating climate risks and opportunities at every phase of policy planning, investment design, implementation and evaluation of development outcomes. For all IDA and IBRD operations, climate and disaster risk screening is one of the mandatory corporate climate commitments. This is supported by the World Bank Group’s Climate and Disaster Risk Screening Tool which enables all Bank staff to assess short- and long-term climate and disaster risks in operations and national or sectoral planning processes. This screening tool draws up-to-date and relevant information from the World Bank Group’s Climate Change Knowledge Portal, a comprehensive online 'one stop shop' for global, regional, and country data related to climate change and development.</span></p><p><span><br></span></p><p><span>Recognizing the value of consistent, easy-to-use technical resources for client countries as well as to support respective internal climate risk assessment and adaptation planning processes, the World Bank Group’s Climate Change Group has developed this content. Standardizing and pooling expertise facilitates the World Bank Group in conducting initial assessments of climate risks and opportunities across sectors within a country, within institutional portfolios across regions, and acts as a global resource for development practitioners.</span></p><p><span><br></span></p><p><span>For developing countries, the climate risk profiles are intended to serve as public goods to facilitate upstream country diagnostics, policy dialogue, and strategic planning by providing comprehensive overviews of trends and projected changes in key climate parameters, sector-specific implications, relevant policies and programs, adaptation priorities and opportunities for further actions.</span></p><p><span><br></span></p><p><span>It is my hope that these efforts will spur deepening of long-term risk management in developing countries and our engagement in supporting climate change adaptation planning at operational levels.</span></p><p><br></p><p><img src="${environment.apiUploadFileUrl}/Bernice.png" width="72" height="86" class="e-rte-image e-imginline e-resize" alt="Bernice.png"  height="auto"><br class="Apple-interchange-newline"><strong>Bernice Van Bronkhorst</strong><br></p><p>Global Director</p><p>Climate Change Group</p><p>The World Bank Group (WBG)</p><p><br></p><p> </p>`,
          forewordFooterLinks: ``,
      });
  }
  if (this.generalProfile === false && this.adbProfile === true) {
      this.form.patchValue({
          forewordMainSection: `<p><span>Climate change is a major risk to good development outcomes, and the World Bank Group is committed to playing an important role in helping countries integrate climate action into their core development agendas. The World Bank Group and the Asian Development Bank are committed to supporting client countries to invest in and build a low-carbon, climate-resilient future, helping them to be better prepared to adapt to current and future climate impacts.</span></p><p><br></p><p><span>Both institutions are investing in incorporating and systematically managing climate risks in development operations through their individual corporate commitments.</span></p><p><br></p><p><span>For the World Bank Group: a key aspect of the World Bank Group’s Action Plan on Adaptation and Resilience (2019) is to help countries shift from addressing adaptation as an incremental cost and isolated investment to systematically incorporating climate risks and opportunities at every phase of policy planning, investment design, implementation and evaluation of development outcomes. For all IDA and IBRD operations, climate and disaster risk screening is one of the mandatory corporate climate commitments. This is supported by the World Bank Group’s Climate and Disaster Risk Screening Tool which enables all Bank staff to assess short- and long-term climate and disaster risks in operations and national or sectoral planning processes. This screening tool draws up-to-date and relevant information from the World Bank’s Climate Change Knowledge Portal, a comprehensive online ‘one stop shop’ for global, regional, and country data related to climate change and development.</span></p><p><br></p><p><span>For the Asian Development Bank (ADB): its Strategy 2030 identified “tackling climate change, building climate and disaster resilience, and enhancing environmental sustainability” as one of its seven operational priorities. Its Climate Change Operational Framework 2017-2030 identified mainstreaming climate considerations into corporate strategies and policies, sector and thematic operational plans, country programming, and project design, implementation, monitoring, and evaluation of climate change considerations as the foremost institutional measure to deliver its commitments under Strategy 2030. ADB’s climate risk management framework requires all projects to undergo climate risk screening at the concept stage and full climate risk and adaptation assessments for projects with medium to high risk. </span></p><p><br></p><p><span>Recognizing the value of consistent, easy-to-use technical resources for our common client countries as well as to support respective internal climate risk assessment and adaptation planning processes, the World Bank Group’s Climate Change Group and ADB’s Sustainable Development and Climate Change Department have worked together to develop this content. Standardizing and pooling expertise facilitates each institution in conducting initial assessments of climate risks and opportunities across sectors within a country, within institutional portfolios across regions, and acts as a global resource for development practitioners.</span></p><p><br></p><p><span>For common client countries, these profiles are intended to serve as public goods to facilitate upstream country diagnostics, policy dialogue, and strategic planning by providing comprehensive overviews of trends and projected changes in key climate parameters, sector-specific implications, relevant policies and programs, adaptation priorities and opportunities for further actions.</span></p><p><br></p><p><span>We hope that this combined effort from our institutions will spur deepening of long-term risk management in our client countries and support further cooperation at the operational level.</span></p><p><br></p><div class="container"> <div class="row"> <div style="width:47%"> <p><img src="${environment.apiUploadFileUrl}/Bernice.png" width="72" height="86" class="e-rte-image e-imginline e-resize e-img-focus" alt="Bernice.png" width="auto" style=""><br class="Apple-interchange-newline"><strong>Bernice Van Bronkhorst</strong><br></p> <p>Global Director</p> <p>Climate Change Group</p> <p>The World Bank Group (WBG)</p> <p><br></p> <p> </p> </div> <div style="width:53%"> <p><img src="${environment.apiUploadFileUrl}/Preety.png" width="72" height="86" class="e-rte-image e-imginline e-resize e-img-focus" alt="Preety.png" width="auto" style=""><br class="Apple-interchange-newline"><strong>Preety Bhandari</strong><br></p> <p>Chief of Climate Change and Disaster Risk Management Thematic Group concurrently Director Climate Change and Sustainable Development & Climate Change Department</p> <p>Asian Development Bank (ADB)</p> <p><br></p> <p> </p> </div> </div></div>`,
          forewordFooterLinks: ``,
      });
  }

}

  
fatchAPIkeyMessageSection() {
    this.form.patchValue({
        keyMessageMainSection: '',
        keyMessageFooterLinks: '',
    });
}

async fatchAPIcountryOverviewSection() {
  this.LifeExpectancyAtBirth = await this.httpClient.get(`https://api.worldbank.org/v2/country/${this.countryCode}/indicator/SP.DYN.LE00.IN?format=json&date=2018`).toPromise();
  if(this.LifeExpectancyAtBirth){
      //this.LifeExpectancyAtBirthData = this.LifeExpectancyAtBirth && this.LifeExpectancyAtBirth[1] && Math.round(this.LifeExpectancyAtBirth[1][0].value * 10) / 10;
      this.LifeExpectancyAtBirthNan = this.LifeExpectancyAtBirth && this.LifeExpectancyAtBirth[1] && Number.parseFloat(this.LifeExpectancyAtBirth[1][0].value).toFixed(1);
      this.LifeExpectancyAtBirthData = parseFloat(this.LifeExpectancyAtBirthNan) || 0;
     
  }
  else {
      this.LifeExpectancyAtBirthData = 0;
  }

  this.adultLiteracyRate = await this.httpClient.get(`https://api.worldbank.org/v2/country/${this.countryCode}/indicator/EN.POP.DNST?format=json&date=2018`).toPromise();
  if(this.adultLiteracyRate){
      //this.adultLiteracyRateData = this.adultLiteracyRate && this.adultLiteracyRate[1] && Math.round(this.adultLiteracyRate[1][0].value * 10) / 10;
      this.adultLiteracyRateNan = this.adultLiteracyRate && this.adultLiteracyRate[1] && Number.parseFloat(this.adultLiteracyRate[1][0].value).toFixed(1);
      this.adultLiteracyRateData = parseFloat(this.adultLiteracyRateNan) || 0;
  }
  else {
      this.adultLiteracyRateData = 0;
  }

  this.PopulationDensity = await this.httpClient.get(`https://api.worldbank.org/v2/country/${this.countryCode}/indicator/EG.ELC.ACCS.ZS?format=json&date=2018`).toPromise();
  if(this.PopulationDensity){
      //this.PopulationDensityData = this.PopulationDensity && this.PopulationDensity[1] && Math.round(this.PopulationDensity[1][0].value * 10) / 10;
      this.PopulationDensityNan = this.PopulationDensity && this.PopulationDensity[1] && Number.parseFloat(this.PopulationDensity[1][0].value).toFixed(1);
      this.PopulationDensityData = parseFloat(this.PopulationDensityNan) || 0;;
      
     
  }
  else {
      this.PopulationDensityData = 0;
  }

  this.PercentOfPopulationwithAccessToElectricity = await this.httpClient.get(`https://api.worldbank.org/v2/country/${this.countryCode}/indicator/NY.GDP.PCAP.CD?format=json&date=2018`).toPromise();
  if(this.PercentOfPopulationwithAccessToElectricity){
      this.PercentOfPopulationwithAccessToElectricityNan = this.PercentOfPopulationwithAccessToElectricity && this.PercentOfPopulationwithAccessToElectricity[1] && Number.parseFloat(this.PercentOfPopulationwithAccessToElectricity[1][0].value).toFixed(1);
      this.PercentOfPopulationwithAccessToElectricityData = parseFloat(this.PercentOfPopulationwithAccessToElectricityNan) || 0;;

  }
  else {
      this.PercentOfPopulationwithAccessToElectricityData = 0;
  }

  this.GdpPerCapita = await this.httpClient.get(`https://api.worldbank.org/v2/country/${this.countryCode}/indicator/SE.ADT.LITR.ZS?format=json&date=2018`).toPromise();
  if(this.GdpPerCapita){
      this.GdpPerCapitaNan = (this.GdpPerCapita && this.GdpPerCapita[1] && Number.parseFloat(this.GdpPerCapita[1][0].value).toFixed(1));
      this.GdpPerCapitaData = parseFloat(this.GdpPerCapitaNan) || 0;;
  }
  else {
      this.GdpPerCapitaData = 0;
  }

  if (this.generalProfile === true && this.adbProfile === false) {
      this.fetchGENImageDataArr = this.fetchGENImageDataArr.sort((a, b) => (a.indexNo > b.indexNo) ? 1 : (a.indexNo === b.indexNo) ? ((a.size > b.size) ? 1 : -1) : -1 );
      let z1 = 1;
      for (let i1 = 0; i1 < this.fetchGENImageDataArr.length; i1++) {
          
          if(this.fetchGENImageDataArr[i1].indexNo === 5 || this.fetchGENImageDataArr[i1].indexNo === 8 || this.fetchGENImageDataArr[i1].indexNo === 9 || this.fetchGENImageDataArr[i1].indexNo === 10) {
              this.fetchGENImageDataArr[i1].figure = `Figure ${z1}`;
          } else {
              this.fetchGENImageDataArr[i1].figure = `Figure ${z1}`;
              z1++;
          }
      }
      
      let countryOverviewDataGENImage1 = this.fetchGENImageDataArr.filter(item => item.indexNo == 1);
      if(countryOverviewDataGENImage1.length > 0){
          console.log('countryOverviewDataGENImage1',countryOverviewDataGENImage1);
          this.fig1GENfigure = countryOverviewDataGENImage1[0].figure;
          this.fig1GENUrl = countryOverviewDataGENImage1[0].url;
          this.fig1GENTitle = countryOverviewDataGENImage1[0].title;
          this.fig1GENHtml = `<div class="imgWrap figureCountryProfile"><p><strong>${this.fig1GENfigure}. </strong>${this.fig1GENTitle}</p><img src=${this.fig1GENUrl} class="e-rte-image e-imginline" alt="figure" width="365"/></div><br>`;
      } else {
          this.fig1GENHtml = '';
      }
    
      let countryOverviewDataGENImage2 = this.fetchGENImageDataArr.filter(item => item.indexNo == 2);
      if(countryOverviewDataGENImage2.length > 0) {
          console.log('countryOverviewDataGENImage2',countryOverviewDataGENImage2);
          this.fig2GENfigure = countryOverviewDataGENImage2[0].figure;
          this.fig2GENUrl = countryOverviewDataGENImage2[0].url;
          this.fig2GENTitle = countryOverviewDataGENImage2[0].title;
          this.fig2GENHtml = `<div class="imgWrap figureCountryProfile"> <p><strong>${this.fig2GENfigure}. </strong>${this.fig2GENTitle}</p><img src=${this.fig2GENUrl} class="e-rte-image e-imginline" alt="No Figure" width="582"/></div>`;
      } else {
          this.fig2GENHtml = '';
      }
      this.form.patchValue({
          countryOverviewMainSection: `<br>${this.fig1GENHtml}
          <p><strong>Table 1. </strong>Data Snapshot: Key Development Indicators</p> <br><table class="e-rte-table" style="width: 100%; min-width: 0px;"><thead><tr><th style="background-color: rgb(0, 170, 231); width: 67.4167%;" class=""><p><span style="background-color: transparent;"><span style="color: rgb(242, 242, 242); text-decoration: inherit;">Indicator</span></span></p></th><th style="background-color: rgb(0, 170, 231); width: 32.4932%;" class=""><p><span style="color: rgb(242, 242, 242); text-decoration: inherit;">2018</span></p></th></tr></thead><tbody><tr><td style="width: 67.4167%;" class=""><p><span style="font-size: 1em; background-color: unset; text-align: inherit;">Life expectancy at birth, total (years)</span></p></td><td style="width: 32.4932%;" class=""><p>${this.LifeExpectancyAtBirthData}</p></td></tr><tr><td style="width: 67.4167%; vertical-align: top;" class=""><p>Adult literacy rate (+15)</p></td><td style="width: 32.4932%;" class=""><p>${this.adultLiteracyRateData}</p></td></tr><tr><td style="width: 67.4167%;" class=""><p>Population density (people per sq. km land area)</p></td><td style="width: 32.4932%;" class=""><p>${this.PopulationDensityData}</p></td></tr><tr><td style="width: 67.4167%;" class=""><p>% of Population with access to electricity</p></td><td style="width: 32.4932%;" class=""><p>${this.PercentOfPopulationwithAccessToElectricityData}%</p></td></tr><tr><td style="width: 67.4167%;" class=""><p>GDP per capita (current US$)</p></td><td style="width: 32.4932%;" class=""><p>$${this.GdpPerCapitaData}</p></td></tr></tbody></table><p><br></p><p>The ND-GAIN Index<sup>2</sup> ranks 181 countries using a score which calculates a country's vulnerability to climate change and other global challenges as well as their readiness to improve resilience. This index aims to help businesses and the public sector better identify vulnerability and readiness in order to better prioritize investment for more efficient responses to global challenges. Due to a combination of political, geographic, and social factors, ${this.countryName} is recognized as vulnerable to climate change impacts, ranked XXX out of 181 countries in the 2018 ND-GAIN Index. The more vulnerable a country is the lower their score, while the more ready a country is to improve its resilience the higher it will be. Norway has the highest score and is ranked 1<sup>st</sup>.<strong> Figure 1</strong> is a time-series plot of the ND-GAIN Index showing ${this.countryName}'s progress</p><br>${this.fig2GENHtml}`,
          countryOverviewFooterLinks: `<p><span><sup>1</sup></span><span>World Bank (2020). Internal Climate Migration Profile – <span>${this.countryName}</span>.</span></p><p><span><sup>2</sup></span><span>University of Notre Dame (2020). Notre Dame Global Adaptation Initiative. URL: </span><span><a href="https://gain.nd.edu/our-work/country-index/"><span>https://gain.nd.edu/our-work/country-index/</span></a></span></p>`,
      });
   }
  if (this.generalProfile === false && this.adbProfile === true) {
      console.log('asas',this.fetchABDImageDataArr);
      this.fetchABDImageDataArr = this.fetchABDImageDataArr.sort((a, b) => (a.indexNo > b.indexNo) ? 1 : (a.indexNo === b.indexNo) ? ((a.size > b.size) ? 1 : -1) : -1 );
      let ADBz1 = 1;
      for (let ADBi1 = 0; ADBi1 < this.fetchABDImageDataArr.length; ADBi1++) {
          
          if(this.fetchABDImageDataArr[ADBi1].indexNo === 4 || this.fetchABDImageDataArr[ADBi1].indexNo === 7 || this.fetchABDImageDataArr[ADBi1].indexNo === 8 || this.fetchABDImageDataArr[ADBi1].indexNo === 9) {
              this.fetchABDImageDataArr[ADBi1].figure = `Figure ${ADBz1}`;
          } else {
              this.fetchABDImageDataArr[ADBi1].figure = `Figure ${ADBz1}`;
              ADBz1++;
          }
      }


      let countryOverviewDataABDImage1 = this.fetchABDImageDataArr.filter(item => item.indexNo == 1);

      if (countryOverviewDataABDImage1.length > 0) {
          console.log('countryOverviewDataABDImage1',countryOverviewDataABDImage1);
          this.fig1Adbfigure = countryOverviewDataABDImage1[0].figure;
          this.fig1AdbUrl = countryOverviewDataABDImage1[0].url;
          this.fig1Adbtitle = countryOverviewDataABDImage1[0].title;
          this.fig1AdbHtml = `<div class="imgWrap figureCountryProfile adb-fig1AdbUrl"> <p><b><span>${this.fig1Adbfigure}. </span></b><span>${this.fig1Adbtitle}</span></p><img src=${this.fig1AdbUrl} alt="" width="582" class="e-rte-image e-imginline"/>
          </div>`;
      } else {
          this.fig1AdbHtml = '';
      }

                      
      this.form.patchValue({
          countryOverviewMainSection: `<p>Due to a combination of political, geographic, and social factors, <span>${this.countryName}</span> is recognized as vulnerable to climate change impacts, ranked 57<sup>th</sup> out of 181 countries in the 2018 ND-GAIN Index.<sup>1</sup> The ND-GAIN Index ranks 181 countries using a score which calculates a country's vulnerability to climate change and other global challenges as well as their readiness to improve resilience. The more vulnerable a country is the lower their score, while the more ready a country is to improve its resilience the higher it will be. Norway has the highest score and is ranked 1<sup>st</sup>. Figure 1 is a time-series plot of the ND-GAIN Index showing <span>${this.countryName}'s</span> progress </p> <br>
          ${this.fig1AdbHtml}
          `,
          countryOverviewFooterLinks: '<p><sup>1</sup>​<span>University of Notre Dame (2019). Notre Dame Global Adaptation Initiative. URL: </span><span><a href="https://gain.nd.edu/our-work/country-index/"><span><span></span></span></a><a href="https://gain.nd.edu/our-work/country-index/"><span>https://gain.nd.edu/our-work/country-index/</span></a></span> <sup></sup></p>',
      });
  }
}

async fatchAPIclimatologySection() {
 this.loading = true;
  if (this.generalProfile === true && this.adbProfile === false) {
      this.fetchGENImageDataArr = this.fetchGENImageDataArr.sort((a, b) => (a.indexNo > b.indexNo) ? 1 : (a.indexNo === b.indexNo) ? ((a.size > b.size) ? 1 : -1) : -1 );
      let z1 = 1;
      for (let i1 = 0; i1 < this.fetchGENImageDataArr.length; i1++) {
          
          if(this.fetchGENImageDataArr[i1].indexNo === 5 || this.fetchGENImageDataArr[i1].indexNo === 8 || this.fetchGENImageDataArr[i1].indexNo === 9 || this.fetchGENImageDataArr[i1].indexNo === 10) {
              this.fetchGENImageDataArr[i1].figure = `Figure ${z1}`;
          } else {
              this.fetchGENImageDataArr[i1].figure = `Figure ${z1}`;
              z1++;
          }
      }
      let countryOverviewDataGENImage3 = this.fetchGENImageDataArr.filter(item => item.indexNo == 3);
      if(countryOverviewDataGENImage3.length > 0) {
          console.log('countryOverviewDataGENImage3',countryOverviewDataGENImage3);
          this.fig3GENfigure = countryOverviewDataGENImage3[0].figure;
          this.fig3GENUrl = countryOverviewDataGENImage3[0].url;
          this.fig3GENTitle = countryOverviewDataGENImage3[0].title;
          this.fig3GENHtml = `<div class="imgWrap figureCountryProfile figure-3" style="padding-bottom:5px;"> <p><b><span>${this.fig3GENfigure}. </span></b><span>${this.fig3GENTitle}</span><sup>3</sup></p><img src=${this.fig3GENUrl} class="e-rte-image e-imginline e-resize" alt="No Figure" width="582"></div><br>`;
      } else {
          this.fig3GENHtml = '';
      }


      let countryOverviewDataGENImage4 = this.fetchGENImageDataArr.filter(item => item.indexNo == 4);
      if (countryOverviewDataGENImage4.length > 0) {
          console.log('countryOverviewDataGENImage4',countryOverviewDataGENImage4);
          this.fig4aGENfigure = countryOverviewDataGENImage4[0].figure;
          this.fig4aGENUrl = countryOverviewDataGENImage4[0].url;
          this.fig4aGENTitle = countryOverviewDataGENImage4[0].title;
          this.fig4aGENHtml = `<img src=${this.fig4aGENUrl} alt="Fig4a.png" width="48%" class="e-rte-image e-imginline">&nbsp;&nbsp;`;
      } else {
          this.fig4aGENHtml = '';
      }

      let countryOverviewDataGENImage5 = this.fetchGENImageDataArr.filter(item => item.indexNo == 5);
      if (countryOverviewDataGENImage5.length > 0) {
          console.log('countryOverviewDataGENImage5',countryOverviewDataGENImage5);
          this.fig4bGENfigure = countryOverviewDataGENImage5[0].figure;
          this.fig4bGENUrl = countryOverviewDataGENImage5[0].url;
          this.fig4bGENTitle = countryOverviewDataGENImage5[0].title;
          this.fig4bGENHtml = `<img src=${this.fig4bGENUrl} class="e-rte-image e-imginline e-resize" alt="Fig4b.png" width="48%">`;
      } else {
          this.fig4bGENHtml = '';
      }

      let countryOverviewDataGENImage6 = this.fetchGENImageDataArr.filter(item => item.indexNo == 6);
      if (countryOverviewDataGENImage6.length > 0) {
          console.log('countryOverviewDataGENImage6',countryOverviewDataGENImage6);
          this.fig5GENfigure = countryOverviewDataGENImage6[0].figure;
          this.fig5GENUrl = countryOverviewDataGENImage6[0].url;
          this.fig5GENTitle = countryOverviewDataGENImage6[0].title;
          this.fig5GENHtml = ``;
          } else {
              this.fig5GENHtml = '';
          }

      let countryOverviewDataGENImage7 = this.fetchGENImageDataArr.filter(item => item.indexNo == 7);
      if (countryOverviewDataGENImage7.length > 0) {
          console.log('countryOverviewDataGENImage7',countryOverviewDataGENImage7);
          this.fig6aGENfigure = countryOverviewDataGENImage7[0].figure;
          this.fig6aGENUrl = countryOverviewDataGENImage7[0].url;
          this.fig6aGENTitle = countryOverviewDataGENImage7[0].title;
          this.fig6aGENHtml = `<img src=${this.fig6aGENUrl} alt="Fig6a.png" width="48%" class="e-rte-image e-imginline e-resize e-img-focus"/>`;
      } else {
          this.fig6aGENHtml = '';
      }
      
      let countryOverviewDataGENImage8 = this.fetchGENImageDataArr.filter(item => item.indexNo == 8);
      if (countryOverviewDataGENImage8.length > 0) {
          console.log('countryOverviewDataGENImage8',countryOverviewDataGENImage8);
          this.fig6bGENfigure = countryOverviewDataGENImage8[0].figure;
          this.fig6bGENUrl = countryOverviewDataGENImage8[0].url;
          this.fig6bGENTitle = countryOverviewDataGENImage8[0].title;
          this.fig6bGENHtml = `<img src=${this.fig6bGENUrl} alt="Fig6b.png" width="48%" class="e-rte-image e-imginline e-resize e-img-focus"/>`;
      } else {
          this.fig6bGENHtml = '';
      }

      let countryOverviewDataGENImage9 = this.fetchGENImageDataArr.filter(item => item.indexNo == 9);
      if (countryOverviewDataGENImage9.length > 0) {
          console.log('countryOverviewDataGENImage9',countryOverviewDataGENImage9);
          this.fig6cGENfigure = countryOverviewDataGENImage9[0].figure;
          this.fig6cGENUrl = countryOverviewDataGENImage9[0].url;
          this.fig6cGENTitle = countryOverviewDataGENImage9[0].title;
          this.fig6cGENHtml = `<img src=${this.fig6cGENUrl} alt="Fig6c.png" width="48%" class="e-rte-image e-imginline"/>`;
      } else {
          this.fig6cGENHtml = '';
      }

      let countryOverviewDataGENImage10 = this.fetchGENImageDataArr.filter(item => item.indexNo == 10);
      if (countryOverviewDataGENImage10.length > 0) {
          console.log('countryOverviewDataGENImage10',countryOverviewDataGENImage10);
          this.fig6dGENfigure = countryOverviewDataGENImage10[0].figure;
          this.fig6dGENUrl = countryOverviewDataGENImage10[0].url;
          this.fig6dGENTitle = countryOverviewDataGENImage10[0].title;
          this.fig6dGENHtml = `<img src=${this.fig6dGENUrl} alt="Fig6d.png" width="48%" class="e-rte-image e-imginline"/>`;
      } else {
          this.fig6dGENHtml = '';
      }

      let countryOverviewDataGENImage11 = this.fetchGENImageDataArr.filter(item => item.indexNo == 11);
      if (countryOverviewDataGENImage11.length > 0) {
          console.log('countryOverviewDataGENImage11',countryOverviewDataGENImage11);
          this.fig7GENfigure = countryOverviewDataGENImage11[0].figure;
          this.fig7GENUrl = countryOverviewDataGENImage11[0].url;
          this.fig7GENTitle = countryOverviewDataGENImage11[0].title;
          this.fig7GENHtml = `<span style="width: 48%;">
          <p style="min-height: 60px;"><b><span>${this.fig7GENfigure}.</span></b> Historical and Projected Average Temperature for <span>${this.countryName}</span> from 1986 to 2099 <sup>7</sup></span></p> 
          <img src=${this.fig7GENUrl} class="e-rte-image e-imginline e-resize" alt="" width="100%" >
      </span>`;
      } else {
          this.fig7GENHtml = '';
      }

      let countryOverviewDataGENImage12 = this.fetchGENImageDataArr.filter(item => item.indexNo == 12);
      if (countryOverviewDataGENImage12.length > 0) {
          console.log('countryOverviewDataGENImage12',countryOverviewDataGENImage12);
          this.fig8GENfigure = countryOverviewDataGENImage12[0].figure;
          this.fig8GENUrl = countryOverviewDataGENImage12[0].url;
          this.fig8GENTitle = countryOverviewDataGENImage12[0].title;
          this.fig8GENHtml = `<span style="width: 48%;">
          <p style="min-height: 60px;"><b><span>${this.fig8GENfigure}.</span></b> Projected Change in Summer Days (Tmax&gt; 25°C)<span><sup>8</sup></span></p><img src=${this.fig8GENUrl} class="e-rte-image e-imginline" alt="" width="100%">
      </span>`;
      } else {
          this.fig8GENHtml = '';
      }

      let countryOverviewDataGENImage13 = this.fetchGENImageDataArr.filter(item => item.indexNo == 13);
      if (countryOverviewDataGENImage13.length > 0) {
          console.log('countryOverviewDataGENImage13',countryOverviewDataGENImage13);
          this.fig9GENfigure = countryOverviewDataGENImage13[0].figure;
          this.fig9GENUrl = countryOverviewDataGENImage13[0].url;
          this.fig9GENTitle = countryOverviewDataGENImage13[0].title;
          this.fig9GENHtml = `<br><div class="imgWrap"> <p><b><span>${this.fig9GENfigure}. </span></b>Annual Average Precipitation in <span>${this.countryName}</span> for 1986 to 2099<span><sup>9</sup></span></p> <img src=${this.fig9GENUrl} class="e-rte-image e-imginline" alt="" width="48%"></div>`;
      } else {
          this.fig9GENHtml = '';
      }

  }
  if (this.generalProfile === false && this.adbProfile === true) {

      this.fetchABDImageDataArr = this.fetchABDImageDataArr.sort((a, b) => (a.indexNo > b.indexNo) ? 1 : (a.indexNo === b.indexNo) ? ((a.size > b.size) ? 1 : -1) : -1 );
      let ADBz1 = 1;
      for (let ADBi1 = 0; ADBi1 < this.fetchABDImageDataArr.length; ADBi1++) {
          
          if(this.fetchABDImageDataArr[ADBi1].indexNo === 4 || this.fetchABDImageDataArr[ADBi1].indexNo === 7 || this.fetchABDImageDataArr[ADBi1].indexNo === 8 || this.fetchABDImageDataArr[ADBi1].indexNo === 9) {
              this.fetchABDImageDataArr[ADBi1].figure = `Figure ${ADBz1}`;
          } else {
              this.fetchABDImageDataArr[ADBi1].figure = `Figure ${ADBz1}`;
              ADBz1++;
          }
      }
      let climatologyDataABDImage2 = await this.fetchABDImageDataArr.filter(item2 => item2.indexNo == 2);
      if (climatologyDataABDImage2.length > 0) {
          this.fig2Adbfigure = climatologyDataABDImage2[0].figure;
          this.fig2AdbUrl = climatologyDataABDImage2[0].url;
          this.fig2Adbtitle = climatologyDataABDImage2[0].title;
          this.fig2AdbHtml = `<div class="imgWrap figureCountryProfile"> <p><b><span>${this.fig2Adbfigure}. </span></b><span>${this.fig2Adbtitle} <span></span> <sup>2</sup></span></p><img src=${this.fig2AdbUrl} class="e-rte-image e-imginline e-resize" alt="Fig2.svg" width="582" style="min-width: 0px; max-width: 613px; min-height: 0px;"></div>`;
      } else {
          this.fig2AdbHtml = '';
      }

      let climatologyDataABDImage3a = this.fetchABDImageDataArr.filter(item => item.indexNo == 3);
      if (climatologyDataABDImage3a.length > 0) {
          this.fig3aAdbfigure = climatologyDataABDImage3a[0].figure;
          this.fig3aAdbUrl = climatologyDataABDImage3a[0].url;
          this.fig3aAdbtitle = climatologyDataABDImage3a[0].title;
          this.fig3aAdbHtml = `<img src=${this.fig3aAdbUrl} alt="Fig3a.png" width="48%" class="e-rte-image e-imginline"/>`;
      } else {
          this.fig3aAdbHtml = '';
      }

      let climatologyDataABDImage3b = this.fetchABDImageDataArr.filter(item => item.indexNo == 4);
      if (climatologyDataABDImage3b.length > 0) {
          this.fig3bAdbfigure = climatologyDataABDImage3b[0].figure;
          this.fig3bAdbUrl = climatologyDataABDImage3b[0].url;
          this.fig3bAdbtitle = climatologyDataABDImage3b[0].title;
          this.fig3bAdbHtml = `<img src=${this.fig3bAdbUrl} class="e-rte-image e-imginline e-resize" alt="Fig3b.png" width="48%"/>`;
      } else {
          this.fig3bAdbHtml = '';
      }

      let climatologyDataABDImage4 = this.fetchABDImageDataArr.filter(item => item.indexNo == 5);
      if (climatologyDataABDImage4.length > 0) {
          this.fig4Adbfigure = climatologyDataABDImage4[0].figure;
          this.fig4AdbUrl = climatologyDataABDImage4[0].url;
          this.fig4Adbtitle = climatologyDataABDImage4[0].title;
          this.fig4AdbHtml = `<img src=${this.fig4AdbUrl} class="e-rte-image e-imginline e-resize" alt="Fig4.png" width="582"/>`;
      } else {
          this.fig4AdbHtml = '';
      }

      let climatologyDataABDImage5a = this.fetchABDImageDataArr.filter(item => item.indexNo == 6);
      if (climatologyDataABDImage5a.length > 0) {
          this.fig5aAdbfigure = climatologyDataABDImage5a[0].figure;
          this.fig5aAdbUrl = climatologyDataABDImage5a[0].url;
          this.fig5aAdbtitle = climatologyDataABDImage5a[0].title;
          this.fig5aAdbHtml = `<img src=${this.fig5aAdbUrl} alt="Fig5a.png" width="48%" class="e-rte-image e-imginline e-resize"/>`;
      } else {
          this.fig5aAdbHtml = '';
      }

      let climatologyDataABDImage5b = this.fetchABDImageDataArr.filter(item => item.indexNo == 7);
      if (climatologyDataABDImage5b.length > 0) {
          this.fig5bAdbfigure = climatologyDataABDImage5b[0].figure;
          this.fig5bAdbUrl = climatologyDataABDImage5b[0].url;
          this.fig5bAdbtitle = climatologyDataABDImage5b[0].title;
          this.fig5bAdbHtml = `<img src=${this.fig5bAdbUrl} alt="Fig5b.png" width="48%" class="e-rte-image e-imginline e-resize"/>`;
      } else {
          this.fig5bAdbHtml = '';
      }

      let climatologyDataABDImage5c = this.fetchABDImageDataArr.filter(item => item.indexNo == 8);
      if (climatologyDataABDImage5c.length > 0) {
          this.fig5cAdbfigure = climatologyDataABDImage5c[0].figure;
          this.fig5cAdbUrl = climatologyDataABDImage5c[0].url;
          this.fig5cAdbtitle = climatologyDataABDImage5c[0].title;
          this.fig5cAdbHtml = `<img src=${this.fig5cAdbUrl} class="e-rte-image e-imginline e-resize" alt="Fig5c.png" width="48%"/>`;
      } else {
          this.fig5cAdbHtml = '';
      }

      let climatologyDataABDImage5d = this.fetchABDImageDataArr.filter(item => item.indexNo == 9);
      if (climatologyDataABDImage5d.length > 0) {
          this.fig5dAdbfigure = climatologyDataABDImage5d[0].figure;
          this.fig5dAdbUrl = climatologyDataABDImage5d[0].url;
          this.fig5dAdbtitle = climatologyDataABDImage5d[0].title;
          this.fig5dAdbHtml = `<img src=${this.fig5dAdbUrl} class="e-rte-image e-imginline e-resize e-img-focus" alt="Fig5d.png" width="48%"/>`;
      } else {
          this.fig5dAdbHtml = '';
      }

      let climatologyDataABDImage6 = this.fetchABDImageDataArr.filter(item => item.indexNo == 10);
      if (climatologyDataABDImage6.length > 0) {
          this.fig6Adbfigure = climatologyDataABDImage6[0].figure;
          this.fig6AdbUrl = climatologyDataABDImage6[0].url;
          this.fig6Adbtitle = climatologyDataABDImage6[0].title;
          this.fig6AdbHtml = `<img src=${this.fig6AdbUrl} class="e-rte-image e-imginline e-resize" alt="" width="100%"/>`;
      } else {
          this.fig6AdbHtml = '';
      }

      let climatologyDataABDImage7 = this.fetchABDImageDataArr.filter(item => item.indexNo == 11);
      if (climatologyDataABDImage7.length > 0) {
          this.fig7Adbfigure = climatologyDataABDImage7[0].figure;
          this.fig7AdbUrl = climatologyDataABDImage7[0].url;
          this.fig7Adbtitle = climatologyDataABDImage7[0].title;
          this.fig7AdbHtml = `<img src=${this.fig7AdbUrl} class="e-rte-image e-imginline" alt="" width="100%"/>`;
      } else {
          this.fig7AdbHtml = '';
      }
  }

  this.climatologyFormGroup.clear();

  this.gettasData = await this.httpClient.get(environment.T2tasURl + this.countryCode).toPromise();
  if(this.gettasData){
      var testing = this.gettasData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12;
      this.tasData = Math.round(avg * 10) / 10;
  }
  else {
      this.tasData = 0;
  }

  this.gett2PrData = await this.httpClient.get(environment.T2prURl + this.countryCode).toPromise();
  if(this.gett2PrData){
      var testing = this.gett2PrData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      this.t2PrData = Math.round(sum * 10) / 10; // precipitation is direct sum
  }
  else {
      this.t2PrData = 0;
  }
  
  this.getT2tasmaxData = await this.httpClient.get(environment.T2tasmaxURl + this.countryCode).toPromise();
  if(this.getT2tasmaxData){
      var testing = this.getT2tasmaxData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12;
      this.T2tasmaxData = Math.round(avg * 10) / 10;
  }
  else {
      this.T2tasmaxData = 0;
  }
  
  this.getT2tasminData = await this.httpClient.get(environment.T2tasminURl + this.countryCode).toPromise();
  if(this.getT2tasminData){
      var testing = this.getT2tasminData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12;
      this.T2tasminData = Math.round(avg * 10) / 10;
  }
  else {
      this.T2tasminData = 0;
  }
  
  this.getT3tasP101stData = await this.httpClient.get(environment.T3tasP101stUrl + this.countryCode).toPromise();
  if(this.getT3tasP101stData){
      var testing = this.getT3tasP101stData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12;
      this.T3tasP101stData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3tasP101stData = 0;
  }
  
  this.getT3tasP102ndData = await this.httpClient.get(environment.T3tasP102ndUrl + this.countryCode).toPromise();
  if(this.getT3tasP102ndData){
      var testing = this.getT3tasP102ndData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12;
      this.T3tasP102ndData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3tasP102ndData = 0;
  }
  
  this.getT3tasP103rdData = await this.httpClient.get(environment.T3tasP103rdUrl + this.countryCode).toPromise();
  if(this.getT3tasP103rdData){
      var testing = this.getT3tasP103rdData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12;
      this.T3tasP103rdData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3tasP103rdData = 0;
  }

  this.getT3tasP104thData = await this.httpClient.get(environment.T3tasP104thUrl + this.countryCode).toPromise();
  if(this.getT3tasP104thData){
      var testing = this.getT3tasP104thData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12;
      this.T3tasP104thData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3tasP104thData = 0;
  }

  this.getT3tasMedian1stData = await this.httpClient.get(environment.T3tasMedian1stUrl + this.countryCode).toPromise();
  if(this.getT3tasMedian1stData){
      var testing = this.getT3tasMedian1stData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12;
      this.T3tasMedian1stData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3tasMedian1stData = 0;
  }

  this.getT3tasMedian2ndData = await this.httpClient.get(environment.T3tasMedian2ndUrl + this.countryCode).toPromise();
  if(this.getT3tasMedian2ndData){
      var testing = this.getT3tasMedian2ndData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12;
      this.T3tasMedian2ndData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3tasMedian2ndData = 0;
  }

  this.getT3tasMedian3rdData = await this.httpClient.get(environment.T3tasMedian3rdUrl + this.countryCode).toPromise();
  if(this.getT3tasMedian3rdData){
      var testing = this.getT3tasMedian3rdData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12;
      this.T3tasMedian3rdData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3tasMedian3rdData = 0;
  }
  
  this.getT3tasMedian4thData = await this.httpClient.get(environment.T3tasMedian4thUrl + this.countryCode).toPromise();
  if(this.getT3tasMedian4thData){
      var testing = this.getT3tasMedian4thData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12;
      this.T3tasMedian4thData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3tasMedian4thData = 0;
  }
  
  this.getT3tasP901stData = await this.httpClient.get(environment.T3tasP901stUrl + this.countryCode).toPromise();
  if(this.getT3tasP901stData){
      var testing = this.getT3tasP901stData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12;
      this.T3tasP901stData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3tasP901stData = 0;
  }
  
  this.getT3tasP902ndData = await this.httpClient.get(environment.T3tasP902ndUrl + this.countryCode).toPromise();
  if(this.getT3tasP902ndData){
      var testing = this.getT3tasP902ndData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12;
      this.T3tasP902ndData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3tasP902ndData = 0;
  }
  
  this.getT3tasP903rdData = await this.httpClient.get(environment.T3tasP903rdUrl + this.countryCode).toPromise();
  if(this.getT3tasP903rdData){
      var testing = this.getT3tasP903rdData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12;
      this.T3tasP903rdData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3tasP903rdData = 0;
  }
  
  this.getT3tasP904thData = await this.httpClient.get(environment.T3tasP904thUrl + this.countryCode).toPromise();
  if(this.getT3tasP904thData){
      var testing = this.getT3tasP904thData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12;
      this.T3tasP904thData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3tasP904thData = 0;
  }
  
  this.getT3prP101stData = await this.httpClient.get(environment.T3prP101stUrl + this.countryCode).toPromise();
  if(this.getT3prP101stData){
      var testing = this.getT3prP101stData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12;
      this.T3prP101stData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3prP101stData = 0;
  }
  
  this.getT3prP102ndData = await this.httpClient.get(environment.T3prP102ndUrl + this.countryCode).toPromise();
  if(this.getT3prP102ndData){
      var testing = this.getT3prP102ndData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12;
      this.T3prP102ndData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3prP102ndData = 0;
  }
  
  this.getT3prP103rdData = await this.httpClient.get(environment.T3prP103rdUrl + this.countryCode).toPromise();
  if(this.getT3prP103rdData){
      var testing = this.getT3prP103rdData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12;
      this.T3prP103rdData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3prP103rdData = 0;
  }
  
  this.getT3prP104thData = await this.httpClient.get(environment.T3prP104thUrl + this.countryCode).toPromise();
  if(this.getT3prP104thData){
      var testing = this.getT3prP104thData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12 ;
      this.T3prP104thData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3prP104thData = 0;
  }
  this.getT3prMedian1stData = await this.httpClient.get(environment.T3prMedian1stUrl + this.countryCode).toPromise();
  if(this.getT3prMedian1stData){
      var testing = this.getT3prMedian1stData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12 ;
      this.T3prMedian1stData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3prMedian1stData = 0;
  }
  
  this.getT3prMedian2ndData = await this.httpClient.get(environment.T3prMedian2ndUrl + this.countryCode).toPromise();
  if(this.getT3prMedian2ndData){
      var testing = this.getT3prMedian2ndData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12 ;
      this.T3prMedian2ndData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3prMedian2ndData = 0;
  }
  
  this.getT3prMedian3rdData = await this.httpClient.get(environment.T3prMedian3rdUrl + this.countryCode).toPromise();
  if(this.getT3prMedian3rdData){
      var testing = this.getT3prMedian3rdData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12 ;
      this.T3prMedian3rdData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3prMedian3rdData = 0;
  }
  
  this.getT3prMedian4thData = await this.httpClient.get(environment.T3prMedian4thUrl + this.countryCode).toPromise();
  if(this.getT3prMedian4thData){
      var testing = this.getT3prMedian4thData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12 ;
      this.T3prMedian4thData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3prMedian4thData = 0;
  }
  
  this.getT3prP901stData = await this.httpClient.get(environment.T3prP901stUrl + this.countryCode).toPromise();
  if(this.getT3prP901stData){
      var testing = this.getT3prP901stData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12 ;
      this.T3prP901stData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3prP901stData = 0;
  }

  this.getT3prP902ndData = await this.httpClient.get(environment.T3prP902ndUrl + this.countryCode).toPromise();
  if(this.getT3prP902ndData){
      var testing = this.getT3prP902ndData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12 ;
      this.T3prP902ndData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3prP902ndData = 0;
  }
  
  this.getT3prP903rdData = await this.httpClient.get(environment.T3prP903rdUrl + this.countryCode).toPromise();
  if(this.getT3prP903rdData){
      var testing = this.getT3prP903rdData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12 ;
      this.T3prP903rdData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3prP903rdData = 0;
  }
  this.getT3prP904thData = await this.httpClient.get(environment.T3prP904thUrl + this.countryCode).toPromise();
  if(this.getT3prP904thData){
      var testing = this.getT3prP904thData['monthVals'];
      var formattedValues = testing.map(i => Number(i));
      var sum = formattedValues.reduce((a, b) => a + b, 0);
      var avg = sum / 12 ;
      this.T3prP904thData = (avg < 0 ? "" : "+") + Math.round(avg * 10) / 10;
  }
  else {
      this.T3prP904thData = 0;
  }
  
  if (this.generalProfile === true && this.adbProfile === false) {
      this.climatologysArray().push(this.fb.group({ "climatologySubSectionsTitle": new FormControl({ value: 'Climate Baseline', disabled: true }), "climatologySubSectionsContent": `<h3>Overview</h3>
      ${this.fig3GENHtml}<p><strong>Table 2</strong>. Data Snapshot: Summary Statistics</p><p><br></p><table class="e-rte-table" style="width: 100%; min-width: 0px;"> <thead> <tr> <th style="background-color: rgb(0, 170, 231); width: 67.4167%;" class=""> <p><span style="background-color: transparent;"><span style="color: rgb(242, 242, 242); text-decoration: inherit;">Climate Variables</span></span> </p> </th> <th style="background-color: rgb(0, 170, 231); width: 32.4932%;" class=""> <p><span style="color: rgb(242, 242, 242); text-decoration: inherit;">1901–2016</span> </p> </th> </tr> </thead> <tbody> <tr> <td style="width: 67.4167%;" class=""> <p><span style="font-size: 1em; background-color: unset; text-align: inherit;">Mean Annual Temperature (°C)</span></p> </td> <td style="width: 32.4932%;" class=""> <p>${this.tasData} °C</p> </td> </tr> <tr style="height: 26px;"> <td style="width: 67.4167%; vertical-align: top;" class=""> <p>Mean Annual Precipitation (mm)</p> </td> <td style="width: 32.4932%;" class=""> <p>${this.t2PrData} mm</p> </td> </tr> <tr> <td style="width: 67.4167%;" class=""> <p>Mean Maximum Annual Temperature (°C)</p> </td> <td style="width: 32.4932%;" class=""> <p>${this.T2tasmaxData} °C</p> </td> </tr> <tr> <td style="width: 67.4167%;" class=""> <p>Mean Minimum Annual Temperature (°C)</p> </td> <td style="width: 32.4932%;" class=""> <p>${this.T2tasminData} °C</p> </td> </tr> </tbody></table><p><br></p>
      
      <div class="imgWrap figureCountryProfile" style="cursor: auto;padding-bottom:5px;"> <p><b><span>${this.fig4aGENfigure}. </span></b>Map of average annual temperature (left); annual precipitation (right) for ${this.countryName}, 1901-2019<sup>4</sup> </p> 
      ${this.fig4aGENHtml}
      ${this.fig4bGENHtml}</div><br>
      
      <h3><span><strong>Key Trends</strong></span></h3><h4 style="font-weight: 400;
      font-style: italic;
      font-family: "BertholdLight", sans-serif !important;">Temperature</h4><br>
      <h4 style="font-weight: 400;
      font-style: italic;
      font-family: "BertholdLight", sans-serif !important;">Precipitation</h4><br>` }));
      this.form.patchValue({
          climatologyMainSection: '',
          climatologyFooterLinks: `<p><span style="letter-spacing: 0.3px; background-color: unset; text-align: inherit;"><sup>3</sup> WBG Climate Change Knowledge Portal (CCKP,2020). ${this.countryName}. URL:</span><a href="https://climateknowledgeportal.worldbank.org/country/"${this.countryName}"/climate-data-historical" style="letter-spacing: 0.3px; text-align: inherit;">https://climateknowledgeportal.worldbank.org/country/${this.countryName}/climate-data-historical</a><br></p><p><span><sup>4</sup>WBG Climate Change Knowledge Portal (CCKP, 2020). <span>${this.countryName}</span>. URL: </span><span><span><a href="https://climateknowledgeportal.worldbank.org/country/"${this.countryName}"/climate-data-historical">https://climateknowledgeportal.worldbank.org/country/${this.countryName}</span>/climate-data-historical</a></span></span></p><p><span><span><sup>5</sup>WBG Climate Change Knowledge Portal (CCKP,2020). <span>${this.countryName}</span>. URL:</span><span> <a href="https://climateknowledgeportal.worldbank.org/country/"${this.countryName}"/climate-data-projections"><span>https://climateknowledgeportal.worldbank.org/country/<span>${this.countryName}</span>/climate-data-projections</span></a></span><span> </span><br></span></p><p><sup>6</sup><span>WBG Climate Change Knowledge Portal (CCKP, 2020).<span> ${this.countryName}</span> Projected Future Climate. URL: </span><span><span><a href="https://climateknowledgeportal.worldbank.org/country/"${this.countryName}"/climate-data-projections">https://climateknowledgeportal.worldbank.org/country/<span>${this.countryName}</span>/climate-data-projections</a></span></span></p><p><span><sup>7</sup> WBG Climate Change Knowledge Portal (CCKP, 2020). Interactive Climate Indicator Dashboard. <span>${this.countryName}</span>. URL </span><a href="https://climatedata.worldbank.org/CRMePortal/web/agriculture/crops-and-land-management?country="${this.countryCode}"&amp;period=2080-2099"><span>https://climatedata.worldbank.org/CRMePortal/web/agriculture/crops-and-land-management?country=<span>${this.countryCode}</span>&amp;period=2080-2099</span></a></p><p><span><sup>8</sup>WBG Climate Change Knowledge Portal (CCKP, 2020). Interactive Climate Indicator Dashboard. <span>${this.countryName}</span>. URL </span><a href="https://climatedata.worldbank.org/CRMePortal/web/agriculture/crops-and-land-management?country="${this.countryCode}"&amp;period=2080-2099"><span>https://climatedata.worldbank.org/CRMePortal/web/agriculture/crops-and-land-management?country=<span>${this.countryCode}</span>&amp;period=2080-2099</span></a></p><p><span><span><span><span><span><sup>9</sup></span></span></span></span></span><span> WBG Climate Change Knowledge Portal (CCKP, 2020). Interactive Climate Indicator Dashboard. <span>${this.countryName}</span>. URL </span><span><a href="https://climatedata.worldbank.org/CRMePortal/web/agriculture/crops-and-land-management?country="${this.countryCode}"&amp;period=2080-2099"><span>https://climatedata.worldbank.org/CRMePortal/web/agriculture/crops-and-land-management?country=<span>${this.countryCode}</span>&amp;period=2080-2099</span></a></span></p>`,
      });
  }
  if (this.generalProfile === false && this.adbProfile === true) { 

      this.climatologysArray().push(this.fb.group({ "climatologySubSectionsTitle": new FormControl({ value: 'Climate Baseline', disabled: true }), "climatologySubSectionsContent": `<h3>Overview</h3><h4><b><span>Annual Cycle</span></b></h4>${this.fig2AdbHtml}
      <br><h4><b><span>Spatial Variation</span></b></h4><div class="imgWrap figureCountryProfile"><p><b><span>${this.fig3aAdbfigure}. </span></b>${this.fig3aAdbtitle}<sup>3</sup></p>
      ${this.fig3aAdbHtml}${this.fig3bAdbHtml}
      </div><br><h3><span><strong>Key Trends</strong></span></h3>
      
      <h4 style="font-weight: 400;
      font-style: italic;
      font-family: "BertholdLight", sans-serif !important;">Temperature</h4>
      <br>
      <h4 style="font-weight: 400;
      font-style: italic;
      font-family: "BertholdLight", sans-serif !important;">Precipitation</h4><p><span><strong>A precautionary approach</strong></span></p><p><span>Studies published since the last iteration of the IPCC’s report (AR5), such as Gasser et al. (2018), have presented evidence which suggests a greater probability that earth will experience medium and high-end warming scenarios than previously estimated.<sup>1</sup>&nbsp;Climate change projections associated with the highest emissions pathway (RCP 8.5) are presented here to facilitate decision making which is robust to these risks. </span></p><p></p>` }));
      this.form.patchValue({
          climatologyMainSection: '',
          climatologyFooterLinks: `<p><span><sup>2</sup></span> WBG Climate Change Knowledge Portal (CCKP, 2020).Climate Data: Historical – <span>${this.countryName}</span>.URL:<a href="https://climateknowledgeportal.worldbank.org/country/"${this.countryName}"/climate-data-historical"><span>https://climateknowledgeportal.worldbank.org/country/${this.countryName}/climate-data-historical</span></a></p><p><span><span><span><span><span><sup>3</sup></span></span></span></span></span><span> WBG Climate Change Knowledge Portal (CCKP, 2020).Climate Data: Historical – <span>${this.countryName}</span>.URL:</span><span><span><a href="https://climateknowledgeportal.worldbank.org/country/"${this.countryName}"/climate-data-historical">https://climateknowledgeportal.worldbank.org/country/${this.countryName}/climate-data-historical</a></span></span></p><p><span><span><span><span><span><span><sup>4</sup></span></span></span></span></span><span>WBG Climate Change Knowledge Portal (CCKP, 2020).Climate Data: Historical – <span>${this.countryName}</span>.URL:</span><span><a href="https://climateknowledgeportal.worldbank.org/country/"${this.countryName}"/climate-data-historical"><span>https://climateknowledgeportal.worldbank.org/country/${this.countryName}/climate-data-historical</span></a></span><span>.</span><br></span></p><p><span><span><sup>5</sup>Flato, G., Marotzke, J., Abiodun, B., Braconnot, P.,Chou, S. C., Collins, W., … Rummukainen, M. (2013). Evaluation of Climate Models. Climate Change 2013: The Physical Science Basis. Contribution of Working Group I to the Fifth Assessment Report of the Intergovernmental Panel on Climate Change, 741–866. URL: </span><span><a href="http://www.climatechange2013.org/images/report/WG1AR5_ALL_FINAL.pdf"><span>http://www.climatechange2013.org/images/report/WG1AR5_ALL_FINAL.pdf</span></a></span><br></span></p><p><span><span><span><span><span><sup>6</sup></span></span></span></span></span><span> WBG Climate Change Knowledge Portal (CCKP 2020).Climate Data. Projections - <span>${this.countryName}</span>.URL: </span><a href="https://climateknowledgeportal.worldbank.org/country/"${this.countryName}"/climate-data-projections"><span>https://climateknowledgeportal.worldbank.org/country/${this.countryName}/climate-data-projections</span></a></p><p><span><span><span><span><span><sup>7</sup></span></span></span></span></span><span> WBG Climate Change Knowledge Portal (CCKP 2020). <span>${this.countryName}</span> Interactive Climate Indicator Dashboard. URL: </span><span><span><a href="https://climatedata.worldbank.org/CRMePortal/web/agriculture/crops-and-land-management?country="${this.countryCode}"&amp;period=2080-2099">https://climatedata.worldbank.org/CRMePortal/web/agriculture/crops-and-land-management?country=${this.countryCode}&amp;period=2080-2099</a></span></span></p><p><span><br></span></p>`,
      });
  }

  if (this.generalProfile === true && this.adbProfile === false) {

      this.climatologysArray().push(this.fb.group({
          "climatologySubSectionsTitle": new FormControl({ value: 'Climate Future', disabled: true }), "climatologySubSectionsContent": `<h3>Overview</h3><p><span>The main data source for the World Bank Group’s </span><a href="https://climateknowledgeportal.worldbank.org/"><span>Climate Change Knowledge Portal</span></a><span> (CCKP) is the CMIP5 (Coupled Model Inter-comparison Project No.5) data ensemble, which builds the database for the global climate change projections presented in the Fifth Assessment Report (AR5) of the Intergovernmental Panel on Climate Change (IPCC). Four Representative Concentration Pathways (i.e. RCP 2.6, RCP 4.5, RCP 6.0, and RCP 8.5) were selected and defined by their total radiative forcing (cumulative measure of GHG emissions from all sources) pathway and level by 2100. The RCP 2.6 for example represents a very strong mitigation scenario, whereas the RCP 8.5 assumes business-as-usual scenario. For more information, please refer to the </span><a href="http://www.iiasa.ac.at/web-apps/tnt/RcpDb/dsd?Action=htmlpage&amp;page=welcome"><span>RCP Database</span></a><span>. For simplification, these scenarios are referred to as a low (RCP 2.6); a medium (RCP 4.5) and a high (RCP 8.5) emission scenario in this profile. <strong>Table 3</strong> provides CMIP5 projections for essential climate variables under high emission scenario (RCP 8.5) over 4 different time horizons. <b>Figure 6</b> presents the multi-model (CMIP5) ensemble of 32 Global Circulation Models (GCMs) showing the projected changes in annual precipitation and temperature for the periods 2040-2059 and 2080-2099.</span></p> <br><br><br><br><br><br><br><br><br><br><br><div><p><b>Table 3.</b> Data Snapshot:  CMIP5 Ensemble Projection</p><br><table class="e-rte-table" style="width: 99.64%; min-width: 0px; height: 129px;"> <thead> <tr> <th style="background-color: rgb(0, 170, 231); width: 20%;" class=""> <p><span style="background-color: transparent;"><span style="color: rgb(242, 242, 242); text-decoration: inherit;">CMIP5 Ensemble Projections</span></span></p> </th> <th style="background-color: rgb(0, 170, 231); width: 20%;" class=""> <p><span style="color: rgb(242, 242, 242); text-decoration: inherit;">2020-2039</span> </p> </th> <th style="background-color: rgb(0, 170, 231); width: 20%;" class=""><span style="color: rgb(242, 242, 242); text-decoration: inherit;">2040-2059</span> </th> <th style="background-color: rgb(0, 170, 231); width: 20%;" class=""><span style="color: rgb(242, 242, 242); text-decoration: inherit;">2060-2079</span> </th> <th style="background-color: rgb(0, 170, 231); width: 20%;" class=""><span style="color: rgb(242, 242, 242); text-decoration: inherit;">2080-2099</span> </th> </tr> </thead> <tbody> <tr> <td style="width: 20%;" class=""> <p><span style="font-size: 1em; background-color: unset; text-align: inherit;">Annual Temperature Anomaly(°C)</span></p> </td> <td style="width: 20%;" class=""><span style="color: rgb(51, 51, 51); "><b>${this.T3tasP101stData} to ${this.T3tasP901stData}</b> <p style="font-size: 1em; background-color: unset; text-align: inherit; margin-left: 19px;"> (${this.T3tasMedian1stData}°C)</p> </span><br></td> <td style="width: 20%;" class=""><span style="color: rgb(51, 51, 51); "><b>${this.T3tasP102ndData} to ${this.T3tasP902ndData}</b> <p style="font-size: 1em; background-color: unset; text-align: inherit; margin-left: 19px;"> (${this.T3tasMedian2ndData}°C)</p> </span><br></td> <td style="width: 20%;" class=""><span style="color: rgb(51, 51, 51); "> <b>${this.T3tasP103rdData} to ${this.T3tasP903rdData}</b> <p style="font-size: 1em; background-color: unset; text-align: inherit; margin-left: 19px;"> (${this.T3tasMedian3rdData}°C)</p> </span><br></td> <td style="width: 20%;" class=""><span style="color: rgb(51, 51, 51); "><b>${this.T3tasP104thData} to ${this.T3tasP904thData}</b> <p style="font-size: 1em; background-color: unset; text-align: inherit; margin-left: 19px;"> (${this.T3tasMedian4thData}°C)</p> </span><br></td> </tr> <tr style="height: 26px;"> <td style="width: 20%; vertical-align: top;" class=""> <p>Annual Precipitation Anomaly (mm)</p> </td> <td style="width: 20%;" class=""><span style="color: rgb(51, 51, 51); "><b>${this.T3prP101stData} to ${this.T3prP901stData}</b> <p style="font-size: 1em; background-color: unset; text-align: inherit; margin-left: 19px;"> (${this.T3prMedian1stData}mm)</p> </span><br></td> <td style="width: 20%;" class=""><span style="color: rgb(51, 51, 51); "><b>${this.T3prP102ndData} to ${this.T3prP902ndData}</b> <p style="font-size: 1em; background-color: unset; text-align: inherit; margin-left: 19px;"> (${this.T3prMedian2ndData}mm)</p> </span><br></td> <td style="width: 20%;" class=""><span style="color: rgb(51, 51, 51); "><b>${this.T3prP103rdData} to ${this.T3prP903rdData}</b> <p style="font-size: 1em; background-color: unset; text-align: inherit; margin-left: 19px;"> (${this.T3prMedian3rdData}mm)</p> </span><br></td> <td style="width: 20%;" class=""><span style="color: rgb(51, 51, 51); "><b>${this.T3prP104thData} to ${this.T3prP904thData}</b> <p style="font-size: 1em; background-color: unset; text-align: inherit; margin-left: 19px;"> (${this.T3prMedian4thData}mm)</p> </span><br></td> </tbody></table><p style="font-size:12px;margin-top:7px;"><b style="font-size:12px">Note:</b> The table shows CMIP5 ensemble projection under RCP 8.5. Bold value is the range (10<sup>th</sup>-90<sup>th</sup> Percentile) and values in parentheses show the median (or 50<sup>th</sup> Percentile).</p><div><br><h3><span><strong>Key Trends</strong></span></h3>
          
          <div class="imgWrap figureCountryProfile Figure6"> <p><b><span>${this.fig6aGENfigure}.</span></b> CMIP5 multi-model ensemble projected change (32 GCMs) in annual temperature (top) and precipitation (bottom) by 2040-2059 (left) and by 2080-2099 (right), relative to 1986-2005 baseline under RCP 8.5<span><sup>6</sup></span></p>
          ${this.fig6aGENHtml}${this.fig6bGENHtml}${this.fig6cGENHtml}${this.fig6dGENHtml}
          </div>
          
          <br><h4><b>Temperature</b></h4>
          <div class="imgWrap figureCountryProfile Figure7">
          ${this.fig7GENHtml}
          ${this.fig8GENHtml}
          </div><br>
          <h4><b>Precipitation</b></h4>
          ${this.fig9GENHtml}
          <div><br clear="all"> <div id="ftn1"> <p><br></p> </div></div><div><b><br></b></div>`
      }));
  }
  if (this.generalProfile === false && this.adbProfile === true) {

      this.climatologysArray().push(this.fb.group({
          "climatologySubSectionsTitle": new FormControl({ value: 'Climate Future', disabled: true }), "climatologySubSectionsContent": `<h3>Overview</h3><p><span>The main data source for the World Bank Group’s </span><a href="https://climateknowledgeportal.worldbank.org/"><span>Climate Change Knowledge Portal</span></a><span> (CCKP) is the Coupled Model Inter-comparison Project Phase 5 (CMIP5) models, which are utilized within the Fifth Assessment Report (AR5) of the Intergovernmental Panel on Climate Change (IPCC), providing estimates of future temperature and precipitation. Four Representative Concentration Pathways (i.e. RCP 2.6, RCP 4.5, RCP 6.0, and RCP 8.5) were selected and defined by their total radiative forcing (cumulative measure of GHG emissions from all sources) pathway and level by 2100. In this analysis RCP 2.6 and RCP 8.5, the extremes of low and high emissions pathways, are the primary focus RCP 2.6 represents a very strong mitigation scenario, whereas RCP 8.5 assumes business-as-usual scenario. For more information, please refer to the</span><a href="http://www.iiasa.ac.at/web-apps/tnt/RcpDb/dsd?Action=htmlpage&amp;page=welcome"> RCP Database</a><span>.</span></p><p><br></p><p><b>Tables 2 </b>and <b>3</b><span> below,</span> provide information on temperature projections and anomalies for the four RCPs over two distinct time horizons; presented against the reference period of 1986-2005.</p><p><br></p><p><strong>Table 2</strong>. Data Snapshot: Summary Statistics</p><br>
          <table class="e-rte-table" style="width: 100%; min-width: 0px;"> <thead> <tr> <th style="background-color: rgb(0, 170, 231); width: 67.4167%;" class=""> <p><span style="background-color: transparent;"><span style="color: rgb(242, 242, 242); text-decoration: inherit;">Climate Variables</span></span> </p> </th> <th style="background-color: rgb(0, 170, 231); width: 32.4932%;" class=""> <p><span style="color: rgb(242, 242, 242); text-decoration: inherit;">1901–2016</span> </p> </th> </tr> </thead> <tbody> <tr> <td style="width: 67.4167%;" class=""> <p><span style="font-size: 1em; background-color: unset; text-align: inherit;">Mean Annual Temperature (°C)</span></p> </td> <td style="width: 32.4932%;" class=""> <p>${this.tasData} °C</p> </td> </tr> <tr style="height: 26px;"> <td style="width: 67.4167%; vertical-align: top;" class=""> <p>Mean Annual Precipitation (mm)</p> </td> <td style="width: 32.4932%;" class=""> <p>${this.t2PrData} mm</p> </td> </tr> <tr> <td style="width: 67.4167%;" class=""> <p>Mean Maximum Annual Temperature (°C)</p> </td> <td style="width: 32.4932%;" class=""> <p>${this.T2tasmaxData} °C</p> </td> </tr> <tr> <td style="width: 67.4167%;" class=""> <p>Mean Minimum Annual Temperature (°C)</p> </td> <td style="width: 32.4932%;" class=""> <p>${this.T2tasminData} °C</p> </td> </tr> </tbody></table><br><p style="text-align: left;"><strong>Table 2</strong>.<span> </span><span>Projected anomaly (changes °C) for maximum, minimum, and average daily temperatures in ${this.countryName} for 2040–2059 and 2080–2099, from the reference period of 1986–2005 for all RCPs. The table is showing the median of the CCKP model ensemble and the10–90th percentiles in brackets</span><span>.</span><span><span><sup>4</sup></span></span></p><div><p> <br><b>Table 3.</b> Data Snapshot: CMIP5 Ensemble Projection<br><br></p><table class="e-rte-table" style="width: 99.64%; min-width: 0px; height: 129px;"> <thead> <tr> <th style="background-color: rgb(0, 170, 231); width: 20%;" class=""> <p><span style="background-color: transparent;"><span style="color: rgb(242, 242, 242); text-decoration: inherit;">CMIP5 Ensemble Projections</span></span></p> </th> <th style="background-color: rgb(0, 170, 231); width: 20%;" class=""> <p><span style="color: rgb(242, 242, 242); text-decoration: inherit;">2020-2039</span> </p> </th> <th style="background-color: rgb(0, 170, 231); width: 20%;" class=""><span style="color: rgb(242, 242, 242); text-decoration: inherit;">2040-2059</span> </th> <th style="background-color: rgb(0, 170, 231); width: 20%;" class=""><span style="color: rgb(242, 242, 242); text-decoration: inherit;">2060-2079</span> </th> <th style="background-color: rgb(0, 170, 231); width: 20%;" class=""><span style="color: rgb(242, 242, 242); text-decoration: inherit;">2080-2099</span> </th> </tr> </thead> <tbody> <tr> <td style="width: 20%;" class=""> <p><span style="font-size: 1em; background-color: unset; text-align: inherit;">Annual Temperature Anomaly(°C)</span></p> </td> <td style="width: 20%;" class=""><span style="color: rgb(51, 51, 51); "><b>${this.T3tasP101stData} to ${this.T3tasP901stData}</b> <p style="font-size: 1em; background-color: unset; text-align: inherit; margin-left: 19px;"> (${this.T3tasMedian1stData}°C)</p> </span><br></td> <td style="width: 20%;" class=""><span style="color: rgb(51, 51, 51); "><b>${this.T3tasP102ndData} to ${this.T3tasP902ndData}</b> <p style="font-size: 1em; background-color: unset; text-align: inherit; margin-left: 19px;"> (${this.T3tasMedian2ndData}°C)</p> </span><br></td> <td style="width: 20%;" class=""><span style="color: rgb(51, 51, 51); "> <b>${this.T3tasP103rdData} to ${this.T3tasP903rdData}</b> <p style="font-size: 1em; background-color: unset; text-align: inherit; margin-left: 19px;"> (${this.T3tasMedian3rdData}°C)</p> </span><br></td> <td style="width: 20%;" class=""><span style="color: rgb(51, 51, 51); "><b>${this.T3tasP104thData} to ${this.T3tasP904thData}</b> <p style="font-size: 1em; background-color: unset; text-align: inherit; margin-left: 19px;"> (${this.T3tasMedian4thData}°C)</p> </span><br></td> </tr> <tr style="height: 26px;"> <td style="width: 20%; vertical-align: top;" class=""> <p>Annual Precipitation Anomaly (mm)</p> </td> <td style="width: 20%;" class=""><span style="color: rgb(51, 51, 51); "><b>${this.T3prP101stData} to ${this.T3prP901stData}</b> <p style="font-size: 1em; background-color: unset; text-align: inherit; margin-left: 19px;"> (${this.T3prMedian1stData}mm)</p> </span><br></td> <td style="width: 20%;" class=""><span style="color: rgb(51, 51, 51); "><b>${this.T3prP102ndData} to ${this.T3prP902ndData}</b> <p style="font-size: 1em; background-color: unset; text-align: inherit; margin-left: 19px;"> (${this.T3prMedian2ndData}mm)</p> </span><br></td> <td style="width: 20%;" class=""><span style="color: rgb(51, 51, 51); "><b>${this.T3prP103rdData} to ${this.T3prP903rdData}</b> <p style="font-size: 1em; background-color: unset; text-align: inherit; margin-left: 19px;"> (${this.T3prMedian3rdData}mm)</p> </span><br></td> <td style="width: 20%;" class=""><span style="color: rgb(51, 51, 51); "><b>${this.T3prP104thData} to ${this.T3prP904thData}</b> <p style="font-size: 1em; background-color: unset; text-align: inherit; margin-left: 19px;"> (${this.T3prMedian4thData}mm)</p> </span><br></td> </tbody></table><p style="font-size:12px;margin-top:7px;"><b style="font-size:12px">Note:</b> The table shows CMIP5 ensemble projection under RCP 8.5. Bold value is the range (10<sup>th</sup>-90<sup>th</sup> Percentile) and values in parentheses show the median (or 50<sup>th</sup> Percentile).</p></div><br><p style="text-align: left;"><strong>Table 3</strong>.<span> </span><span>Projections of average temperature anomaly (°C) in <span>${this.countryName}</span> for different seasons (3-monthly time slices) over different time horizons and emissions pathways, showing the median estimates of the full CCKP model ensemble and the 10th and 90th percentiles in brackets</span><span>.</span><sup>17</sup></p><br><h4><b>Model ensemble</b></h4><p>Climate projections presented in this document are derived from datasets available through the CCKP, unless otherwise stated. These datasets are processed outputs of simulations performed by multiple General Circulation Models (GCM) (for further information see Flato et al., 2013).  Collectively, these different GCM simulations are referred to as the ‘model ensemble’. Due to the differences in the way GCMs represent the key physical processes and interactions within the climate system, projections of future climate conditions can vary widely between different GCMs, this is particularly the case for rainfall related variables and at national and local scales. The range of projections from 16 GCMs for annual average temperature change and annual precipitation change in <span>${this.countryName}</span> under RCP 8.5 is shown in <b>Figure 4</b>. Spatial variation of future projections of annual temperature and precipitation for mid and late century under RCP 8.5 are presented in <b>Figure 5</b>.</p><div><br></div>
          
          <div class="imgWrap figureCountryProfile"><p style="text-align: left;"><b>${this.fig4Adbfigure}.</b><span> ‘Projected average temperature anomaly’ and ‘projected annual rainfall anomaly’ in <span>${this.countryName}</span>. Outputs of 16 models within the ensemble simulating RCP 8.5 over the period 2080-2099. Models shown represent the subset of models within the ensemble which provide projections across all RCPs and therefore are most robust for comparison.</span><span><span>17</span></span><span> Three models are labelled.</span></p>
          ${this.fig4AdbHtml}
          </div><br>
          
          <h4><b>Spatial Variation</b></h4><div class="imgWrap figureCountryProfile"> <p><b><span>${this.fig5aAdbfigure} </span></b><span>CMIP5 ensemble projected change (32 GCMs) in annual temperature (top) and precipitation (bottom) by 2040–2059 (left) and by 2080–2090 (right) relative to 1986–2005 baseline under RCP 8.5.<sup>6</sup></span></p>
          ${this.fig5aAdbHtml}${this.fig5bAdbHtml}${this.fig5cAdbHtml}${this.fig5dAdbHtml}
          </p> <p style="text-align: left;"></div><br><h4><b>Temperature</b></h4><br>
          
          <div class="imgWrap figureCountryProfile Figure7">
              <span style="width: 48%;">
                  <p><b><span>${this.fig6Adbfigure}.</span></b> <span>Historic and projected average annual temperature in <span>
                      ${this.countryName}</span> under RCP 2.6 (blue) and RCP 8.5 (red) estimated by the model ensemble. 
                      Shading represents the standard deviation of the model ensemble.<sup>7</sup></span></p> 
                      ${this.fig6AdbHtml}
              </span>
              <span style="width: 48%;">
                  <p><b><span>${this.fig7Adbfigure}.</span></b> 
                      <span>Projected change (anomaly) in monthly temperature, shown by month, for 
                          <span>${this.countryName}</span> for the period 2080-2099 under RCP 8.5. 
                          The value shown represents the median of the model ensemble with the 
                          shaded areas showing the 10th – 90th percentiles.<sup>8</sup></span>
                      </p>
                      ${this.fig7AdbHtml}
              </span>
          </div>`
      }));
  }
  setTimeout(() => {
      this.loading = false;
      }, 10000);

}

fatchAPIclimateRelatedNaturalHazardSection() {
  this.climateRelatedNaturalHazardFormGroup.clear();
  if (this.generalProfile === true && this.adbProfile === false) {
      this.form.patchValue({
          climateRelatedNaturalHazardMainSection: `<h3>Overview</h3><p><span>Data from the Emergency Event Database: EM-Dat,<sup>10</sup> presented in <b>Table 4</b>, shows the country has endured various natural hazards, including floods, landslides, epidemic diseases, storms, earthquakes and droughts, costing lives, and economic damage.</span></p><div><br><p><b><span>Table 4.</span></b><span> Natural Disasters in <span>${this.countryName}</span>, 1900 - 2020</span></p>`,
          climateRelatedNaturalHazardFooterLinks: '',
      });
  }
}
fatchAPIclimateChangeImpactSection() {
    this.form.patchValue({
        climateChangeImpactMainSection: '',
        climateChangeImpactFooterLinks: '',
    });
}
fatchAPIpoliciesAndProgramSection() {
    this.form.patchValue({
        policiesAndProgramMainSection: '',
        policiesAndProgramFooterLinks: '',
    });
}
fatchAPIadaptationSection() {
    this.adaptationFormGroup.clear();
    this.form.patchValue({
        adaptationMainSection: '',
        adaptationFooterLinks: '',
    });
}

AllfatchAPI(){
  this.loading = true;
  if (this.generalProfile === true && this.adbProfile === false) {
      this.fetchGENImageData(this.countryCode,this.countryName);
  }  
  if (this.generalProfile === false && this.adbProfile === true) { 
      this.fetchABDImageData(this.countryCode,this.countryName);
  } 
  this.fatchAPIacknowledgementSection();
  this.fatchAPIforewordSection();
  this.fatchAPIcountryOverviewSection();
  this.fatchAPIadaptationSection();
  this.fatchAPIclimateChangeImpactSection();
  this.fatchAPIclimateRelatedNaturalHazardSection();
  this.fatchAPIkeyMessageSection();
  this.fatchAPIpoliciesAndProgramSection();
  setTimeout(() => {
      this.fatchAPIclimatologySection();
      }, 10000);
  setTimeout(() => {
    this.loading = false;
    }, 25000);
}

  editCountryProfile( countryProfileID) {
    this.ngZone.run(() => this.router.navigateByUrl('updateCountryProfile/' + countryProfileID));
  }

  get myCountryProfileForm() {
    return this.form.controls;
  }


  saveAsDraft = async (event) => {
    this.isSubmitted = true;
    this.loading = true;
    if (!this.form.valid) {
      this.loading = false;
      for (const key of Object.keys(this.form.controls)) {
        if (this.form.controls[key].invalid) {
          for (var i = 0; i < this.form.controls[key]['controls'].length; i++) {
            this.form.controls[key]['controls'].map(res => {
              this.forwardSubSectionErrorMsg = res['controls']['forewordSubSectionsContent'];
              this.keyMessageSubSectionErrorMsg = res['controls']['keyMessageSubSectionsTitle'];
              this.countryOverviewSubSectionsErrorMsg = res['controls']['countryOverviewSubSectionsTitle'];
              this.climatologySubSectionErrorMsg = res['controls']['climatologySubSectionsTitle'];
              this.climateRelatedNaturalHazardSubSectionErrorMsg = res['controls']['climateRelatedNaturalHazardSubSectionsTitle'];
              this.climateChangeImpactSubSectionErrorMsg = res['controls']['climateChangeImpactSubSectionsTitle'];
              this.policiesAndProgramSubSectionErrorMsg = res['controls']['policiesAndProgramSubSectionsTitle'];
              this.adaptationSubSectionErrorMsg = res['controls']['adaptationSubSectionsTitle'];
            });

            if (this.forwardSubSectionErrorMsg) {
              this.toastr.errorToastr(`Forward Sub section title is required`, ``, {
                dismiss: 'auto',
                enableHTML: true,
                showCloseButton: true,
                animate: 'slideFromLeft',
                newestOnTop: true,
                maxShown: 10
              });

            }

            if (this.keyMessageSubSectionErrorMsg) {
              this.toastr.errorToastr(`keyMessage Sub section title is required`, ``, {
                dismiss: 'auto',
                enableHTML: true,
                showCloseButton: true,
                animate: 'slideFromLeft',
                newestOnTop: true
              });
            }

            if (this.countryOverviewSubSectionsErrorMsg) {
              this.toastr.errorToastr(`CountryOverview Sub section title is required`, ``, {
                dismiss: 'auto',
                enableHTML: true,
                showCloseButton: true,
                animate: 'slideFromLeft',
                newestOnTop: true
              });
            }

            if (this.climatologySubSectionErrorMsg) {
              this.toastr.errorToastr(`Climatology Sub section title is required`, ``, {
                dismiss: 'auto',
                enableHTML: true,
                showCloseButton: true,
                animate: 'slideFromLeft',
                newestOnTop: true
              });
            }

            if (this.climateRelatedNaturalHazardSubSectionErrorMsg) {
              this.toastr.errorToastr(`Climate Related Natural Hazard Sub section title is required`, ``, {
                dismiss: 'auto',
                enableHTML: true,
                showCloseButton: true,
                animate: 'slideFromLeft',
                newestOnTop: true
              });
            }

            if (this.climateChangeImpactSubSectionErrorMsg) {
              this.toastr.errorToastr(`Climate Change Impact Sub section title is required`, ``, {
                dismiss: 'auto',
                enableHTML: true,
                showCloseButton: true,
                animate: 'slideFromLeft',
                newestOnTop: true
              });
            }

            if (this.policiesAndProgramSubSectionErrorMsg) {
              this.toastr.errorToastr(`Policies And Program Sub section title is required`, ``, {
                dismiss: 'auto',
                enableHTML: true,
                showCloseButton: true,
                animate: 'slideFromLeft',
                newestOnTop: true
              });
            }

            if (this.adaptationSubSectionErrorMsg) {
              this.toastr.errorToastr(`Adaptation Sub section title is required`, ``, {
                dismiss: 'auto',
                enableHTML: true,
                showCloseButton: true,
                animate: 'slideFromLeft',
                newestOnTop: true
              });
            }
            break;
          }
        }
      }
      return false;
    } else {
      this.countryProfile.postUrl(this.countryProfileID, { pdfUrl: null, status: 1 }).subscribe(res => {
        console.log('success')
      })
      this.form.patchValue({
        isCheckedAcknowledgementSection: this.isCheckedAcknowledgementSection,
        isCheckedforewordSection: this.isCheckedforewordSection,
        isCheckedkeyMessageSection: this.isCheckedkeyMessageSection,
        isCheckedcountryOverviewSection: this.isCheckedcountryOverviewSection,
        isCheckedclimatologySection: this.isCheckedclimatologySection,
        isCheckedclimateRelatedNaturalHazardSection: this.isCheckedclimateRelatedNaturalHazardSection,
        isCheckedclimateChangeImpactSection: this.isCheckedclimateChangeImpactSection,
        isCheckedpoliciesAndProgramSection: this.isCheckedpoliciesAndProgramSection,
        isCheckedadaptationSection: this.isCheckedadaptationSection,

        isDisplayAcknowledgementSection: this.isDisplayAcknowledgementSection,
        isDisplayforewordSection: this.isDisplayforewordSection,
        isDisplaykeyMessageSection: this.isDisplaykeyMessageSection,
        isDisplaycountryOverviewSection: this.isDisplaycountryOverviewSection,
        isDisplayclimatologySection: this.isDisplayclimatologySection,
        isDisplayclimateRelatedNaturalHazardSection: this.isDisplayclimateRelatedNaturalHazardSection,
        isDisplayclimateChangeImpactSection: this.isDisplayclimateChangeImpactSection,
        isDisplaypoliciesAndProgramSection: this.isDisplaypoliciesAndProgramSection,
        isDisplayadaptationSection: this.isDisplayadaptationSection,
      });
      await this.createsectionsService.updateSection(this.form.getRawValue(), this.countryProfileID).subscribe(
        res => {
          this.loading = false;
          if (event.target.id === "saveInformation") {
            this.toastr.successToastr("Page saved successfully")
          } else {
            this.ngZone.run(() => this.router.navigateByUrl('home'));
          }
         // this.toastr.success("We are generating your pdf, we will let you know once done.")
        },
        err => {
          if (err.status == 422) {
            this.serverErrorMessages = err.error.join('<br/');
          }
          else {
            this.serverErrorMessages = 'Something went wrong.Please contact admin.';
          }
        }
      );
    }
  }

  submit = async () => {
    this.isSubmitted = true;
    this.loading = true;
    if (!this.form.valid) {
      this.loading = false;
      for (const key of Object.keys(this.form.controls)) {
        if (this.form.controls[key].invalid) {
          for (var i = 0; i < this.form.controls[key]['controls'].length; i++) {
            this.form.controls[key]['controls'].map(res => {
              this.forwardSubSectionErrorMsg = res['controls']['forewordSubSectionsContent'];
              this.keyMessageSubSectionErrorMsg = res['controls']['keyMessageSubSectionsTitle'];
              this.countryOverviewSubSectionsErrorMsg = res['controls']['countryOverviewSubSectionsTitle'];
              this.climatologySubSectionErrorMsg = res['controls']['climatologySubSectionsTitle'];
              this.climateRelatedNaturalHazardSubSectionErrorMsg = res['controls']['climateRelatedNaturalHazardSubSectionsTitle'];
              this.climateChangeImpactSubSectionErrorMsg = res['controls']['climateChangeImpactSubSectionsTitle'];
              this.policiesAndProgramSubSectionErrorMsg = res['controls']['policiesAndProgramSubSectionsTitle'];
              this.adaptationSubSectionErrorMsg = res['controls']['adaptationSubSectionsTitle'];
            });

            if (this.forwardSubSectionErrorMsg) {
              this.toastr.errorToastr(`Forward Sub section title is required`, ``, {
                dismiss: 'auto',
                enableHTML: true,
                showCloseButton: true,
                animate: 'slideFromLeft',
                newestOnTop: true,
                maxShown: 10
              });

            }

            if (this.keyMessageSubSectionErrorMsg) {
              this.toastr.errorToastr(`keyMessage Sub section title is required`, ``, {
                dismiss: 'auto',
                enableHTML: true,
                showCloseButton: true,
                animate: 'slideFromLeft',
                newestOnTop: true
              });
            }

            if (this.countryOverviewSubSectionsErrorMsg) {
              this.toastr.errorToastr(`CountryOverview Sub section title is required`, ``, {
                dismiss: 'auto',
                enableHTML: true,
                showCloseButton: true,
                animate: 'slideFromLeft',
                newestOnTop: true
              });
            }

            if (this.climatologySubSectionErrorMsg) {
              this.toastr.errorToastr(`Climatology Sub section title is required`, ``, {
                dismiss: 'auto',
                enableHTML: true,
                showCloseButton: true,
                animate: 'slideFromLeft',
                newestOnTop: true
              });
            }

            if (this.climateRelatedNaturalHazardSubSectionErrorMsg) {
              this.toastr.errorToastr(`Climate Related Natural Hazard Sub section title is required`, ``, {
                dismiss: 'auto',
                enableHTML: true,
                showCloseButton: true,
                animate: 'slideFromLeft',
                newestOnTop: true
              });
            }

            if (this.climateChangeImpactSubSectionErrorMsg) {
              this.toastr.errorToastr(`Climate Change Impact Sub section title is required`, ``, {
                dismiss: 'auto',
                enableHTML: true,
                showCloseButton: true,
                animate: 'slideFromLeft',
                newestOnTop: true
              });
            }

            if (this.policiesAndProgramSubSectionErrorMsg) {
              this.toastr.errorToastr(`Policies And Program Sub section title is required`, ``, {
                dismiss: 'auto',
                enableHTML: true,
                showCloseButton: true,
                animate: 'slideFromLeft',
                newestOnTop: true
              });
            }

            if (this.adaptationSubSectionErrorMsg) {
              this.toastr.errorToastr(`Adaptation Sub section title is required`, ``, {
                dismiss: 'auto',
                enableHTML: true,
                showCloseButton: true,
                animate: 'slideFromLeft',
                newestOnTop: true
              });
            }
            break;
          }
        }
      }
      return false;
    } else {
      const pdfReportContent = {
        "URL": "google.co.in",
        "profilename": this.countryName.replace(/[^a-zA-Z]/g, "") + this.countryProfileID + ".pdf",
        "conversionDelay": environment.conversionDelay,
        "navigationtimeout": environment.navigationtimeout,
        "footerText": "<table style='width: 100%;margin-top:20px'><tr><td style='width: 90%;padding-left:6rem;'><p style='font-weight: bold;font-size: 15px;font-family:" + this.AndesBold + ", sans-serif; color:rgb(44,107,172);letter-spacing: -0.2px;text-transform: uppercase;'> CLIMATE RISK COUNTRY PROFILE — " + this.countryName + " </p></td><td> <p style='font-weight: bold;  font-size: 15px; font-family:" + this.AndesBold + ", sans-serif; color: rgb(44,107,172);'>&p;</p></td></tr></table>",
        "sectionlist": [
          {
            "sectiontitle": "firstcoverpage",
            "sectionurl": environment.appUrl + "/pdfReport/getFirstPage/" + this.countryProfileID
            //"sectionurl":"http://15.206.234.47/pdfReport/getFirstPage/5f22534efdec3e70a558ccff"
          },
          {
            "sectiontitle": "acknowledgement",
            "sectionurl": environment.appUrl + "/pdfReport/getSecondPage/" + this.countryProfileID
            //"sectionurl":"http://15.206.234.47/pdfReport/getKeyMessagesSection/5f22534efdec3e70a558ccff"
          },
          {
            "sectiontitle": "content",
            "sectionurl": environment.appUrl + "/pdfReport/content/" + this.countryProfileID
            //"sectionurl":"http://15.206.234.47/pdfReport/getForewordsSection/5f28f008fdec3e70a558cd09"
          },
          {
            "sectiontitle": "lastcoverpage",
            "sectionurl": environment.appUrl + "/pdfReport/getFirstPage/" + this.countryProfileID
            //"sectionurl":"http://15.206.234.47/pdfReport/getFirstPage/5f22534efdec3e70a558ccff"
          }
        ]
      }

      await this.countryProfile.getPDFReport(pdfReportContent).subscribe(
        res => {
          this.loading = false;
          //setTimeout(() => {
          //res['URL'] = "http://15.206.234.47/pdfReport/getFirstPage/5f22534efdec3e70a558ccff";
          this.pdfUrlMsg = res['URL'];
          this.countryProfile.postUrl(this.countryProfileID, { pdfUrl: this.pdfUrlMsg }).subscribe(res => {
            console.log('success')
          })
          // }, 6000);
          this.toaster.clear();
          this.toaster.success(`Your PDF is generated successfully for ${this.countryName} - ${this.countryProfileTitle}.<br>Click <a class="pdfUrlMsg" href="${this.pdfUrlMsg}" target="_blank" > here </a> to open the PDF.`)
        },
        err => {
          console.log(err);
        }
      );

      this.countryProfile.postUrl(this.countryProfileID, { status: 2 }).subscribe(res => {
        console.log('success')
      })

      this.form.patchValue({
        isCheckedAcknowledgementSection: this.isCheckedAcknowledgementSection,
        isCheckedforewordSection: this.isCheckedforewordSection,
        isCheckedkeyMessageSection: this.isCheckedkeyMessageSection,
        isCheckedcountryOverviewSection: this.isCheckedcountryOverviewSection,
        isCheckedclimatologySection: this.isCheckedclimatologySection,
        isCheckedclimateRelatedNaturalHazardSection: this.isCheckedclimateRelatedNaturalHazardSection,
        isCheckedclimateChangeImpactSection: this.isCheckedclimateChangeImpactSection,
        isCheckedpoliciesAndProgramSection: this.isCheckedpoliciesAndProgramSection,
        isCheckedadaptationSection: this.isCheckedadaptationSection,

        isDisplayAcknowledgementSection: this.isDisplayAcknowledgementSection,
        isDisplayforewordSection: this.isDisplayforewordSection,
        isDisplaykeyMessageSection: this.isDisplaykeyMessageSection,
        isDisplaycountryOverviewSection: this.isDisplaycountryOverviewSection,
        isDisplayclimatologySection: this.isDisplayclimatologySection,
        isDisplayclimateRelatedNaturalHazardSection: this.isDisplayclimateRelatedNaturalHazardSection,
        isDisplayclimateChangeImpactSection: this.isDisplayclimateChangeImpactSection,
        isDisplaypoliciesAndProgramSection: this.isDisplaypoliciesAndProgramSection,
        isDisplayadaptationSection: this.isDisplayadaptationSection,
      });
      await this.createsectionsService.updateSection(this.form.getRawValue(), this.countryProfileID).subscribe(
        res => {
          this.loading = false;
          this.ngZone.run(() => this.router.navigateByUrl('home'));
          //this.toaster.clear();
          this.toaster.success("We are generating your pdf, we will let you know once done.")

        },
        err => {
          if (err.status == 422) {
            this.serverErrorMessages = err.error.join('<br/');
          }
          else {
            this.serverErrorMessages = 'Something went wrong.Please contact admin.';
          }
        }
      );
    }
  }


}
