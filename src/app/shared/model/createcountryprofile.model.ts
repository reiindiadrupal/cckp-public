export class Createcountryprofile {
    title: String;
    regionName: String;
    countryName: String;
    primaryImageSetURL: String;
    secondaryImageSetURL: String;
    wbLogoSetURL: String;
    adbWbLogoSetURL: String;
    isAdbProfile: Boolean;
    isGenralProfile:Boolean;
    pdfUrl: String;    
    createdBy: String;
    updatedBy: String;
    status: number;
    createdDate: String;
    updatedDate: String;
}
