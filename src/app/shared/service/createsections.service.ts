import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { Createsections } from '../model/createsections.model';
import { Observable } from 'rxjs';
import { MsalGuard, MsalService } from '@azure/msal-angular';


@Injectable({
  providedIn: 'root'
})
export class CreatesectionsService {

  constructor(private http: HttpClient, private router: Router ,private _msalService: MsalService,) { }

  postSections(createsections: Createsections) {
    if(environment.isBasicAuth){
      const tokenObs = Observable.fromPromise(this._msalService.acquireTokenSilent({scopes: ["user.read"]}));
      return tokenObs
      .mergeMap( token => { 
        const headers = { 'Authorization':  `Bearer ${token.accessToken}` };         return this.http.post(environment.apiBaseUrl+'/postSections',createsections,{ headers})
        }
      );
    } else { 
      return this.http.post(environment.apiBaseUrl+'/postSections',createsections);
    }
  }

  updateSection(createsections: Createsections, countryProfileID) {
    if(environment.isBasicAuth){
      const tokenObs = Observable.fromPromise(this._msalService.acquireTokenSilent({scopes: ["user.read"]}));
      return tokenObs
      .mergeMap( token => { 
        const headers = { 'Authorization':  `Bearer ${token.accessToken}` };         return this.http.post(environment.apiBaseUrl +'/updateSection/'+countryProfileID, createsections,{ headers })
        }
      );
    } else { 
      return this.http.post(environment.apiBaseUrl +'/updateSection/'+countryProfileID, createsections);
    }
    
  }

}
