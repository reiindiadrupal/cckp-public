import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { Createcountrylist } from '../model/createcountrylist.model';
import { MsalGuard, MsalService } from '@azure/msal-angular';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
@Injectable({
  providedIn: 'root'
})
export class CreatecountrylistService {

  constructor(private http: HttpClient, private router: Router , private _msalService: MsalService) { }

  getAllCountryLists() {
    if(environment.isBasicAuth){
      const tokenObs = Observable.fromPromise(this._msalService.acquireTokenSilent({scopes: ["user.read"]}));
      return tokenObs
      .mergeMap( token => { 
        const headers = { 'Authorization':  `Bearer ${token.accessToken}` };
        return this.http.get(environment.apiBaseUrl+'/allCountryLists', { headers })
        }
      );
    } else { 
      return this.http.get(environment.apiBaseUrl+'/allCountryLists');
    }
  }
}
