import { TestBed } from '@angular/core/testing';

import { CreatecountryprofileService } from './createcountryprofile.service';

describe('CreatecountryprofileService', () => {
  let service: CreatecountryprofileService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreatecountryprofileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
