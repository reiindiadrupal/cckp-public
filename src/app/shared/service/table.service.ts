import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { Createsections } from '../model/createsections.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TableService {
  tasData: any = []; 
  testArray = {};
  constructor(private http: HttpClient, private router: Router) { }

//table1 value
  
  getLifeExpectancyAtBirth = (countryISOCode) => {
    return this.http.get(`https://api.worldbank.org/v2/country/${countryISOCode}/indicator/SP.DYN.LE00.IN?format=json&date=2018`);
  }

  getAdultLiteracyRate = (countryISOCode) => {
    return this.http.get(`https://api.worldbank.org/v2/country/${countryISOCode}/indicator/SE.ADT.LITR.ZS?format=json&date=2018`);
  }

  getPopulationDensity = (countryISOCode) => {
    return this.http.get(`https://api.worldbank.org/v2/country/${countryISOCode}/indicator/EN.POP.DNST?format=json&date=2018`);
  }

  getPercentOfPopulationwithAccessToElectricity = (countryISOCode) => {
    return this.http.get(`https://api.worldbank.org/v2/country/${countryISOCode}/indicator/EG.ELC.ACCS.ZS?format=json&date=2018`);
  }

  getGdpPerCapita = (countryISOCode) => {
    return this.http.get(`https://api.worldbank.org/v2/country/${countryISOCode}/indicator/NY.GDP.PCAP.CD?format=json&date=2018`);
  }

  //---------------------table 2 values---------------------------*/
  getTasValues(countryISOCode) {
     return this.http.get(environment.T2tasURl + countryISOCode);
  }
  getT2prVlaues(countryISOCode) {
    return this.http.get(environment.T2prURl + countryISOCode);
  }
  getT2tasmax(countryISOCode) {
    return this.http.get(environment.T2tasmaxURl + countryISOCode);
  }
  getT2tasmin(countryISOCode) {
    return this.http.get(environment.T2tasminURl + countryISOCode)
  }
  /*---------------------------------------------------------*/

  //table 3 tas values
  ///*--------------Table 3 tas P10------------------------- */
  getT3tasP101st(countryISOCode) {
    return this.http.get(environment.T3tasP101stUrl + countryISOCode)
  }
  getT3tasP102nd(countryISOCode) {
    return this.http.get(environment.T3tasP102ndUrl + countryISOCode)
  }
  getT3tasP103rd(countryISOCode) {
    return this.http.get(environment.T3tasP103rdUrl + countryISOCode)
  }
  getT3tasP104th(countryISOCode) {
    return this.http.get(environment.T3tasP104thUrl + countryISOCode)
  }
  /*------------------------------------------------------------------------------- */

  //*-----------------------Table 3 tas median-------------------------------------- */
  getT3tasMedian1st(countryISOCode) {
    return this.http.get(environment.T3tasMedian1stUrl + countryISOCode)
  }
  getT3tasMedian2nd(countryISOCode) {
    return this.http.get(environment.T3tasMedian2ndUrl + countryISOCode)
  }
  getT3tasMedian3rd(countryISOCode) {
    return this.http.get(environment.T3tasMedian3rdUrl + countryISOCode)
  }
  getT3tasMedian4th(countryISOCode) {
    return this.http.get(environment.T3tasMedian4thUrl + countryISOCode)
  }
/*------------------------------------------------------------------------------- */
//*-----------------------Table 3 tas p90-------------------------------------- */
  getT3tasP901st(countryISOCode) {
    return this.http.get(environment.T3tasP901stUrl + countryISOCode)
  }
  getT3tasP902nd(countryISOCode) {
    return this.http.get(environment.T3tasP902ndUrl + countryISOCode)
  }
  getT3tasP903rd(countryISOCode) {
    return this.http.get(environment.T3tasP903rdUrl + countryISOCode)
  }
  getT3tasP904th(countryISOCode) {
    return this.http.get(environment.T3tasP904thUrl + countryISOCode)
  }
/*------------------------------------------------------------------------------- */

    //table 3 pr values
/*--------------Table 3 pr P10--------------------------------------------------- */
  getT3prP101st(countryISOCode) {
    return this.http.get(environment.T3prP101stUrl + countryISOCode)
  }
  getT3prP102nd(countryISOCode) {
    return this.http.get(environment.T3prP102ndUrl + countryISOCode)
  }
  getT3prP103rd(countryISOCode) {
    return this.http.get(environment.T3prP103rdUrl + countryISOCode)
  }
  getT3prP104th(countryISOCode) {
    return this.http.get(environment.T3prP104thUrl + countryISOCode)
  }
/*-------------------------------------------------------------------------------*/

//*-----------------------Table 3 pr median-------------------------------------- */
  getT3prMedian1st(countryISOCode) {
    return this.http.get(environment.T3prMedian1stUrl + countryISOCode)
  }
  getT3prMedian2nd(countryISOCode) {
    return this.http.get(environment.T3prMedian2ndUrl + countryISOCode)
  }
  getT3prMedian3rd(countryISOCode) {
    return this.http.get(environment.T3prMedian3rdUrl + countryISOCode)
  }
  getT3prMedian4th(countryISOCode) {
    return this.http.get(environment.T3prMedian4thUrl + countryISOCode)
  }
/*-------------------------------------------------------------------------------*/
//*-----------------------Table 3 pr p90-------------------------------------- */
  getT3prP901st(countryISOCode) {
    return this.http.get(environment.T3prP901stUrl + countryISOCode)
  }
  getT3prP902nd(countryISOCode) {
    return this.http.get(environment.T3prP902ndUrl + countryISOCode)
  }
  getT3prP903rd(countryISOCode) {
    return this.http.get(environment.T3prP903rdUrl + countryISOCode)
  }
  getT3prP904th(countryISOCode) {
    return this.http.get(environment.T3prP904thUrl + countryISOCode)
  }
/*-------------------------------------------------------------------------------*/
 }
