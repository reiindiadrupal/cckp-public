import { TestBed } from '@angular/core/testing';

import { CreatesectionsService } from './createsections.service';

describe('CreatesectionsService', () => {
  let service: CreatesectionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreatesectionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
