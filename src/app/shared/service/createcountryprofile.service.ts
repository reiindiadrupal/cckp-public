import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { Createcountryprofile } from '../model/createcountryprofile.model';
import { MsalGuard, MsalService } from '@azure/msal-angular';


@Injectable({
  providedIn: 'root'
})
export class CreatecountryprofileService {

  constructor(private http: HttpClient, private router: Router ,private _msalService: MsalService) { }

  getAllCountryProfiles() {
    if(environment.isBasicAuth){
      const tokenObs = Observable.fromPromise(this._msalService.acquireTokenSilent({scopes: ["user.read"]}));
      return tokenObs
      .mergeMap( token => { 
        const headers = { 'Authorization':  `Bearer ${token.accessToken}` };         return this.http.get(environment.apiBaseUrl+'/allCountryProfiles', { headers })
        }
      );
    } else { 
      return this.http.get(environment.apiBaseUrl+'/allCountryProfiles');
    }
  }
  getCountryProfile(countryProfileID) {
      return this.http.get(environment.apiBaseUrl+'/countryProfile/'+countryProfileID);
  }
  deleteCountryProfile(countryProfileID) {
    if(environment.isBasicAuth){
      const tokenObs = Observable.fromPromise(this._msalService.acquireTokenSilent({scopes: ["user.read"]}));
      return tokenObs
      .mergeMap( token => { 
        const headers = { 'Authorization':  `Bearer ${token.accessToken}` };         
        return this.http.get(environment.apiBaseUrl+'/removeCountryProfile/'+countryProfileID, { headers })
        }
      );
        
    } else { 
      return this.http.get(environment.apiBaseUrl+'/removeCountryProfile/'+countryProfileID);
    }
  }
  postCreateCountryProfile(createcountryprofile: Createcountryprofile) {
    if(environment.isBasicAuth){
      const tokenObs = Observable.fromPromise(this._msalService.acquireTokenSilent({scopes: ["user.read"]}));
      return tokenObs
      .mergeMap( token => { 
        const headers = { 'Authorization':  `Bearer ${token.accessToken}` };         return this.http.post(environment.apiBaseUrl+'/countryProfile',createcountryprofile, { headers })
        }
      );
        
    } else { 
      return this.http.post(environment.apiBaseUrl+'/countryProfile',createcountryprofile);
    }
  }
  postPrimaryImage(imageData) {
    if(environment.isBasicAuth){
      const tokenObs = Observable.fromPromise(this._msalService.acquireTokenSilent({scopes: ["user.read"]}));
      return tokenObs
      .mergeMap( token => { 
        const headers = { 'Authorization':  `Bearer ${token.accessToken}` };         return this.http.post(environment.apiBaseUrl+'/postPrimaryImage',imageData, { headers })
        }
      );
        
    } else { 
      return this.http.post(environment.apiBaseUrl+'/postPrimaryImage',imageData);
    }
  }
  postSecondaryImage(imageData) {
    if(environment.isBasicAuth){
      const tokenObs = Observable.fromPromise(this._msalService.acquireTokenSilent({scopes: ["user.read"]}));
      return tokenObs
      .mergeMap( token => { 
        const headers = { 'Authorization':  `Bearer ${token.accessToken}` };         return this.http.post(environment.apiBaseUrl+'/postSecondaryImage',imageData, { headers })
        }
      );
        
    } else { 
      return this.http.post(environment.apiBaseUrl+'/postSecondaryImage',imageData);
    }
    
  }
  postWbLogoImage(imageData) {
    if(environment.isBasicAuth){
      const tokenObs = Observable.fromPromise(this._msalService.acquireTokenSilent({scopes: ["user.read"]}));
      return tokenObs
      .mergeMap( token => { 
        const headers = { 'Authorization':  `Bearer ${token.accessToken}` };         return this.http.post(environment.apiBaseUrl+'/postWbLogoImage',imageData, { headers });
        }
      );
        
    } else { 
      return this.http.post(environment.apiBaseUrl+'/postWbLogoImage',imageData);
    }
    
  }
  postAdbWbLogoImage(imageData) {
    if(environment.isBasicAuth){
      const tokenObs = Observable.fromPromise(this._msalService.acquireTokenSilent({scopes: ["user.read"]}));
      return tokenObs
      .mergeMap( token => { 
        const headers = { 'Authorization':  `Bearer ${token.accessToken}` };         return this.http.post(environment.apiBaseUrl+'/postAdbWbLogoImage',imageData, { headers });
        }
      );
        
    } else { 
      return this.http.post(environment.apiBaseUrl+'/postAdbWbLogoImage',imageData);
    }
    
  }
  updateCountryProfile(createcountryprofile: Createcountryprofile, countryProfileID) {
    if(environment.isBasicAuth){
      const tokenObs = Observable.fromPromise(this._msalService.acquireTokenSilent({scopes: ["user.read"]}));
      return tokenObs
      .mergeMap( token => { 
        const headers = { 'Authorization':  `Bearer ${token.accessToken}` };         return this.http.post(environment.apiBaseUrl + '/updatecountryProfile/'+countryProfileID, createcountryprofile, { headers });
        }
      );
        
    } else { 
      return this.http.post(environment.apiBaseUrl + '/updatecountryProfile/'+countryProfileID, createcountryprofile);
    }
    
  }
  getPDFReport(pdfReportContent) {
    if(environment.isBasicAuth){
      const tokenObs = Observable.fromPromise(this._msalService.acquireTokenSilent({scopes: ["user.read"]}));
      return tokenObs
      .mergeMap( token => { 
        const headers = { 'Authorization':  `Bearer ${token.accessToken}` };         return this.http.post(environment.pdfcreatorServerUrl,pdfReportContent, { headers });
        }
      );
        
    } else { 
      return this.http.post(environment.pdfcreatorServerUrl,pdfReportContent);
    }
  }
  postUrl( countryProfileID, pdfUrl) {
    if(environment.isBasicAuth){
      const tokenObs = Observable.fromPromise(this._msalService.acquireTokenSilent({scopes: ["user.read"]}));
      return tokenObs
      .mergeMap( token => { 
        const headers = { 'Authorization':  `Bearer ${token.accessToken}` };         return this.http.post(environment.apiBaseUrl + '/postUrl/' + countryProfileID, pdfUrl, { headers });
        }
      );
        
    } else { 
      return this.http.post(environment.apiBaseUrl + '/postUrl/' + countryProfileID, pdfUrl);
    }
    
  }
}
