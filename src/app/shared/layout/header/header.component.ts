import { constants } from '../../../config/constants';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  headerData = constants.headerComponent
  constructor() { }

  ngOnInit(): void {
  }

}
