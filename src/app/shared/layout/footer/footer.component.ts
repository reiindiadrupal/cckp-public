import { Component, OnInit } from '@angular/core';
import { constants } from '../../../config/constants';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  footerData = constants.footerComponent
  constructor() { }

  ngOnInit(): void {
  }

}
