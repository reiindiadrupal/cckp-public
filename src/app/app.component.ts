import { Component, OnInit, NgZone } from '@angular/core';
import {
  Event,
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router
} from '@angular/router';
import { BroadcastService, MsalService } from '@azure/msal-angular';
import { environment } from '../environments/environment';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Climate Change Knowledge Portal';
  loading = false;

  constructor(private router: Router,private ngZone: NgZone,private broadcastService: BroadcastService, private authService: MsalService,private toastr: ToastrManager) {
    this.router.events.subscribe((event: Event) => {
      switch (true) {
        case event instanceof NavigationStart: {
          this.loading = true;
          break;
        }
 
        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: {
          this.loading = false;
          break;
        }
        default: {
          break;
        }
      }
    });
 
    if(environment.isBasicAuth){ 
      this.broadcastService.subscribe("msal:loginFailure", (payload) => {
        if(payload.errorCode=="interaction_required")   {
          this.toastr.errorToastr("You are not authorized to access this portal")
          this.ngZone.run(() => this.router.navigateByUrl('home'));
        }
      })
    }
  }
}
