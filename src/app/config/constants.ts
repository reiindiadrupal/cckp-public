export const constants = {
    homeComponent: {
        dashboardTitle: "Profile(s) Dashboard",
        noProfiles: "No profile available",
        profileDeleted: "Profile has been deleted successfully.",
        newProfileCreation: "Create New Profile",
        pdfMsg: "Your PDF is getting generated we will notify you once generated.",
        pdfUrlMsg: "You can download the pdf from here."
    },

    headerComponent: {
        Logo: "./assets/images/logo/logo-wb-cckp.png",
        wbgLogo: "./assets/images/logo/WBG-logo.png",
        cckpLogo: "./assets/images/logo/CCKP-logo.png",
        logoLine:"./assets/images/logo/line.png"
    },
    
    footerComponent: {
        climateChange: "Climate Change Knowledge Portal",
        copyRight: "@" + ' ' + `${new Date().getFullYear()}` + ' ' + "The World Bank Group, All Rights Reserved.",
        footerLogo:"./assets/images/logo/logo-wbg-footer-en.svg"
    },

    updateCountryProfileComponent: {
        title: "Update Profile",
        titleValidationMsg: "Title is required.",
        regionValidationMsg: "Select Region.",
        countryValidationMsg:"Select Country."
    }
};