import { AlertModule } from './alert.module';
import { AlertService } from './shared/service/alert.service';
import { UpdateCountryProfileComponent } from './country-profile/update-country-profile/update-country-profile.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './shared/layout/footer/footer.component';
import { HeaderComponent } from './shared/layout/header/header.component';
import { APP_BASE_HREF } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { CreateCountryProfileComponent } from './country-profile/create-country-profile/create-country-profile.component';
import { HttpClientModule,HTTP_INTERCEPTORS,} from '@angular/common/http';
import { CreateSectionComponent } from './country-profile/create-section/create-section.component';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccordionModule } from '@syncfusion/ej2-angular-navigations';
import { DropDownListModule } from '@syncfusion/ej2-angular-dropdowns';
import { CreatesectionsService } from './shared/service/createsections.service';
import { CreatecountryprofileService } from './shared/service/createcountryprofile.service';
import { CreatecountrylistService } from './shared/service/createcountrylist.service';
import { PdfReportComponent } from './country-profile/pdf-report/pdf-report.component';
import { SafeHtmlPipe } from './shared/safe-html.pipe';
import { GetFirstPageComponent } from './country-profile/get-first-page/get-first-page.component';
import { GetSecondPageComponent } from './country-profile/get-second-page/get-second-page.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { PdfComponent } from './country-profile/pdf/pdf.component';
import { UpdateSectionComponent } from './country-profile/update-section/update-section.component';
import { SafeUrlPipe } from './shared/safe-url.pipe';
import { ToastrModule } from 'ng6-toastr-notifications';
import { ModalComponent } from './modal/modal.component';
import { TableService } from './shared/service/table.service';
import { PreviewPdfComponent } from './country-profile/preview-pdf/preview-pdf.component';
import { environment } from 'src/environments/environment';
import { MsalModule, MsalInterceptor, MSAL_CONFIG, MSAL_CONFIG_ANGULAR, MsalService, MsalAngularConfiguration } from '@azure/msal-angular';
import { Configuration } from 'msal';
function MSALConfigFactory(): Configuration {
  return {
    auth: {
      clientId: environment.clientId,
      authority: 'https://login.microsoftonline.com/'+environment.tenanatId +'/',
      validateAuthority: true,
      redirectUri: environment.appUrl,
      postLogoutRedirectUri: environment.appUrl,
      navigateToLoginRequestUrl: true,
    },
    cache: {
      storeAuthStateInCookie: true,
    }
  };
}

function MSALAngularConfigFactory(): MsalAngularConfiguration {
  return {
    popUp: true,
    protectedResourceMap
  };
}
export const protectedResourceMap: [string, string[]][] = [
  ['https://graph.microsoft.com/beta/', ['user.read']]
];


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    HomeComponent,
    CreateCountryProfileComponent,
    CreateSectionComponent,
    PdfReportComponent,
    SafeHtmlPipe,
    GetFirstPageComponent,
    GetSecondPageComponent,
    PdfComponent,
    UpdateSectionComponent,
    UpdateCountryProfileComponent,
    SafeUrlPipe,
    ModalComponent,
    PreviewPdfComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    RichTextEditorAllModule,
    BrowserAnimationsModule,
    AccordionModule,
    DropDownListModule, 
    NgxPaginationModule,
    AlertModule,
    MsalModule,
    ToastrModule.forRoot()
  ],
  providers: [
    CreatesectionsService,
    CreatecountryprofileService,
    CreatecountrylistService,
    TableService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MsalInterceptor,
      multi: true
    },
    {
      provide: MSAL_CONFIG,
      useFactory: MSALConfigFactory
    },
    {
      provide: MSAL_CONFIG_ANGULAR,
      useFactory: MSALAngularConfigFactory
    },
    MsalService
  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
