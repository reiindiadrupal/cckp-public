import { ModalService } from './../shared/service/modal.service';
import { constants } from '../config/constants';
import { Component, ViewChild, OnInit, ElementRef, NgZone  } from '@angular/core';
import { FormsModule, FormGroup, FormBuilder, Validators } from "@angular/forms";
declare var $;
import { Router, ActivatedRoute } from '@angular/router';
import { FilteringEventArgs } from '@syncfusion/ej2-dropdowns';
import { EmitType } from '@syncfusion/ej2-base';
import { Query } from '@syncfusion/ej2-data';
import { CreatecountryprofileService } from '../shared/service/createcountryprofile.service';
import { CreatecountrylistService } from '../shared/service/createcountrylist.service';
import { TableService } from '../shared/service/table.service';

import { environment } from '../../environments/environment';
//import { constants } from "../../config/constants";
import { ToastrManager } from 'ng6-toastr-notifications';
import { AlertService } from './../shared/service/alert.service';
import { MsalGuard, MsalService } from '@azure/msal-angular';
import { HttpClient,HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  pdfUrl: string = "";
  showPdfMsg = false;
  pdfUrlMsg = '';
   loading = false;
  dataTable: any;
  dtOption: any = {};
  public fields: Object = { text: "Country", value: "Id" };
  public text: string = "Filter By Country";
  isDisplay = false;
  isDeleted = false;
  profilesData = [];
  createdCounter = [];
  countrySelectedData = [];
  url :string;
  AndesBold : string;
  config:any;
  collection = { data: [] };
  homeData = constants.homeComponent;
  regions:Array<any>;
  countriesData:Array<any>;
  collectionCountries = { data1: [] };
  data:Array<any>;
  draftCounter = [];
  countyTitle :any = {};
  selectedIndex: number = null;
  public interval;
  username:string;
  name:string;
  constructor(
          private activatedRoute: ActivatedRoute,
          private router: Router, 
          private countryProfiles: CreatecountryprofileService,
          private ngZone: NgZone,
          private toastr: ToastrManager,
          private countryListService: CreatecountrylistService,
          private modalService: ModalService,
          private tableService: TableService,
          private toaster: AlertService,
          private _msalService: MsalService,
          private http: HttpClient
          
  ) { }
   
  ngOnInit() {
   
    if(environment.isBasicAuth){
      const account = this._msalService.getAccount();
      this.name = account.name;
      this.username = account.userName;
    } else {
      this.name = 'User';
      this.username = 'User';
    }
    this.countryList();
    this.countryProfile();
    this.interval = setInterval(() => {this.countryProfile();}, 20000);
  }

  pageChanged(event) { this.config.currentPage = event; if (this.config.currentPage > 1) { console.log('asas', this.config.currentPage); clearInterval(this.interval); } }
  ngAfterViewInit() {
   
  }
  
  openModal(item,profile) {
    this.countyTitle = profile
    this.modalService.open(item);
  }

  closeModal(item,profile){
    this.deleteCountryProfile(profile);
    this.modalService.close(item);
  }
  
  cancelModal(item){
    this.modalService.close(item);
  }

  countryList() {
    this.countryListService.getAllCountryLists().subscribe(
      res => {
        this.countriesData = res['countryList'];
        this.regions = res['countryList'];

        console.log("this.countriesData=",this.countriesData);
        for (var i = 0; i < this.countriesData.length; i++) {
          this.collectionCountries.data1.push(
            this.countriesData[i].country
          );
        }

        var merged = [].concat.apply([], this.collectionCountries.data1);
        var merged2 = [].concat.apply(['All'], merged.sort());
        this.data = merged2;
        
      },
      err => { 
        console.log(err);
      }
    );
  }
  countryProfile = async()=> {
    await this.countryProfiles.getAllCountryProfiles().subscribe(
      res => {
            this.profilesData = res['countryProfile'];
            this.profilesData =  this.profilesData.reverse();
            //this.countrySelectedData = res['countryProfile'];

            for (var i = 0; i < this.profilesData.length; i++) {
              this.collection.data.push(
                {
                  id: i + 1,
                  value: "items number " + (i + 1)
                }
              );
            }
            this.config = {
              itemsPerPage: 10,
              currentPage: 1,
              totalItems: this.profilesData.length
            };

          },
          err => { 
            console.log(err);
          }
        );
  }

  deleteCountryProfile(countryProfileID) {
      this.isDeleted = false;
      this.countryProfiles.deleteCountryProfile(countryProfileID).subscribe(
            res => {
              this.isDeleted = true;
              this.countryProfile();
            },
            err => { 
              console.log(err);
            }
          );
  }

  openPdfReport(i, countryName, countryProfileID, profile) {
    this.selectedIndex = i;
    this.showPdfMsg = true;
    // const url = this.router.serializeUrl(
    //   this.router.createUrlTree(['/pdf/'+countryProfileID+'/'+countryName])
    // );
  
    // window.open(url, '_blank');
    //this.loading = true;
    this.AndesBold = "AndesBold"
    const pdfReportContent = {
      "profilename": countryName.replace(/[^a-zA-Z]/g, "")+countryProfileID+".pdf",
      "conversionDelay" : environment.conversionDelay,
      "navigationtimeout":environment.navigationtimeout,
      "footerText": "<table style='width: 100%;margin-top:20px'><tr><td style='width: 90%;padding-left:6rem;'><p style='font-weight: bold;font-size: 15px;font-family:"+ this.AndesBold +", sans-serif; color:rgb(44,107,172);letter-spacing: -0.2px;text-transform: uppercase;'> CLIMATE RISK COUNTRY PROFILE — "+ countryName +" </p></td><td> <p style='font-weight: bold;  font-size: 15px; font-family:"+ this.AndesBold +", sans-serif; color: rgb(44,107,172);'>&p;</p></td></tr></table>",
      "sectionlist": [
          {
            "sectiontitle": "firstcoverpage",
           "sectionurl": environment.appUrl+"/pdfReport/getFirstPage/"+countryProfileID
           //"sectionurl":"http://15.206.234.47/pdfReport/getFirstPage/5f22534efdec3e70a558ccff"
          },
          {
            "sectiontitle": "acknowledgement",
            "sectionurl":environment.appUrl+"/pdfReport/getSecondPage/"+countryProfileID
            //"sectionurl":"http://15.206.234.47/pdfReport/getKeyMessagesSection/5f22534efdec3e70a558ccff"
          },
          {
            "sectiontitle":"content",
            "sectionurl":environment.appUrl+"/pdfReport/content/"+countryProfileID
            //"sectionurl":"http://15.206.234.47/pdfReport/getForewordsSection/5f28f008fdec3e70a558cd09"
          },
          {
            "sectiontitle": "lastcoverpage",
            "sectionurl":environment.appUrl+"/pdfReport/getFirstPage/"+countryProfileID
           //"sectionurl":"http://15.206.234.47/pdfReport/getFirstPage/5f22534efdec3e70a558ccff"
          }
      ]
    }
    this.countryProfiles.getPDFReport(pdfReportContent).subscribe(
      res => {
        this.loading = false;
        //console.log(res['URL']);
       // setTimeout(() => {
          this.showPdfMsg = false;
          //res['URL'] = "http://15.206.234.47/pdfReport/getFirstPage/5f22534efdec3e70a558ccff";
        this.pdfUrlMsg = res['URL'];
          this.countryProfiles.postUrl(countryProfileID, { pdfUrl: this.pdfUrlMsg }).subscribe(res => {
            console.log('success')
          })
        this.toaster.success(`Your PDF is generated successfully for ${profile.countryName} - ${profile.title}.<br>Click <a class="pdfUrlMsg" href="${this.pdfUrlMsg}" target="_blank" > here </a> to open the PDF.`)
       // }, 10000);
      },
      err => { 
        console.log(err);
      }
    );

  }

  openPdfReportURL(pdfUrl) {
     window.open(pdfUrl, '_blank');
  }
  
  editCountryProfile(countrySelectedData, countryProfileID, profile) {
    this.ngZone.run(() => this.router.navigateByUrl('updateCountryProfile/' + countryProfileID));
  }

  public countryFilter (countryName) {

    this.countryProfiles.getAllCountryProfiles().subscribe(
      res => {
        this.countrySelectedData = res['countryProfile'];
      if(countryName.value === "All") {
         this.interval = setInterval(() => {this.countryProfile();}, 20000);
        
        this.profilesData = this.countrySelectedData;
        this.draftCounter = this.profilesData.filter(
          item => item.status === 1);

        this.createdCounter = this.profilesData.filter(
          item => item.status === 2);
        
      } else {
        clearInterval(this.interval);
        this.isDisplay = true;
        this.profilesData = this.countrySelectedData.filter(function(cntry) {
          return cntry.countryName === countryName.value;
        });

        this.draftCounter = this.profilesData.filter(
          item => item.status === 1);
        
        this.createdCounter = this.profilesData.filter(
        book => book.status === 2);

      }
      this.config = {
        itemsPerPage: 10,
        currentPage: 1,
        totalItems: this.profilesData.length
      };
    },
      err => { 
        console.log(err);
      }
    );
  }


}
